<?php
/**
 * Output a single payment method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment-method.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<li class="wc_payment_method payment_method_<?php echo $gateway->id; ?>">
	<input style="display: none;" id="payment_method_<?php echo $gateway->id; ?>" type="radio" class="input-radio" name="payment_method" value="<?php echo esc_attr( $gateway->id ); ?>" <?php checked( $gateway->chosen, true ); ?> data-order_button_text="<?php echo esc_attr( $gateway->order_button_text ); ?>" />

	<label for="payment_method_<?php echo $gateway->id; ?>">
		<?php if('paypal' == $gateway->id): ?>
			<img src="https://www.paypalobjects.com/webstatic/mktg/logo-center/logotipo_paypal_seguridad.png" alt="Pagar con PayPal">
			<a href="https://www.paypal.com/mx/cgi-bin/webscr?cmd=xpt/Marketing/general/WIPaypal-outside" class="about_paypal" onclick="javascript:window.open('https://www.paypal.com/mx/cgi-bin/webscr?cmd=xpt/Marketing/general/WIPaypal-outside','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;">¿Qué es PayPal?</a>
		<?php elseif('openpay_cards' == $gateway->id): ?>
			<div class="name">
                <strong>Tarjeta de Crédito o Débito</strong>
                <div><img src="https://www.laclicsa.com.mx/wp-content/plugins/openpay-cards//assets/images/openpay.png" alt="OpenPay"></div>
            </div>
			<img src="<?php echo get_site_url(); ?>/wp-content/plugins/openpay-cards/assets/images/credit_cards.png" alt="Paga con Tarjeta de Crédito y Débito">
			<img src="<?php echo get_site_url(); ?>/wp-content/plugins/openpay-cards//assets/images/debit_cards.png" alt="Paga con Tarjeta de Crédito y Débito">
		<?php elseif('openpay_stores' == $gateway->id): ?>
            <div class="name">
			    <strong>Tiendas de Conveniencia</strong>
                <div><img src="https://www.laclicsa.com.mx/wp-content/plugins/openpay-cards//assets/images/openpay.png" alt="OpenPay"></div>
            </div>
            <img src="<?php echo get_site_url(); ?>/wp-content/plugins/openpay-stores//assets/images/stores.png" alt="Paga con Tarjeta de Crédito y Débito">
		<?php else : ?>
				<?php echo $gateway->get_title(); ?> <?php echo $gateway->get_icon(); ?>
		<?php endif; ?>
	</label>

	<?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>
		<div class="payment_box payment_method_<?php echo $gateway->id; ?>" <?php if ( ! $gateway->chosen ) : ?>style="display:none;"<?php endif; ?>>
			<?php $gateway->payment_fields(); ?>
		</div>
	<?php endif; ?>
</li>
