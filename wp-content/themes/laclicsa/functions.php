<?php

// Inicialización del tema
include_once( get_template_directory() . '/lib/init.php' );

require( get_stylesheet_directory() . '/lib/helper-functions.php' );

// Soporte para HTML5
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

// configuración inicial
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

// Datos de usuario
include_once ( get_stylesheet_directory() . '/lib/users-fields.php' );

// Funciones para salida HTML
include_once( get_stylesheet_directory() . '/lib/output-functions.php' );

// Custom post types
include_once( get_stylesheet_directory() . '/lib/custom-posts.php' );

// Traducción
add_action( 'after_setup_theme', 'laclicsa_localization_setup' );

function laclicsa_localization_setup(){
	load_child_theme_textdomain( 'laclicsa', get_stylesheet_directory() . '/languages' );
}

// OPciones de laclicsa
require 'lib/options-page.php';

// Actualización de preferencias
// require 'lib/preferences.php';

// Child Theme
define( 'CHILD_THEME_NAME', 'Laclicsa Laboratorios' );
define( 'CHILD_THEME_URL', 'http://www.laclicsa.com/' );
define( 'CHILD_THEME_VERSION', '1.1.16' );

require 'lib/styles-scripts.php';
// CSS personalizado
include_once( get_stylesheet_directory() . '/lib/custom-css.php' );

// Soporte para accesibilidad
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

// Responsividad
add_theme_support( 'genesis-responsive-viewport' );

// Encabezado personalizado
require 'lib/custom-header.php';

// Widgets para los posts
// add_theme_support( 'genesis-after-entry-widget-area' );

// Color de fondo
add_theme_support( 'custom-background' );

// 3 columnass de widgets en el pie
add_theme_support( 'genesis-footer-widgets', 3 );

// REnombrar barras de navegación
add_theme_support( 'genesis-menus', array( 'primary' => __('Menú Principal', 'laclilcsa'), 'secondary' => __( 'Menu Secundario', 'laclicsa' ) ) );

// Tamaño del Gravatar en la caja de autor
add_filter( 'genesis_author_box_gravatar_size', 'laclicsa_author_box_gravatar' );

function laclicsa_author_box_gravatar( $size ) {
	return 90;
}

// Add WooCommerce support.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php' );

// Images formats
include_once( get_stylesheet_directory() . '/lib/images.php' );

// Add the required WooCommerce styles and Customizer CSS.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php' );

// Add the Genesis Connect WooCommerce notice.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php' );

// Shortcodes for Laclicsa
include_once( get_stylesheet_directory() . '/lib/shortcodes.php' );

// Jobs board
include_once( get_stylesheet_directory() . '/lib/jobs.php' );

// Tamaño del gravatar en los comentarios
add_filter( 'genesis_comment_list_args', 'genesis_sample_comments_gravatar' );

function genesis_sample_comments_gravatar( $args ) {
	$args['avatar_size'] = 60;
	return $args;
}

/* PAra presupuestos */
function laclicsa_budgets_rewrite_basic() {

    //add_rewrite_rule('^presupuesto/pagado/?$', 'index.php?page_id=' . get_field('budgets', 'option') . '&s=payed', 'top');
    //add_rewrite_rule('^presupuesto/verificar/?$', 'index.php?page_id=' . get_field('budgets', 'option') . '&s=verify', 'top');
    add_rewrite_rule('^presupuesto/([^/]+)/?$', 'index.php?page_id=' . get_field('budgets', 'option') . '&budget=$matches[1]', 'top');
    add_rewrite_rule('^presupuesto/([^/]+)/(pagado|verificar)/?$', 'index.php?page_id=' . get_field('budgets', 'option') . '&budget=$matches[1]&status=$matches[2]', 'top');
    add_rewrite_rule('^presupuesto/openpay/?$', 'index.php?page_id=' . get_field('budgets', 'option') . '&action=openpay', 'top');
    add_rewrite_rule('^presupuesto/openpay-wh/?$', 'index.php?page_id=' . get_field('budgets', 'option') . '&action=openpay-wh', 'top');

}
add_action('init', 'laclicsa_budgets_rewrite_basic', 10, 0);

function laclicsa_budgets_rewrite_tag() {
    add_rewrite_tag('%budget%', '([^&]+)');
    add_rewrite_tag('%status%', '([^&]+)');
    add_rewrite_tag('%action%', '([^&]+)');
}
add_action('init', 'laclicsa_budgets_rewrite_tag', 10, 0);

// Widget para facebook
genesis_register_sidebar( array(
    'id' => 'facebook-widget',
    'name' => __( 'Facebook Portada', 'laclicsa' ),
    'description' => __( 'Área para presentar los reviews de facebook en la portada', 'laclicsa' ),
    ) );

add_filter('loop_shop_per_page', 'laclicsa_loop_number_products', 20);
/**
 * Cambia el número de productos por fila
 */
if (!function_exists('laclicsa_loop_number_products')) {
    function laclicsa_loop_number_products( $columns ) {
        return 30; // 3 products per row
    }
}