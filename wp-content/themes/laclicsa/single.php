<?php
/**
 * Genesis Framework.
 *
 * WARNING: This file is part of the core Genesis Framework. DO NOT edit this file under any circumstances.
 * Please do all modifications in the form of a child theme.
 *
 * @package Genesis\Templates
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    http://my.studiopress.com/themes/genesis/
 */

// This file handles single entries, but only exists for the sake of child theme forward compatibility.

// Add class blog to page
add_filter( 'body_class', 'laclicsa_add_blog_class' );

function laclicsa_add_blog_class( $classes ){
    $classes[] = 'laclicsa-blog';
    return $classes;
}

add_action( 'genesis_before_entry', 'featured_post_image', 8 );
function featured_post_image() {
    if ( ! is_singular( 'post' ) )  return;
    the_post_thumbnail('post-image');
}

genesis();
