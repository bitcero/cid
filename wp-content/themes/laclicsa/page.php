<?php
/**
 * Plantilla general para las página
 */

add_action('genesis_after_header', 'laclicsa_page_header');

if(get_field('featured-image', get_the_ID())){
    add_action( 'genesis_before_entry', 'featured_post_image', 8 );
    function featured_post_image() {
        if ( ! is_singular( 'page' ) )  return;
        the_post_thumbnail('post-image');
    }
}

genesis();