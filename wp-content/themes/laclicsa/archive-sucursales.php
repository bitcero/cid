<?php

add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

remove_action( 'genesis_loop', 'genesis_do_loop' );

add_action( 'genesis_loop', 'laclicsa_sucursales_loop' );

function laclicsa_sucursales_loop() {

    echo '<h1 class="sucursales-title">' . __('Nuestras Sucursales', 'laclicsa') . '</h1>';

    $args = [
        'post_type' => ['sucursales'],
        'post_status' => 'publish',
        'orderby' => 'name',
        'order' => 'ASC',
        'posts_per_page' => 60
    ];

    $query = new WP_Query($args);

    if ( $query->have_posts() ){

        echo '<div class="laclicsa-sucursales">';

        $counter = 1;
        $col3 = 1;
        $col2 = 1;

        while ($query->have_posts() ) : $query->the_post();

            printf( '<article %s>', genesis_attr('entry', ['data-three' => $col3, 'data-two' => $col2]) );

            $map = get_field('map', get_the_ID());

            if( '' == $map ){
                $map = get_field('default-map', 'option' );
            }

            ?>

            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <img src="<?php echo $map; ?>" alt="<?php the_title(); ?>">
            </a>

            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            <p><?php echo get_field('address', get_the_ID()); ?></p>

            <?php

            echo '</article>';

            if($counter % 2 == 0){
                echo '<div class="clearfix show-in-two"></div>';
            }

            if($counter % 3 == 0){
                echo '<div class="clearfix show-in-three"></div>';
            }

            $counter++;
            $col3++;
            $col2++;

            if($col3 > 3){
                $col3 = 1;
            }

            if($col2 > 2){
                $col2 = 1;
            }

        endwhile;

        echo '</div>';

    }

}

genesis();