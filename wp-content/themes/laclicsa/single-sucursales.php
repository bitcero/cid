<?php

add_action('genesis_after_header', 'laclicsa_sucursal_header');

function laclicsa_sucursal_header() {
	?>
    <div id="laclicsa-page-title" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
        <div class="overlay">
            <div class="wrap">
                <h1>
                    <small><?php _e('Sucursales', 'laclicsa'); ?></small>
					<?php echo get_the_title(); ?>
                </h1>
            </div>
        </div>
    </div>
	<?php
}

add_filter('language_attributes', 'laclicsa_html_class');
function laclicsa_html_class($output) {
	return $output . ' class="sucursales-html"';
}

add_filter( 'genesis_markup_site-inner', '__return_null' );
add_filter( 'genesis_markup_content-sidebar-wrap_output', '__return_false' );
add_filter( 'genesis_markup_content', '__return_null' );
remove_action( 'genesis_loop', 'genesis_do_loop' );

add_filter( 'genesis_pre_get_option_site_layout', function(){return 'full-width-content';} );

add_action('genesis_after_header', 'laclicsa_suscursal_content');

/**
 * Muestra el mapa antes del footer
 *
 * @return
 */
function laclicsa_suscursal_content() {

    ?>
    <div id="sucursal-page" class="wrap">

        <div class="three-fourths first">
            <div class="info">

                <div class="sucursal-data">
                    <ul>
                        <li>
                            <label><?php _e('Servicios de esta sucursal:', 'laclicsa'); ?></label>
                            <ul>
                                <?php
                                $services = get_field('services');
                                foreach($services as $service):
                                    ?>
                                    <li><?php echo $service['name']; ?></li>
                                <?php
                                endforeach;
                                ?>
                            </ul>
                        </li>
                        <li>
                            <label><?php _e('Dirección', 'laclicsa'); ?></label>
                            <?php echo get_field('address'); ?>
                        </li>

                        <li>
                            <label><?php _e('Teléfono:', 'laclicsa'); ?></label>
                            <?php echo get_field('phone'); ?>
                        </li>

                        <li>
                            <label><?php _e('Horario de Atención', 'laclicsa'); ?></label>
                            <?php echo get_field('working'); ?>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="map">
                <div id="map-container">
                    <?php echo do_shortcode('[display_map width="100%" height="500" zoom="18" language="es" map_type="ROADMAP" map_draggable="true" marker1="' . get_field('lat') . " | " . get_field('lon') . ' | title | infowindow message | marker category name"]'); ?>
                </div>
            </div>
        </div>

        <div class="one-fourth">
            <?php echo do_action('genesis_sidebar'); ?>
        </div>

    </div>

    <?php

    //echo do_shortcode('[display_map width="100%" height="500" zoom="18" language="es" map_type="ROADMAP" map_draggable="true" marker1="' . get_field('lat') . " | " . get_field('lon') . ' | title | infowindow message | marker category name"]');

}


//add_action('genesis_before_footer', 'laclicsa_footer_map');
//add_action('genesis_after', 'laclicsa_footer_map');

function laclicsa_footer_map() {
    echo '<div id="map-container">';
	echo do_shortcode('[display_map width="100%" height="500" zoom="18" language="es" map_type="ROADMAP" map_draggable="true" marker1="' . get_field('lat') . " | " . get_field('lon') . ' | title | infowindow message | marker category name"]');
	echo '</div>';
}

genesis();