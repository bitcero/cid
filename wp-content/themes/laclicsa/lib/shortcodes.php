<?php

/**
 * Agrega la información a la página de contacto
 */
function laclicsa_sc_contact($attrs, $content)
{

    $attrs = shortcode_atts([
        'type' => 'contact'
    ], $attrs);

    ob_start(); ?>

    <div class="two-sixths first">
        <h4><?php _e('Otros métodos de contacto', 'laclicsa'); ?></h4>
        <ul class="laclisa-contact-options">
            <?php
            $phones = get_field('phones', 'option');

            if ($phones): ?>

                <li class="title"><?php _e('Teléfonos:', 'laclicsa'); ?></li>

                <?php
            endif;
            foreach ($phones as $phone) {
                ?>
                <li>
                    <span class="icon icon-phone"></span>
                    <a href="tel:<?php echo clean_phone_number(
                        $phone['phone'], false
                    ); ?>"><?php echo $phone['phone']; ?></a>
                </li>
                <?php
            }
            ?>

            <?php
            $whats = get_field('whatsapp-number', 'option');
            if ($whats):
                ?>
                <li class="title"><?php _e('Vía Whatsapp', 'laclicsa'); ?></li>
                <li>
                    <span class="icon icon-whatsapp"></span>
                    <a href="https://api.whatsapp.com/send?phone=<?php echo clean_phone_number(
                        $whats
                    ); ?>"><?php echo $whats; ?></a>
                </li>
                <?php
            endif;
            ?>
            <li class="title">Horarios de servicio:</li>
            <li>
                Lunes a Viernes de 9:00 a 19:00 horas.<br>
                Sábados de 9:00 a 14:00 horas.<br>
                Días festivos de 9:00 a 14:00 Horas.<br>
                Se suspenden labores los días 25 de Diciembre y 1 de Enero.

            </li>

            <li class="title"></li>
            <li>
                <span class="icon icon-facebook-square"></span>
                <a href="<?php echo get_option(
                    'facebook-link', 'option'
                ); ?>"><?php _e('En Facebook', 'laclicsa'); ?></a>
            </li>
            <li class="title"></li>
            <li><a href="/sucursales"><strong><?php _e(
                            'Visita cualquiera de nuestras sucursales',
                            'laclicsa'
                        ); ?></strong></a></li>
        </ul>
    </div>
    <div class="four-sixths">
        <?php echo $content; ?>
        <hr>
        <?php

        $formId = get_field($attrs['type'] . '-form', 'option');

        if ($formId) {
            echo do_shortcode(
                '[wpforms id="' . $formId
                . '" title="false" description="false"]'
            );
        }
        ?>
    </div>

    <?php
    return ob_get_clean();
}

add_shortcode('laclicsa-contact', 'laclicsa_sc_contact');

//--------------------

/**
 * Shortcode para la página de empresas
 */
function laclicsa_empresas_analisis($attrs, $content)
{
    ob_start(); ?>

    <div class="laclicsa-page-empresas">
        <div class="info-analysis">
            <h4 class="title">Servicios de Análisis Clínicos</h4>

            <ul class="credits">
                <li>Laboratorio acreditado en <strong>ISO 15189:2012</strong></li>
                <li>Equipo de laboratorio automatizado</li>
            </ul>

            <strong>Servicios</strong>

            <ul class="services">
                <li>Platicas de medicina preventiva, seguridad y salud
                    ocupacional.
                </li>
                <li>Diagnostico situacional de la empresa.</li>
                <li>Check Up ejecutivos.</li>
                <li>Pruebas toxicológicas.</li>
                <li>Pruebas para contratación de personal.</li>
                <li>Estudios periódicos de prevención y control de
                    enfermedades.
                </li>
                <li>Tomas a domicilio.</li>
                <li>Desarrollo de paquetes de salud.</li>
                <li>Jornadas de salud.</li>
                <li>Tarjeta de beneficios para sus empleados sin costo para
                    usted.
                </li>
                <li>Control de manejadores de alimentos.</li>

            </ul>

            <button type="button" class="button" data-card="0"><?php _e(
                    'Contacto', 'laclicsa'
                ); ?></button>
        </div>
    </div>

    <div class="laclicsa-empresas-contact">

        <div class="contact-card-overlay"></div>

        <?php
        $contactos = get_field('empresas', 'option');

        foreach ($contactos as $id => $contact):
            ?>

            <div id="contact-<?php echo $id; ?>" class="contact-card">
                <span class="close">&times;</span>
                <span class="dashicons dashicons-businessman contact-icon"></span>
                <ul>
                    <li class="name"><?php echo $contact['name']; ?></li>
                    <li class="position"><?php echo $contact['position']; ?></li>
                    <li class="phone"><span
                                class="dashicons dashicons-phone"></span> <?php echo $contact['phone']; ?>
                    </li>
                    <li class="cellphone"><span
                                class="dashicons dashicons-smartphone"></span> <?php echo $contact['cellphone']; ?>
                    </li>
                    <li class="email"><span
                                class="dashicons dashicons-email"></span> <?php echo str_replace(
                            ['@', '.'], ['<i></i>', '<em></em>'],
                            $contact['email']
                        ); ?></li>
                </ul>
            </div>

            <?php
        endforeach;
        ?>

    </div>

    <?php
    return ob_get_clean();

}

function laclicsa_empresas_cs($attrs, $content)
{

    ob_start();

    ?>

    <div class="laclicsa-page-empresas">

        <div class="info-control">
            <h4 class="title">Servicios de Control Sanitario</h4>

            <ul class="credits">
                <li>Laboratorio acreditado en <strong>ISO 17025:2005</strong></li>
                <li>Laboratorio tercero autorizado <strong>COFEPRIS</strong>
                </li>
            </ul>

            <strong>Servicios</strong>

            <ul class="services">
                <li>Análisis microbiológicos de los alimentos</li>
                <li>Análisis microbiológicos de agua y hielo</li>
                <li>Análisis microbiológicos de albercas.</li>
                <li>Playas</li>
                <li>Fisicoquímico de Agua</li>
            </ul>

            <button type="button" class="button" data-card="1"><?php _e(
                    'Contacto', 'laclicsa'
                ); ?></button>
        </div>
    </div>

    <div class="laclicsa-empresas-contact">

        <div class="contact-card-overlay"></div>

        <?php
        $contactos = get_field('empresas', 'option');

        foreach ($contactos as $id => $contact):
            ?>

            <div id="contact-<?php echo $id; ?>" class="contact-card">
                <span class="close">&times;</span>
                <span class="dashicons dashicons-businessman contact-icon"></span>
                <ul>
                    <li class="name"><?php echo $contact['name']; ?></li>
                    <li class="position"><?php echo $contact['position']; ?></li>
                    <li class="phone"><span
                                class="dashicons dashicons-phone"></span> <?php echo $contact['phone']; ?>
                    </li>
                    <li class="cellphone"><span
                                class="dashicons dashicons-smartphone"></span> <?php echo $contact['cellphone']; ?>
                    </li>
                    <li class="email"><span
                                class="dashicons dashicons-email"></span> <?php echo str_replace(
                            ['@', '.'], ['<i></i>', '<em></em>'],
                            $contact['email']
                        ); ?></li>
                </ul>
            </div>

            <?php
        endforeach;
        ?>

    </div>

    <?php

    return ob_get_clean();

}

add_shortcode('laclicsa-analisis-empresas', 'laclicsa_empresas_analisis');
add_shortcode('laclicsa-cs-empresas', 'laclicsa_empresas_cs');

/**
 * Shortcodes for ISO logos
 */
function laclicsa_iso_logo($args)
{

    $args = shortcode_atts(
        [
            'type' => '',
            'size' => '50'
        ], $args
    );

    if( '' == $args['type'] ){
        return '';
    }

    if('15189' == $args['type']){
        return '<img src="' . get_stylesheet_directory_uri() . '/assets/images/iso15189-s.png" class="iso-logo-small" style="width: ' . $args['size'] . 'px;">';
    }

    if('17025' == $args['type']){
        return '<img src="' . get_stylesheet_directory_uri() . '/assets/images/iso17025-s.png" class="iso-logo-small" style="width: ' . $args['size'] . 'px;">';
    }

}

add_shortcode('iso-logo', 'laclicsa_iso_logo');