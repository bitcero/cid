<?php
/**
 * Este archivo genera el código CSS personalizado derivado de las opciones
 * de configuración del tema.
 */

/**
 * Agrega el CSS en línea
 */
function laclicsa_inline_styles() {

	ob_start();
	include 'custom-css/css-dynamic.scss';
	$scss = ob_get_clean();

	require 'scss.inc.php';

	$scssphp = new \Leafo\ScssPhp\Compiler();
	$scssphp->setImportPaths(get_stylesheet_directory() . "/lib/custom-css/");
	$css = $scssphp->compile($scss);

	require 'custom-css/minifier.php';
	$css = minify_css($css);

	wp_add_inline_style( 'laclicsa-laboratorios', $css );
}

add_action( 'wp_enqueue_scripts', 'laclicsa_inline_styles' );