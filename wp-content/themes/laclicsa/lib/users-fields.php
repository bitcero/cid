<?php
/**
 * Modifica los datos que se registran del usuario para permitir
 * relacionarlo con la base de datos de Laclicsa.
 */

add_action( 'show_user_profile', 'laclicsa_user_profile_fields' );
add_action( 'edit_user_profile', 'laclicsa_user_profile_fields' );
/**
 * Muestra el campo "ID Laclicsa" en el formulario de
 * edición.
 *
 * @param $user
 */
function laclicsa_user_profile_fields( $user ) { ?>
	<h3><?php _e("Información Laclicsa", "laclicsa"); ?></h3>

	<table class="form-table">
		<tr>
			<th><label for="id_laclicsa"><?php _e("ID Laclicsa:"); ?></label></th>
			<td>
				<input type="number" name="id_laclicsa" id="id_laclicsa" value="<?php echo esc_attr( get_user_meta( $user->ID, 'id_laclicsa', true ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e("Proporcione el ID interno de Laclicsa", 'laclicsa'); ?></span>
			</td>
		</tr>
	</table>
<?php }

add_action( 'personal_options_update', 'laclicsa_save_user_profile_fields' );
add_action( 'edit_user_profile_update', 'laclicsa_save_user_profile_fields' );
/**
 * Almacena los nuevos datos de usuario utilizados por Laclicsa
 *
 * @param $user_id
 *
 * @return bool
 */
function laclicsa_save_user_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) ) {
		return false;
	}
	return update_user_meta( $user_id, 'id_laclicsa', $_POST['id_laclicsa'] );
}