<?php
add_filter( 'genesis_theme_settings_defaults', 'genesis_sample_theme_defaults' );
/**
 * Updates theme settings on reset.
 *
 * @since 2.2.3
 */
function genesis_sample_theme_defaults( $defaults ) {

	$defaults['blog_cat_num']              = 9;
	$defaults['content_archive']           = 'full';
	$defaults['content_archive_limit']     = 0;
	$defaults['content_archive_thumbnail'] = 0;
	$defaults['posts_nav']                 = 'numeric';
	$defaults['site_layout']               = 'content-sidebar';

	return $defaults;

}

add_action( 'after_switch_theme', 'genesis_sample_theme_setting_defaults' );
/**
 * Updates theme settings on activation.
 *
 * @since 2.2.3
 */
function genesis_sample_theme_setting_defaults() {

	if ( function_exists( 'genesis_update_settings' ) ) {

		genesis_update_settings( array(
			'blog_cat_num'              => 6,
			'content_archive'           => 'full',
			'content_archive_limit'     => 0,
			'content_archive_thumbnail' => 0,
			'posts_nav'                 => 'numeric',
			'site_layout'               => 'content-sidebar',
		) );

	}

	update_option( 'posts_per_page', 6 );

}

add_filter('genesis_pre_get_option_footer_text', 'laclicsa_footer_creds_filter');
function laclicsa_footer_creds_filter( $creds ) {
	$creds = get_field('footer', 'option');
	return $creds;
}

// Insert content before footer
function laclicsa_before_footer() {

    if( is_woocommerce() ) {
        ?>

        <div class="site-inner pre-footer">
            <div class="content legend-price">
                * El precio en línea no es válido en sucursales, solo aplica en pagos en línea.
            </div>
        </div>

        <?php
    }
}

//add_action ('genesis_before_footer', 'laclicsa_before_footer', 5);

function laclicsa_after_footer()
{
    if(get_field('livechat', 'option')){
        echo get_field('livechat_code', 'option');
    }
}
add_action('genesis_after_footer', 'laclicsa_after_footer');

/**
 * Solución temporal para enviar webhooks a laclicsa
 */
add_filter('http_request_reject_unsafe_urls','__return_false');
add_filter('https_ssl_verify','__return_false');
