<?php

add_theme_support( 'custom-logo', array(
	'width'       => 300,
	'height'      => 100,
	'flex-width' => true,
	'flex-height' => true,
) );

add_filter( 'genesis_seo_title', 'laclicsa_header_inline_logo', 10, 3 );

add_action( 'genesis_before_header', 'laclicsa_make_topmenu' );
/**
 * Add an image inline in the site title element for the logo
 *
 * @param string $title Current markup of title.
 * @param string $inside Markup inside the title.
 * @param string $wrap Wrapping element for the title.
 *
 * @author @_AlphaBlossom
 * @author @_neilgee
 * @author @_JiveDig
 * @author @_srikat
 */
function laclicsa_header_inline_logo( $title, $inside, $wrap ) {
	// If the custom logo function and custom logo exist, set the logo image element inside the wrapping tags.
	if ( function_exists( 'has_custom_logo' ) && has_custom_logo() ) {
		$inside = sprintf( '<span class="screen-reader-text">%s</span>%s', esc_html( get_bloginfo( 'name' ) ), get_custom_logo() );
	} else {
		// If no custom logo, wrap around the site name.
		$inside	= sprintf( '<a href="%s">%s</a>', trailingslashit( home_url() ), esc_html( get_bloginfo( 'name' ) ) );
	}

	// Build the title.
	$title = genesis_markup( array(
		'open'    => sprintf( "<{$wrap} %s>", genesis_attr( 'site-title' ) ),
		'close'   => "</{$wrap}>",
		'content' => $inside,
		'context' => 'site-title',
		'echo'    => false,
		'params'  => array(
			'wrap' => $wrap,
		),
	) );

	return $title;
}

add_filter( 'genesis_attr_site-description', 'laclicsa_add_site_description_class' );
/**
 * Add class for screen readers to site description.
 * This will keep the site description markup but will not have any visual presence on the page
 * This runs if there is a logo image set in the Customizer.
 *
 * @param array $attributes Current attributes.
 *
 * @author @_neilgee
 * @author @_srikat
 */
function laclicsa_add_site_description_class( $attributes ) {
	if ( function_exists( 'has_custom_logo' ) && has_custom_logo() ) {
		$attributes['class'] .= ' screen-reader-text';
	}

	return $attributes;
}

// Cambiar la posición del menú principal
unregister_sidebar( 'header-right' );
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav', 12 );

/**
 * Agregamos el menú en la parte superior de la cabecera.
 * ----
 * Este menú se configura en las opciones del tema laclicsa,
 * en la pestaña "Menú Superior"
 */
remove_action( 'genesis_header', 'genesis_header_markup_open', 5 );

// Add back header opening markup function w/o the structural wrap
add_action( 'genesis_header', 'laclicsa_header_markup_open', 5 );
function laclicsa_header_markup_open() {

	genesis_markup( array(
		'html5'   => '<header %s>',
		'xhtml'   => '<div id="header">',
		'context' => 'site-header',
	) );

}

add_action( 'genesis_header', 'laclicsa_menubar', 7 );
function laclicsa_menubar() {

	//laclicsa_make_topmenu();

	// Maybe add opening .wrap div tag with header context.
	genesis_structural_wrap( 'header' );

}

add_action( 'get_header', 'remove_titles_all_single_pages' );
function remove_titles_all_single_pages() {
	if ( is_singular('page') ) {
		remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
	}
}

unregister_sidebar( 'header-right' );

/**
 * Add toggle menu button
 */
add_action( 'genesis_header_right', 'laclicsa_header_toggle' );

function laclicsa_header_toggle() {
    ?>
    <button type="button" id="primary-menu-toggle">
        <span class="sr-only">Mostrar Menú</span>
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
    </button>
    <?php
}

// Agregar el cuadro de búsqueda
add_filter( 'wp_nav_menu_items', 'laclicsa_add_search_to_menu', 10, 2 );

function laclicsa_add_search_to_menu( $menu, $args ) {

    //* Change 'primary' to 'secondary' to add extras to the secondary navigation menu
    if ( 'primary' !== $args->theme_location )
        return $menu;
    //* Uncomment this block to add a search form to the navigation menu

    ob_start();
    ?>
    <li id="laclicsa-search-nav" class="menu-item menu-item-type-search">
        <form itemprop="potentialAction" itemtype="https://schema.org/SearchAction" method="get" action="<?php echo bloginfo('url'); ?>" role="search">
            <meta itemprop="target" content="<?php echo bloginfo('url'); ?>/?s={s}">
            <label class="search-form-label screen-reader-text" for="searchform-input"><?php _('Search this website', 'laclicsa'); ?></label>
            <input itemprop="query-input" type="search" name="s" id="searchform-input" placeholder="<?php _e('Buscar...', 'laclicsa'); ?>">
            <button type="submit" title="<?php _e('Buscar', 'laclicsa'); ?>"><span class="dashicons dashicons-search"></span></button>
        </form>
    </li>
    <?php
    $search = ob_get_clean();
    $menu  = $search . $menu;

    //* Uncomment this block to add the date to the navigation menu
    /*
    $menu .= '<li class="right date">' . date_i18n( get_option( 'date_format' ) ) . '</li>';
    */
    return $menu;
}