<?php

// Imágenes para promociones de la tienda
add_image_size( 'shop-offer', 500);
add_image_size( 'home_services', 700, 350, true);
add_image_size( 'blog-thumbnail', 700, 500, true);
add_image_size( 'testimonial', 300, 300, true);
