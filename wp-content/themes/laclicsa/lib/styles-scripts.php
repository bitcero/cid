<?php
/*
 * Archivo para agregar estilos CSS y scripts JS al tema
 */

add_action( 'wp_enqueue_scripts', 'laclicsa_enqueue_scripts_styles' );

function laclicsa_enqueue_scripts_styles() {

    wp_enqueue_style( 'main-font', get_field('font', 'option'), array(), CHILD_THEME_VERSION );
    wp_enqueue_style( 'secondary-font', get_field('additional-font', 'option'), array(), CHILD_THEME_VERSION );
    //wp_enqueue_style( 'dashicons' );
    //wp_enqueue_style( 'laclicsa-icons', get_stylesheet_directory_uri() . '/assets/css/lfont.css', [], CHILD_THEME_VERSION );

    $suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
    /*wp_enqueue_script( 'laclicsa-responsive-menu', get_stylesheet_directory_uri() . "/assets/js/responsive-menus{$suffix}.js", array( 'jquery' ), CHILD_THEME_VERSION, true );
    wp_localize_script(
        'genesis-sample-responsive-menu',
        'genesis_responsive_menu',
        laclicsa_responsive_menu_settings()
    );*/

    // JS principal
    if( get_page_template_slug() == 'page-templates/tpl-budgets.php' ){
        $lacsRequires = ['jquery', 'openpay-data'];
    } else {
        $lacsRequires = ['jquery'];
    }
    wp_enqueue_script('laclicsa', get_stylesheet_directory_uri() . '/assets/js/laclicsa.min.js', $lacsRequires, CHILD_THEME_VERSION, true);


    // Remove unused styles
    wp_dequeue_style('ls-google-fonts');
    wp_deregister_style('ls-google-fonts');
    wp_dequeue_style('simple-job-board-google-fonts');
    wp_deregister_style('simple-job-board-google-fonts');
    wp_dequeue_style('simple-job-board-font-awesome');
    wp_deregister_style('simple-job-board-font-awesome');
    wp_dequeue_style('simple-job-board-jquery-ui');
    wp_deregister_style('simple-job-board-jquery-ui');
    wp_dequeue_style('simple-job-board-frontend');
    wp_deregister_style('simple-job-board-frontend');

    //wp_dequeue_style('fbrev_css');
    //wp_deregister_style('fbrev_css');

    wp_dequeue_style('genesis-sample-woocommerce-styles');
    wp_deregister_style('genesis-sample-woocommerce-styles');

    wp_dequeue_style('wpgmp-frontend');
    wp_deregister_style('wpgmp-frontend');


}

// Define our responsive menu settings.
function laclicsa_responsive_menu_settings() {

	$settings = array(
		'mainMenu'          => __( 'Menu', 'genesis-sample' ),
		'menuIconClass'     => 'dashicons-before dashicons-menu',
		'subMenu'           => __( 'Submenu', 'genesis-sample' ),
		'subMenuIconsClass' => 'dashicons-before dashicons-arrow-down-alt2',
		'menuClasses'       => array(
			'combine' => array(
				'.nav-primary',
				'.nav-header',
			),
			'others'  => array(),
		),
	);

	return $settings;

}