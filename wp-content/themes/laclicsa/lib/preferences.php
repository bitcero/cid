<?php

# Actualizar los estilos CSS
add_action('acf/save_post', 'laclicsa_onsave_preferences', 20);

/**
 * Usada para actualizar los estilos CSS a partir de SASS
 * @param $post_id
 */
function laclicsa_onsave_preferences($post_id) {

	if($post_id != 'options'){
		return;
	}

	require 'scss.inc.php';

	$scssphp = new \Leafo\ScssPhp\Compiler();
	$scssphp->setImportPaths(get_stylesheet_directory() . "/src/sass/");

	// Save compiled file
	file_put_contents(
		get_stylesheet_directory() . '/assets/css/style.min.css',
		$scssphp->compile(file_get_contents(get_stylesheet_directory() . "/src/sass/styles.scss"))
	);

	die();
}