<?php
add_shortcode('laclicsa-url', 'laclicsa_url');

function laclicsa_url()
{
    return get_site_url();
}

/**
 * Get default link color for Customizer.
 * Abstracted here since at least two functions use it.
 *
 * @since 2.2.3
 *
 * @return string Hex color code for link color.
 */
function genesis_sample_customizer_get_default_link_color()
{
    return '#09357A';
}

/**
 * Get default accent color for Customizer.
 * Abstracted here since at least two functions use it.
 *
 * @since 2.2.3
 *
 * @return string Hex color code for accent color.
 */
function genesis_sample_customizer_get_default_accent_color()
{
    return '#09357A';
}

/**
 * Calculate the color contrast.
 *
 * @since 2.2.3
 *
 * @return string Hex color code for contrast color
 */
function genesis_sample_color_contrast($color)
{

    $hexcolor = str_replace('#', '', $color);
    $red      = hexdec(substr($hexcolor, 0, 2));
    $green    = hexdec(substr($hexcolor, 2, 2));
    $blue     = hexdec(substr($hexcolor, 4, 2));

    $luminosity = (($red * 0.2126) + ($green * 0.7152) + ($blue * 0.0722));

    return ($luminosity > 128) ? '#333333' : '#ffffff';

}

/**
 * Calculate the color brightness.
 *
 * @since 2.2.3
 *
 * @return string Hex color code for the color brightness
 */
function genesis_sample_color_brightness($color, $change)
{

    $hexcolor = str_replace('#', '', $color);

    $red   = hexdec(substr($hexcolor, 0, 2));
    $green = hexdec(substr($hexcolor, 2, 2));
    $blue  = hexdec(substr($hexcolor, 4, 2));

    $red   = max(0, min(255, $red + $change));
    $green = max(0, min(255, $green + $change));
    $blue  = max(0, min(255, $blue + $change));

    return '#' . dechex($red) . dechex($green) . dechex($blue);

}

function clean_phone_number($number, $addPrefix = true)
{

    $number = preg_replace('/[^0-9,]|,[0-9]*$/', '', $number);

    if ($addPrefix) {
        return "52" . $number;
    }

    return $number;

}

/**
 * Inserta un encabezado gráfico en la página para mostrar como
 * fondo de la barra de nevageación y título
 */
function laclicsa_page_header()
{
    $showBg = get_field('image', get_the_ID());
    ?>
    <div id="laclicsa-page-title"
         style="<?php echo $showBg ? 'background-image: url('
             . get_the_post_thumbnail_url() . ');' : ''; ?>" class="with-quality">
        <div class="overlay">
            <div class="wrap">
                <h1>
                    <?php
                    $title   = get_field('headline', get_the_ID());
                    $title   = '' == $title ? get_the_title() : $title;
                    $tagline = get_field('tagline', get_the_ID());
                    ?>
                    <?php echo $title; ?>
                    <?php if ('' != $tagline): ?>
                        <small><?php echo $tagline; ?></small>
                    <?php endif; ?>
                </h1>


                    <div class="quality-logos">
                        <a href="<?php echo get_field('banner-url', 'option'); ?>">
                            <span>Consulta los alcances aquí</span>
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/iso17025-h.png'; ?>" alt="Laboratorio de prueba tercero autorizado">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/iso15189-h.png'; ?>" alt="Primer laboratorio en México acreditado en ISO 15189:2012">
                        </a>
                    </div>


            </div>
        </div>
    </div>
    <?php
}


