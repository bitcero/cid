<?php
/**
 * Genesis Sample.
 *
 * This file adds the required WooCommerce setup functions to the Genesis Sample Theme.
 *
 * @package Genesis Sample
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    http://www.studiopress.com/
 */

add_action( 'wp_enqueue_scripts', 'laclicsa_products_match_height', 99 );
/**
 * Print an inline script to the footer to keep products the same height.
 *
 * @since 2.3.0
 */
function laclicsa_products_match_height() {

	// If Woocommerce is not activated, or a product page isn't showing, exit early.
	if ( ! class_exists( 'WooCommerce' ) || ! is_shop() && ! is_product_category() && ! is_product_tag() ) {
		return;
	}

	wp_enqueue_script( 'laclicsa-match-height', get_stylesheet_directory_uri() . '/js/jquery.matchHeight.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true );
	wp_add_inline_script( 'laclicsa-match-height', "jQuery(document).ready( function() { jQuery( '.product .woocommerce-LoopProduct-link').matchHeight(); });" );

}

add_filter( 'woocommerce_style_smallscreen_breakpoint', 'laclicsa_woocommerce_breakpoint' );
/**
 * Modify the WooCommerce breakpoints.
 *
 * @since 2.3.0
 *
 * @return string Pixel width of the theme's breakpoint.
 */
function laclicsa_woocommerce_breakpoint() {

	$current = genesis_site_layout();
	$layouts = array(
		'one-sidebar' => array(
			'content-sidebar',
			'sidebar-content',
		),
		'two-sidebar' => array(
			'content-sidebar-sidebar',
			'sidebar-content-sidebar',
			'sidebar-sidebar-content',
		),
	);

	if ( in_array( $current, $layouts['two-sidebar'] ) ) {
		return '2000px'; // Show mobile styles immediately.
	}
	elseif ( in_array( $current, $layouts['one-sidebar'] ) ) {
		return '1200px';
	}
	else {
		return '860px';
	}

}

add_filter( 'genesiswooc_products_per_page', 'laclicsa_default_products_per_page' );
/**
 * Set the default products per page.
 *
 * @since 2.3.0
 *
 * @return int Number of products to show per page.
 */
function laclicsa_default_products_per_page() {
	return 8;
}

add_filter( 'woocommerce_pagination_args', 	'laclicsa_woocommerce_pagination' );
/**
 * Update the next and previous arrows to the default Genesis style.
 *
 * @since 2.3.0
 *
 * @return string New next and previous text string.
 */
function laclicsa_woocommerce_pagination( $args ) {

	$args['prev_text'] = sprintf( '&laquo; %s', __( 'Previous Page', 'laclicsa' ) );
	$args['next_text'] = sprintf( '%s &raquo;', __( 'Next Page', 'laclicsa' ) );

	return $args;

}

add_action( 'after_switch_theme', 'laclicsa_woocommerce_image_dimensions_after_theme_setup', 1 );
/**
* Define WooCommerce image sizes on theme activation.
*
* @since 2.3.0
*/
function laclicsa_woocommerce_image_dimensions_after_theme_setup() {

	global $pagenow;

	if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' || ! class_exists( 'WooCommerce' ) ) {
		return;
	}

	laclicsa_update_woocommerce_image_dimensions();

}

add_action( 'activated_plugin', 'laclicsa_woocommerce_image_dimensions_after_woo_activation', 10, 2 );
/**
 * Define the WooCommerce image sizes on WooCommerce activation.
 *
 * @since 2.3.0
 */
function laclicsa_woocommerce_image_dimensions_after_woo_activation( $plugin ) {

	// Check to see if WooCommerce is being activated.
	if ( $plugin !== 'woocommerce/woocommerce.php' ) {
		return;
	}

	laclicsa_update_woocommerce_image_dimensions();

}

/**
 * Update WooCommerce image dimensions.
 *
 * @since 2.3.0
 */
function laclicsa_update_woocommerce_image_dimensions() {

	$catalog = array(
		'width'  => '500', // px
		'height' => null, // px
		'crop'   => 0,     // true
	);
	$single = array(
		'width'  => '655', // px
		'height' => null, // px
		'crop'   => 0,     // true
	);
	$thumbnail = array(
		'width'  => '180', // px
		'height' => '180', // px
		'crop'   => 1,     // true
	);

	// Image sizes.
	update_option( 'shop_catalog_image_size', $catalog );     // Product category thumbs.
	update_option( 'shop_single_image_size', $single );       // Single product image.
	update_option( 'shop_thumbnail_image_size', $thumbnail ); // Image gallery thumbs.

}


add_filter( 'woocommerce_before_widget_product_list', 'laclicsa_woocommerce_products_widget' );
/**
 * Actualiza el widget para mostrar productos
 */
function laclicsa_woocommerce_products_widget() {

    /*echo '<div class="membresia-price in-widget"><span>' . __('Con tu membresía obtén descuentos y acumula puntos', 'laclicsa') . '</span>
            <a href="' . get_field('membership-page', 'option' ) . '" class="member-button">Más Información</a>
            </div>';*/
	echo '<ul class="laclicsa_products_list_widget">';
}

add_filter('loop_shop_columns', 'laclicsa_loop_products');
/**
 * Cambia el número de productos por fila
 */
if (!function_exists('laclicsa_loop_products')) {
	function laclicsa_loop_products( $columns ) {
		return 3; // 3 products per row
	}
}

add_filter('woocommerce_add_to_cart_redirect', 'laclicsa_add_to_cart_redirect');
/**
 * Redireccionar directamente a la página de pago
 * @return mixed
 */
function laclicsa_add_to_cart_redirect() {
	global $woocommerce;
	$checkout_url = wc_get_checkout_url();
	return $checkout_url;
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

/**
 * Modificamos los campos necesarios en la página de pago
 *
 * @param $fields
 *
 * @return mixed
 */
function custom_override_checkout_fields( $fields ) {
	unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_country']);
	unset($fields['billing']['billing_comments']);
	unset($fields['order']['order_comments']);
	//unset($fields['billing']['billing_address_1']);
	//unset($fields['billing']['billing_address_2']);
	//unset($fields['billing']['billing_city']);
	//unset($fields['billing']['billing_state']);
	//unset($fields['billing']['billing_postcode']);
	//unset($fields['billing']['billing_phone']);


	//$fields['billing']['billing_email']['class'] = ['form-row-ride'];

	return $fields;
}

add_filter( 'woocommerce_product_single_add_to_cart_text', 'laclicsa_custom_cart_button_text' );    // 2.1 +

function laclicsa_custom_cart_button_text() {

	return __( 'PAGAR EN LÍNEA', 'laclicsa' );

}

/**
 * Modifica los datos enviados a través del webhook
 * @param $item_id
 * @param $values
 */

/*function laclicsa_add_to_webhook($item_id, $item) {
	$key = 'member_plan';
	$userId = get_current_user_id();

	// Datos de la suscripción (si existe)
	$member = pms_get_member_subscriptions(array('user_id' => $userId));

	if(empty($member)){
		return;
	}

	$subscription = pms_get_subscription_plan($member[0]->subscription_plan_id);

	$value = ['plan' => $subscription->name, 'laclicsaID' => get_user_meta($userId, 'id_laclicsa', true)];
	wc_add_order_item_meta($item_id, $key, $value, true);
}
add_filter('woocommerce_new_order_item', 'laclicsa_add_to_webhook', 10, 3);*/


add_filter( 'woocommerce_checkout_fields' , 'laclicsa_override_checkout_fields' );
/**
 * Agrega el campo "recibir email" en la orden
 *
 * @param $fields
 *
 * @return mixed
 */
function laclicsa_override_checkout_fields( $fields ) {

    // Remove address fields
    unset(
        $fields['billing']['billing_address_1'],
        $fields['billing']['billing_address_2'],
        $fields['billing']['billing_city'],
        $fields['billing']['billing_state'],
        $fields['billing']['billing_postcode']
    );

    // Modify phone field wide
    $fields['billing']['billing_phone']['class'] = ['form-row-wide'];

    // Modify email field wide
    $fields['billing']['billing_email']['class'] = ['form-row-wide'];

	$fields['billing']['billing_promo'] = array(
		'label'     => __('Deseo recibir ofertas y promociones en mi correo electrónico', 'woocommerce'),
		'type'      => 'checkbox',
		'required'  => false,
		'clear'     => true,
		'class'     => ['form-row-wide']
	);

	return $fields;
}


add_action( 'woocommerce_admin_order_data_after_shipping_address', 'laclicsa_checkout_field_display_admin_order_meta', 10, 1 );
/**
 * Muestra el campo en la edición de la orden
 *
 * @param $order
 */
function laclicsa_checkout_field_display_admin_order_meta($order){
	echo '<p><strong>'.__('Recibe emails promocionales', 'laclicsa').':</strong> ' . get_post_meta( $order->get_id(), '_billing_promo', true ) . '</p>';
}



//add_action('woocommerce_webhook_payload', 'laclicsa_webhook_payload');
//
//function laclicsa_webhook_payload($payload) {
//
//	$user_info = get_userdata($payload["customer"]["id"]);
//	$userId = $payload["customer_id"];
//
//	// Datos de la suscripción (si existe)
//	$member = pms_get_member_subscriptions(array('user_id' => $userId));
//
//	if(empty($member)){
//		return;
//	}
//
//	$subscription = pms_get_subscription_plan($member[0]->subscription_plan_id);
//
//	$value = [
//		'plan' => $subscription->name,
//		'laclicsaID' => get_user_meta($userId, 'id_laclicsa', true),
//		'email' => get_post_meta( $payload['id'], '_billing_promo', true ),
//		'userID' => $userId
//	];
//
//	$payload["member"] = $value;
//
//	return $payload;
//}

/**
 * Agrega un campo para precios adicionales
 */
add_action( 'woocommerce_product_options_pricing', 'laclicsa_add_price_field' );

function laclicsa_add_price_field() {

    echo '<div class="options_group">';

    $field = [
        'id' => '_normal_price',
        'placeholder' => '0.00',
        'name' => '_normal_price',
        'data_type' => 'price',
        'label' => 'Precio normal Laclicsa:'
    ];

    woocommerce_wp_text_input( $field );

    echo '</div>';
}

/**
 * Almacena el precio normal laclicsa del producto
 */

add_action( 'woocommerce_process_product_meta', 'laclicsa_save_price' );

function laclicsa_save_price( $post_id ) {

    $price = $_POST['_normal_price'];

    if( !empty( $price ) )
        update_post_meta( $post_id, '_normal_price', esc_attr( $price ) );

}

/**
 * Muestra los tres precios laclicsa
 *
 * Este filtro asume que los productos publicados siempre contendrán
 * tres precios: precio normal, precio con descuento y precio en línea.
 * Si esta condición cambia, el filtro deberá ser actualizado también.
 */

add_filter( 'woocommerce_get_price_html', 'laclicsa_show_three_prices', 10, 2 );

function laclicsa_show_three_prices( $price, $product ){

    $price = '<table style="width: auto;">';
    $price .= '<tr class="normal-price"><td>Precio normal:</td><td><del>' . wc_price(get_post_meta($product->get_id(), '_normal_price', true)) . '</del></td></tr>';
    $price .= wc_format_sale_price($product->get_regular_price(), wc_get_price_to_display($product) );

    return $price;
}

/**
 * Modifica el formato HTML de los precios "normales"
 * de WooCommerce
 */
add_filter( 'woocommerce_format_sale_price', 'laclicsa_reformat_price', 10, 3 );

function laclicsa_reformat_price( $priceHTML, $regular, $sale ){

    ob_start(); ?>

        <tr class="regular-price">
            <td>Precio promoción:</td>
            <td><del><?php echo wc_price( $regular ); ?></del></td>
        </tr>
        <tr class="online-price">
            <td>Precio en línea:</td>
            <td><ins><?php echo wc_price($sale); ?>*</ins></td>
        </tr>
    </table>


    <?php
    $price = ob_get_clean();

    return $price;
}

add_filter( 'woocommerce_sale_flash', 'laclicsa_replace_ofert_text' );
function laclicsa_replace_ofert_text( $html ) {
    return str_replace( __( 'Sale!', 'woocommerce' ), __( 'Precio en Línea', 'laclicsa' ), $html );
}

// Add legend before cart and checkout
function lacs_before_cart() {
	echo '<div class="one-per-patient">';
	echo '<img src="' . get_stylesheet_directory_uri() . '/assets/images/info.svg" alt="Información Importante">'; 
	echo 'Recuerde que los estudios agregados a su carrito aplican para un solo paciente. Si desea adquirir el mismo estudio para pacientes distintos
		  deberá volver a realizar el proceso de pago por cada paciente deseado.';
	echo '</div>';
}


add_action('woocommerce_before_cart', 'lacs_before_cart');
add_action('woocommerce_before_checkout_form', 'lacs_before_cart');