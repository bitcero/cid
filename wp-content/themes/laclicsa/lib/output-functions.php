<?php
/**
 * Genera el menú superior
 */
function laclicsa_make_topmenu() {

	$buttons = get_field('top-buttons', 'option');
	$titleSuc = get_field( 'title-sucursales', 'option' );
	$sucursales = get_field( 'sucursales', 'option' );

	?>
	<div id="laclicsa-top-menu">

		<?php if(false == empty($buttons)): ?>
		<div class="buttons">
			<ul>
				<?php foreach($buttons as $button): ?>
				<li>
					<a href="<?php echo $button['link']; ?>">
						<?php if('' != $button['icon']): ?>
						<span class="icon icon-<?php echo $button['icon']; ?>"></span>
						<?php endif; ?>
						<?php echo $button['caption']; ?>
					</a>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php endif; ?>

		<?php if(get_field('sucursales-enabled', 'option') && false == empty($sucursales)): shuffle($sucursales); ?>
		<div class="sucursales">
            <span class="caption">
                <?php if('' == $titleSuc): ?>
                    <?php _e('Sucursales', 'laclicsa'); ?>
                <?php else: ?>
                    <?php echo $titleSuc; ?>
                <?php endif; ?>
            </span>
			<ul>
				<?php foreach($sucursales as $sucursal): ?>
				<li>
					<a href="<?php echo get_permalink($sucursal['link']->ID); ?>">
						<span class="name"><?php echo $sucursal['name']; ?></span>
						<span class="phone"><?php echo $sucursal['phone']; ?></span>
					</a>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php endif; ?>

        <?php
        $whatsapp = get_field('whatsapp', 'option');
        if($whatsapp):
        ?>
        <div class="top-button whatsapp-button">
            <a href="https://api.whatsapp.com/send?phone=<?php echo clean_phone_number(get_field("whatsapp-number", 'option')); ?>">
                <span class="caption"><?php echo get_field('whatsapp-caption', 'option'); ?></span>
                <span class="number"><?php echo get_field('whatsapp-number', 'option'); ?></span>
            </a>
        </div>
        <?php endif; ?>

        <?php
        $facebook = get_field('facebook', 'option');
        if($facebook):
        ?>
        <div class="top-button facebook-button">
            <a href="<?php echo get_field("facebook-link", 'option'); ?>">
                <span class="caption"><?php echo get_field('facebook-caption', 'option'); ?></span>
                <span class="brand"><?php _e('Facebook', 'laclicsa'); ?></span>
            </a>
        </div>
        <?php endif; ?>

        <div class="top-button customers-button">
            <a href="https://planlealtad.laclicsa.com.mx">
                <span class="caption">Consulta</span>
                <span class="brand">Resultados</span>
            </a>
        </div>

        <div class="top-button companies-button">
            <a href="https://accesoempresas.laclicsa.com.mx">
                <span class="caption">Empresas</span>
                <span class="brand">Facturación</span>
            </a>
        </div>

	</div>

    <button type="button" id="top-toggle-menu">
        <span></span>
        <span></span>
        <span></span>
    </button>

	<?php
}

add_action( 'woocommerce_single_product_summary', 'laclicsa_add_indications', 35 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
/**
 * Agrega las indicaciones a la página de detalles del producto.
 */
function laclicsa_add_indications() {

    ?>
    <div class="laclicsa-indications">
        <h3><?php _e('Indicaciones', 'laclicsa'); ?></h3>
        <p>Antes de presentarse al estudio por favor atienda las siguientes indicaciones:</p>
        <ul>
            <li>
                <?php
                $lines = get_field('indications' );
                echo str_replace("\n", '</li><li>', $lines );
                ?>
            </li>
        </ul>
    </div>

    <?php
    $tests = get_field('tests');

    if($tests):
    ?>

    <div class="laclicsa-tests">
        <h3><?php _e('Estudios Incluidos', 'laclicsa'); ?></h3>
        <?php

        echo '<ul class="laclicsa-included-tests">';
        foreach($tests as $test):
	        ?>
            <li>
                <a href="<?php echo get_permalink($test->ID); ?>"><?php echo $test->post_title; ?></a>
            </li>
	        <?php
        endforeach;
        echo '</ul>';
        ?>
    </div>
    <?php
    endif;
}
