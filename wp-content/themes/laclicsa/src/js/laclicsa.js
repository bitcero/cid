(function($){

    this.RotadorSucursales = function(){

        this.current = 0;
        this.items = null;
        _self = this;

        this.start = function(ele){

            if(ele.length <= 0){
                return false;
            }

            this.items = $(ele).find("li");
            if(this.items.length <= 0){
                return false;
            }

            this.next();

        };

        this.next = function(){

            if(_self.current >= _self.items.length){
                _self.current = 0;
            }

            //console.log("Mostrando " + _self.current);
            $(_self.items).removeClass('active');
            $(_self.items[_self.current]).addClass('active');
            this.current++;
            setTimeout(function(){
                //console.log("Se mostrará " + _self.current);
                _self.next();
            }, 5000);

        }

    };

    $(document).ready(function(){
        if($("#laclicsa-top-menu > .sucursales").length > 0){
            var rotator = new RotadorSucursales();
            rotator.start($("#laclicsa-top-menu > .sucursales > ul"));
        }

        $("#top-toggle-menu").click(function(){
            $("#laclicsa-top-menu").slideToggle(300);
        })
    });

    $("#primary-menu-toggle").click(function(){

        $(this).toggleClass('open');
        $("#genesis-nav-primary").slideToggle(500);

    });

    $("#laclicsa-search-nav #searchform-input")
        .focus(function(){

            //$(this).parents("form").width('auto');
            $(this).parents("li").addClass('active');

            //var items = $("#menu-menu-principal > li:nth-child(n+2)");
            //$(items).addClass('searching');

        })
        .blur(function(){

            $(this).parents('li').removeClass('active')

        });

    $(".laclicsa-page-empresas button").click(function(){

        var card = $(this).data('card');

        $(".laclicsa-empresas-contact .contact-card-overlay").fadeIn(300, function(){
            $("#contact-" + card).fadeIn(300);
        });
    });

    $(".laclicsa-empresas-contact .contact-card-overlay, .laclicsa-empresas-contact .contact-card .close").click(function(){

        $(".laclicsa-empresas-contact .contact-card:visible").fadeOut(250, function(){
            $(".laclicsa-empresas-contact .contact-card-overlay").fadeOut(250);
        });

    });

    $("#openpay-cards").click(function(){
        $("#payment-overlay").fadeIn(300,function(){
            $("#openpay-cards-modal").fadeIn(300)
        });
    });

    $("#openpay-stores").click(function(){
        $("#payment-overlay").fadeIn(300,function(){
            $("#openpay-stores-modal").fadeIn(300)
        });
    });

    $("#cancel-openpay, #cancel-openpay2").click(function(){
        $(".openpay-modal").fadeOut(200,function(){
            $("#payment-overlay").fadeOut(200);
        });
    });

    if($("#openpay-cards-modal").length > 0) {

        var success_callbak = function (response) {
            var token_id = response.data.id;
            $('#token_id').val(token_id);
            $('#payment-form').submit();
        };

        var error_callbak = function (response) {
            var desc = response.data.description != undefined ?
                response.data.description : response.message;
            alert("ERROR [" + response.status + "] " + desc);
            $("#pay-button").prop("disabled", false);
        };

        // Sandbox
        OpenPay.setId("mm7xybykr2ewi819rnnq");
        OpenPay.setApiKey("pk_8901360b0ae74e3186ed36ee55af0401");
        OpenPay.setSandboxMode(true);
        /*
        OpenPay.setId("mnoijjindkzr3qrjowud");
        OpenPay.setApiKey("pk_66cc789fc3be465cb4c616153cf457d5");
        OpenPay.setSandboxMode(false);
*/
        var deviceSession = OpenPay.deviceData.setup("payment-form", "deviceIdHiddenFieldName");

        $('#pay-button').on('click', function (event) {
            event.preventDefault();
            $("#pay-button").prop("disabled", true);
            OpenPay.token.extractFormAndCreate('payment-form', success_callbak, error_callbak);

        });
    }


})(jQuery);