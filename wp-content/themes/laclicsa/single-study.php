<?php

/**
 * Encabezado personalizado para los perfiles
 */
add_action('genesis_after_header', 'laclicsa_study_header');

function laclicsa_study_header() {
	?>
	<div id="laclicsa-page-title" class="with-quality">
		<div class="overlay">
			<div class="wrap">

				<h1>
					<small><?php _e('Estudios', 'laclicsa'); ?></small>
					<?php echo get_the_title(); ?>
				</h1>

                <div class="quality-logos">
                    <a href="<?php echo get_field('banner-url', 'option'); ?>">
                        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/iso17025-h.png'; ?>" alt="Laboratorio de prueba tercero autorizado">
                        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/iso15189-h.png'; ?>" alt="Primer laboratorio en México acreditado en ISO 15189:2012">
                    </a>
                </div>

			</div>
		</div>
	</div>
	<?php
}

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'laclicsa_study_loop' );

function laclicsa_study_loop() {

	the_post();

	$content = get_the_content();

	?>
    <span class="search-your-study">Busca tu estudio</span>
    <?php echo do_shortcode('[search_live placeholder="Buscar estudios..."]'); ?>
    <div class="not-find-message">
        ¿No encuentra el estudio que necesita?
        <a href="<?php echo get_field('contact-url', 'option' ); ?>">Contáctenos</a>
        y con gusto podremos ayudarle.
    </div>

    <div class="clearfix"></div>
    <?php

	if('' != trim($content)):
	?>
    <div class="four-sixths first">
        <h2><?php _e('Descripción', 'laclicsa'); ?></h2>

	    <?php echo $content; ?>
    </div>
    <?php endif; ?>

	<div class="<?php echo '' != trim($content) ? 'two-sixths' : ''; ?>">
		<h3><?php _e('Detalles del Estudio', 'laclicsa'); ?></h3>

        <?php
        // Current post categories IDS
        if ( $cIds = wp_get_post_terms( get_the_ID(), 'type' ) ){
            $cIds = wp_list_pluck( $cIds, 'term_id' );
        }
        // CS category
        $csCat = get_field( 'studys_cats', 'option' );
        $csCat = $csCat[0]['control'];
        ?>

		<table>
			<tbody>
            <?php if(get_field( 'price') && in_array( $csCat, $cIds ) == false): ?>
                <tr>
                    <td class="price">
                        <strong><?php _e('Precio', 'laclicsa'); ?></strong>
                        <span>
                            <?php echo '$' . number_format( get_field('price'), '2'); ?>*
                            <small><em>Incluye IVA</em></small>
                        </span>

                        <span class="price-legend">
                            *<?php echo get_field('price_legend', 'option'); ?>
                        </span>
                    </td>
                </tr>
            <?php endif; ?>

            <?php if(get_field('days') > 0): ?>
			<tr>
				<td>
					<strong><?php _e('Entrega de resultados', 'laclicsa'); ?></strong>
					<br>
					<?php echo sprintf(__('%u días', 'laclicsa'), get_field('days')); ?>
				</td>
			</tr>
            <?php endif; ?>
            <?php if(get_field('method') != ''): ?>
			<tr>
				<td>
					<strong><?php _e('Método aplicado:', 'laclicsa'); ?></strong>
					<br>
					<?php echo get_field('method'); ?>
				</td>
			</tr>
            <?php endif; ?>
            <?php if(get_field('instructions')): ?>
			<tr>
				<td>
                    <?php if(in_array( $csCat, $cIds )): ?>
                        <strong><?php _e('Indicaciones:', 'laclicsa'); ?></strong>
                    <?php else: ?>
                        <strong><?php _e('Indicaciones para el paciente:', 'laclicsa'); ?></strong>
                    <?php endif; ?>
					<br>
					<ul class="test-indications">
						<?php
						$guidelines = get_field('instructions');
						foreach($guidelines as $item):
						?>
						<li>
							<?php echo ucfirst(strtolower($item['text'])); ?>
						</li>
						<?php endforeach; ?>
					</ul>
				</td>
			</tr>
            <?php endif; ?>
			</tbody>
		</table>
	</div>

	<div class="clearfix"></div>

    <?php if(get_field('products')): ?>
	<h3>Productos Recomendados</h3>
	<p>Las siguientes promociones o productos contienen este estudio como parte del paquete.</p>

	<div class="page-on-sale">
		<?php
		$products = get_field('products');

		if(false == empty($products)):
		?>
		<ul class="products">
			<?php
			foreach($products as $object):
				$product = wc_get_product($object);
			?>
				<li class="product">
					<a href="<?php the_permalink($product->ID); ?>">
						<?php echo $product->get_image('shop_single'); ?>
						<?php if($product->is_on_sale()): ?>
							<span class="on-sale"><?php _e('Promoción', 'laclicsa'); ?></span>
						<?php endif; ?>
						<span class="title"><?php echo $product->get_title(); ?></span>
						<?php echo $product->get_price_html(); ?><br>
						<span class="button"><?php _e('Pago en Línea', 'laclicsa'); ?></span>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php
		endif;
		?>
	</div>
	<?php
    endif;
}

genesis();