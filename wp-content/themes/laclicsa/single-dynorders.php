<?php

add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'laclicsa_presupuesto_loop' );

function laclicsa_presupuesto_loop(){

    the_post();

    // Budget data
    $data = get_post_meta( get_the_ID(), 'body', true );

    //$s = isset($_GET)

    ?>

    <div id="budget-data">

        <h1>Presupuesto #<?php the_ID(); ?></h1>

        <div class="customer-data">
            <div class="data-item">
                <label>Nombre:</label>
                <div class="data-value">
                    <?php echo $data->customer->name . ' ' . $data->customer->lastname; ?>
                </div>
            </div>
            <div class="data-item">
                <label>Email:</label>
                <div class="data-value">
                    <?php echo $data->customer->email; ?>
                </div>
            </div>
            <div class="data-item">
                <label>Teléfono:</label>
                <div class="data-value">
                    <?php echo $data->customer->phone != '' ? $data->customer->phone : 'Not provided'; ?>
                </div>
            </div>
            <div class="data-item">
                <label>Fecha:</label>
                <div class="data-value">
                    <?php echo $data->date; ?>
                </div>
            </div>
        </div>

    </div>

    <?php

}

genesis();