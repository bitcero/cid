<?php
/**
 * Template Name: Promociones VRIM
 */

add_action('genesis_after_header', 'laclicsa_page_header');

// Replace standard loop
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'laclicsa_sucursales_loop' );

function laclicsa_sucursales_loop(){

    ?>

    <div id="promos-container" style="background-image: url(<?php echo get_field('vrim-bg', 'option'); ?>);">
        <div class="promos-wrap">
        <?php
        $args = [
            'post_type' => 'promo',
            'post_status' => 'publish',
            'posts_per_page' => 6
        ];
        $query = new WP_Query( $args );

        if($query->have_posts()){

            while($query->have_posts()){

                $query->the_post();

                ?>

                <div class="promo-item">
                    <div class="item-bg">
                        <h2 class="title<?php echo strlen(get_the_title()) < 15 ? ' big-title' : ''; ?>"><?php the_title(); ?></h2>
                        <span class="regular">Precio regular: $<?php echo get_field('normal-price'); ?></span>
                        <span class="price">
                            $<?php echo get_field('promo-price'); ?>
                            <?php if(get_field('iva')): ?>
                            <span class="iva">Con IVA</span>
                            <?php endif; ?>
                        </span>

                    </div>
                    <span class="valid">Vigencia: <?php echo get_field('until'); ?></span>
                </div>

                <?php


            }


        }
        ?>
        </div>
    </div>

    <?php

}

genesis();