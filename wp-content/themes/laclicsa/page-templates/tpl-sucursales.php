<?php
/**
 * Template Name: Todas las Sucursales
 */

add_action('genesis_after_header', 'laclicsa_page_header');

// Replace standard loop
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'laclicsa_sucursales_loop' );

function laclicsa_sucursales_loop(){

    ob_start(); ?>

    <div id="map-controls">
        <button type="button" data-location="cancun">Cancún</button>
        <button type="button" data-location="playa">Playa del Carmen</button>
				<button type="button" data-location="cozumel">Cozumel</button>
    </div>
    <div id="sucursales-map">

        <!--?php echo do_shortcode( get_field('home-map', 'option' ) ); ?-->

    </div>

    <script>

        var map;
        var centerCancun = {lat: 21.137215, lng: -86.833086};
        var centerPlaya = {lat: 20.625764, lng: -87.080557};
				var centerCozumel = {lat: 20.496972, lng: -86.942260};

        function initSucursales() {


            // Map object
            map = new google.maps.Map(document.getElementById('sucursales-map'), {
                zoom: 14,
                center: centerCancun
            });

            var markerIcon = "<?php echo get_field('marker', 'option'); ?>";
            var opened;

            // Add markers
            <?php
                $args = [
                    'post_type' => 'sucursales',
                    'post_status' => 'publish',
                    'posts_per_page' => 60
                ];
                $query = new WP_Query( $args );

                $markers = '';
                $infoWindows = '';
                $markersInfos = '';

                if($query->have_posts()){

                    $markers .= 'var markers = {';
                    $infoWindows .= 'var infoWindows = {';

                    while($query->have_posts()){

                        $query->the_post();

                        $markers .= 'marker_' . get_the_ID() . ': new google.maps.Marker({';
                        $markers .= 'position: {lat: ' . get_field('lat') . ', lng: ' . get_field('lon') . '},';
                        $markers .= 'map: map,';
                        $markers .= 'icon: markerIcon';

                        // Info Windows
                        $infoWindows .= str_replace("\n", '', 'info_' . get_the_ID() . ': new google.maps.InfoWindow({ content: \'<div class="sucursal-info"><div class="title">' . get_the_title() . ' <span class="fc-badge info"></span></div><div class="featured-image">' . get_the_post_thumbnail(get_the_ID(), 'thumbnail') . ' </div><address><b>Dirección: </b>' . str_replace(["\r\n", "\n"], ['', '<br />'], get_field('address')) . '</address><div class="phone"><strong>Teléfono:</strong> ' . get_field('phone') . '</div><p><a href="' . get_the_permalink() . '">Más Información »</a></p></div>\'}),');

                        $markers .= '}),';

                        $markersInfos .= 'markers.marker_' . get_the_ID() . '.addListener("click", function() { 
                        if(opened){ opened.close(); }
                        infoWindows.info_' . get_the_ID() . '.open(map, markers.marker_' . get_the_ID() . '); opened = infoWindows.info_' . get_the_ID() .'; });';

                    }

                    $markers .= '};';
                    $infoWindows .= '};';

                }

                echo $markers;
                echo $infoWindows;
                echo $markersInfos;
            ?>

        }

        jQuery(document).ready(function(){
            initSucursales();

            jQuery("#map-controls button").click(function(){
								var location = jQuery(this).data('location');
                if('cancun' == location){
                    map.setCenter(centerCancun);
										return;
                } 
								
								if('cozumel' == location){
										map.setCenter(centerCozumel);
										return;
								}
								
                map.setCenter(centerPlaya);
              
            });
        });

    </script>
    <?php

}

genesis();