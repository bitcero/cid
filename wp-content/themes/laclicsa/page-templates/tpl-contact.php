<?php
/**
 * Template Name: Formularios de Contacto
 */

/**
 * Encabezado personalizado para los perfiles
 */
add_action('genesis_after_header', 'laclicsa_contacts_header');

function laclicsa_contacts_header() {
	?>
	<div id="laclicsa-page-title" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);" class="with-quality">
		<div class="overlay">
			<div class="wrap">
				<h1>
					<small>Servicio Laclicsa</small>
					<?php echo get_the_title(); ?>
				</h1>

                <div class="quality-logos">
                    <a href="<?php echo get_field('banner_url', 'option'); ?>">
                        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/iso17025-h.png'; ?>" alt="Laboratorio de prueba tercero autorizado">
                        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/iso15189-h.png'; ?>" alt="Primer laboratorio en México acreditado en ISO 15189:2012">
                    </a>
                </div>
			</div>
		</div>
	</div>
	<?php
}

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'laclicsa_contact_loop' );

function laclicsa_contact_loop() {
	the_post();
	?>

	<div class="one-half first">
		<?php $image = get_field('image'); ?>

		<img src="<?php echo get_the_post_thumbnail_url(null, 'medium_large'); ?>" alt="<?php echo get_the_title(); ?>">

		<?php the_content(); ?>
	</div>

	<div class="one-half">
		<h3><?php _e('Contáctenos', 'laclicsa'); ?></h3>
		<?php echo get_field('intro'); ?>

		<?php echo do_shortcode(get_field('form')); ?>
	</div>

	<?php

}

genesis();