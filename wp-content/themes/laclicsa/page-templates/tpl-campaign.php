<?php
/**
 * Template Name: Campaña
 */
add_action('genesis_after_header', 'laclicsa_page_header');

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'laclicsa_companies_loop' );

function laclicsa_companies_loop() {
    the_post();
    ?>

    <div id="laclicsa-companies-page">

        <div class="two-thirds first">
            <h2>Promociones</h2>

            <?php
            $query = new WC_Product_Query( array(
                'limit' => 10,
                'orderby' => 'date',
                'order' => 'DESC',
                'status' => 'publish'
            ) );
            $products = $query->get_products();
            $index = 0;


                foreach($products as $product):
                    //$product = wc_get_product($id);
                    $image = wp_get_attachment_image_src( $product->image_id, 'shop-offer');
                    $index++;
            ?>

                    <div class="one-half <?php echo $index % 2 > 0 ? "first" : ""; ?>">
                        <div class="promo-container">
                            <a href="<?php echo get_permalink($product->get_id()); ?>">
                                <img src="<?php echo $image[0]; ?>" alt="<?php echo $product->name; ?>">
                            </a>
                            <h3><a href="<?php echo get_permalink($product->get_id()); ?>"><?php echo $product->name; ?></a></h3>
                            <table style="width: auto;" class="prices">
                                <tbody>
                                <tr class="normal-price">
                                    <td>Precio normal:</td>
                                    <td><del><?php echo wc_price(get_post_meta($product->get_id(), '_normal_price', true)); ?></del></td>
                                </tr>
                                <tr class="regular-price">
                                    <td>Precio promoción:</td>
                                    <td><del><?php echo wc_price($product->regular_price); ?></del></td>
                                </tr>
                                <tr class="online-price">
                                    <td>Precio en línea:</td>
                                    <td><ins><?php echo wc_price($product->price); ?></ins></td>
                                </tr>
                                </tbody>
                            </table>
                            <a href="<?php echo get_permalink($product->get_id()); ?>" class="btn">
                                Ver Detalles
                            </a>
                        </div>
                    </div>

            <?php
                endforeach;

            ?>

        </div>

        <div class="one-third">
            <h2>Sucursales</h2>

            <div class="sucursales-bar">
                <?php
                $query = new WP_Query( array(
                    'limit' => 3,
                    'orderby' => 'title',
                    'order' => 'DESC',
                    'status' => 'publish',
                    'post_type' => 'sucursales',
                    'post__in' => [4637,13329,4605],
                    'post_ty'
                ) );

                if($query->have_posts()):
                    while($query->have_posts()):
                        $query->the_post();
                        ?>
                        <div class="sucursal-container">
                            <a href="<?php echo get_the_permalink(); ?>"><img src="<?php echo get_field('map'); ?>"></a>
                            <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo the_title(); ?></a></h3>
                            <p><?php echo get_field('address'); ?></p>
                            <a href="<?php echo get_the_permalink(); ?>" class="btn">Más Información</a>
                        </div>

                    <?php
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>

    <?php

}

genesis();