<?php
/**
 * Template Name: Calidad
 */


add_action('genesis_after_header', 'laclicsa_page_header');

/**
 * Imagen destacada.
 * Se activa o desactiva desde el formulario de creación de la página
 */
if(get_field('featured-image', get_the_ID())){
    add_action( 'genesis_before_entry', 'featured_post_image', 8 );
    function featured_post_image() {
        if ( ! is_singular( 'page' ) )  return;
        the_post_thumbnail('post-image');
    }
}

/**
 * Contenido de la página
 */


/**
 * Shortcode para ISO 17025
 */
add_shortcode('iso-info', 'laclicsa_iso_info');

function laclicsa_iso_info( $args, $content ){

    $atts = shortcode_atts(['iso' => ''], $args );

    if('' == $atts['iso'])
        return '';

    ob_start(); ?>

    <div id="iso<?php echo $atts['iso']; ?>" class="quality-sections">
        <div class="two-sixths first">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/iso'.$atts['iso'].'.png'; ?>" alt="Laboratorio de preuba tercero autorizado">
        </div>
        <div class="four-sixths">
            <?php echo $content; ?>

            <?php
            $docs = get_field('docs-' . $atts['iso'], 'option');
            if($docs):
                ?>
                <h4><?php _e('Acreditaciones', 'laclicsa'); ?></h4>
                <ul>
                    <?php foreach($docs as $doc): ?>
                        <li>
                            <a href="<?php echo $doc['url']; ?>" target="_blank"><?php echo $doc['title']; ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php
            endif;
            ?>
        </div>
    </div>

    <?php
    return ob_get_clean();

}

genesis();