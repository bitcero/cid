<?php
/**
 * Template Name: Páginas de Empresas
 */
add_action('genesis_after_header', 'laclicsa_page_header');

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'laclicsa_companies_loop' );

function laclicsa_companies_loop() {
	the_post();
	?>

	<div id="laclicsa-companies-page">
        <div class="one-half first">
            <?php $image = get_field('image'); ?>

            <img src="<?php echo get_the_post_thumbnail_url(null, 'medium_large'); ?>" alt="<?php echo get_the_title(); ?>">

        </div>

        <div class="one-half">
            <article class="entry-content">
                <?php the_content(); ?>
            </article>
        </div>
    </div>

	<?php

}

genesis();