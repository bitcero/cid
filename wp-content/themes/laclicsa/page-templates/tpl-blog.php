<?php
/**
 * Template Name: Blog Laclicsa
 */

// The blog page loop logic is located in lib/structure/loops.php.

add_action('genesis_after_header', 'laclicsa_page_header');

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'laclicsa_blog_loop' );

function laclicsa_blog_loop() {

    global $wp_query;

    // Get posts
    $wp_query = new WP_Query( [
        'post_type' => 'post',
        'posts_per_page' => 5
    ] );
    
    if ( have_posts() ){
        while ( have_posts() ): the_post();
        ?>

            <article <?php post_class('blog-article'); ?>>
                <aside>
                    <a href="<?php the_permalink(); ?>">
                        <?php echo the_post_thumbnail( 'blog-thumbnail' ); ?>
                    </a>
                </aside>
                <header>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <div class="meta">
                        <?php echo the_date('l j \d\e F, Y'); ?>
                    </div>
                </header>
                <section>
                    <?php the_excerpt(); ?>

                    <a href="<?php the_permalink(); ?>" class="more-button">
                        Leer Más
                    </a>
                </section>
            </article>

        <?php
        endwhile;

        do_action( 'genesis_after_endwhile' );

    }

}

// Add class blog to page
add_filter( 'body_class', 'laclicsa_add_blog_class' );

function laclicsa_add_blog_class( $classes ){
    $classes[] = 'laclicsa-blog';
    return $classes;
}

function blog_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'blog_excerpt_length', 999 );

genesis();