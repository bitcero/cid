<?php
/**
 * Template Name: Portada Laclicsa
 */

add_filter('wpgmp_location_criteria', 'laclicsa_map_location_criteria',1,2 );

function laclicsa_map_location_criteria($location_criteria,$map) {

//This will apply for map id 1.
	if( $map->map_id == 1) {
		$location_criteria['show_all_locations'] = true;
	}
	return $location_criteria;
}

add_filter( 'genesis_markup_site-inner', '__return_null' );
add_filter( 'genesis_markup_content-sidebar-wrap_output', '__return_false' );
add_filter( 'genesis_markup_content', '__return_null' );

/**
 * Agregar el slider después del encabezado.
 * El funcionamiento de esta característica depende de la integración
 * de un plugin para slider (como LayerSlider o RevSlider) que proporcione
 * shortcodes
 */
add_action('genesis_after_header', 'laclicsa_show_homepage');

function laclicsa_show_homepage(){

	?>

    <div id="home-slider">
        <?php echo do_shortcode(get_field('home-slider', 'option')); ?>
        <?php echo do_shortcode(get_field('home-slider-mobile', 'option')); ?>
    </div>

	<section id="main-services">
        <div class="site-inner">
	        <?php
	        $services = get_field('home-services', 'option');
	        foreach($services as $index => $service):
		        ?>
                <div class="service-column">
                    <div class="service-item">
                        <a href="<?php echo $service['page']; ?>" class="icon-wrapper">
                            <span class="icon icon-<?php echo $service['icon']; ?>"></span>
                        </a>
                        <h2><a href="<?php echo $service['page']; ?>"><?php echo $service['name']; ?></a></h2>
                        <p><?php echo $service['description']; ?></p>
                    </div>
	                <?php
	                if($service['button'] != ''):
		                ?>
                        <a href="<?php echo $service['page']; ?>" class="button">
			                <?php echo $service['button']; ?>
                        </a>
	                <?php endif; ?>
                </div>

                <?php
                if(1 == $index){
                    echo '<div class="clearfix"></div>';
                }

                ?>

		        <?php
	        endforeach;
	        ?>
        </div>
		<div class="clearfix"></div>
	</section>

	<?php

    if(get_field('banner', 'option')): ?>

        <a id="laclicsa-home-banner" href="<?php echo '' != get_field('banner-url', 'option') ? get_field('banner-url', 'option') : '#'; ?>">
            <div class="wrap">
                <?php if(get_field('banner-show-logo', 'option')): ?>
                    <img src="<?php echo get_field('banner-logo', 'option'); ?>" alt="<?php bloginfo('name'); ?>">
                <?php endif; ?>
                <p><?php echo get_field('banner-text', 'option' ); ?></p>
                <div class="isos">
                    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/iso17025.png'; ?>" alt="Laboratorio de prueba tercero autorizado">
                    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/iso15189.png'; ?>" alt="Primer laboratorio en México acreditado en ISO 15189:2012">
                </div>
            </div>
        </a>

    <?php
    endif;
    ?>

    <section id="lacs-reviews">
        <h2>¿Que opinan nuestros clientes?</h2>
        <div class="wrap">
        <?php genesis_widget_area( 'facebook-widget', array(
		'before' => '<div class="reviews-widget widget-area">',
		'after'  => '</div>',
    ) ); ?>
        </div>
    </section>

    <div id="lacs-video" style="background-image: url(<?php echo get_field('video-bg', 'option'); ?>);">
        <div class="video-container">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe src="https://www.youtube.com/embed/4WGcZQT-m_k?rel=0&amp;showinfo=0" frameborder="0"
                        class="embed-responsive-item"
                        allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <?php
    echo do_shortcode('[put_wpgm id=1]');
    ?>

    <div id="lacs-promo">
        <a href="<?php echo get_field("promo-url", 'option'); ?>">
            <img src="<?php echo get_field('promo', 'option'); ?>">
        </a>
    </div>

    <?php
}

remove_action( 'genesis_loop', 'genesis_do_loop' );

genesis();