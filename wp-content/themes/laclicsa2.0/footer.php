
    </main>
</div>

    <footer id="l-footer">
        <div class="container">
            <div class="footer-widgets d-md-flex align-items-start justify-content-center">

                <div class="widget-item widget-menu">
                    <div class="links">
                        <h4>Navegación</h4>
                        <?php wp_nav_menu(['theme_location'  => 'footer']); ?>

                        <h4>Empresas</h4>
                        <?php wp_nav_menu(['theme_location'  => 'companies']); ?>
                    </div>

                    <div class="links">
                        <h4>Contacto</h4>
                        <?php wp_nav_menu(['theme_location'  => 'contact']); ?>

                        <h4>Nosotros</h4>
                        <?php wp_nav_menu(['theme_location'  => 'about']); ?>
                    </div>
                </div>

                <div class="widget-item widget-promos">
                    <h4>Promociones</h4>
                    <ul>
                    <?php
                    $query = new WC_Product_Query( array(
                        'limit' => 3,
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'status' => 'publish',
                    ) );
                    $products = $query->get_products();
                    foreach($products as $product):
                    ?>
                        <li class="d-flex align-items-center">
                            <div class="image">
                                <a href="<?php echo $product->get_permalink(); ?>">
                                    <?php echo $product->get_image('thumbnail'); ?>
                                </a>
                            </div>
                            <div class="content">
                                <a href="<?php echo $product->get_permalink(); ?>" class="name">
                                    <?php echo $product->get_name(); ?>
                                </a>
                                <span class="price">
                                    $<?php echo number_format($product->get_price(), 2); ?>
                                    <small>Precio en línea</small>
                                </span>
                                <span class="description">
                                    <?php echo $product->get_description(); ?>
                                </span>
                            </div>
                        </li>
                    <?php
                    endforeach;
                    ?>
                    </ul>
                </div>

                <div class="widget-item widget-branches">
                    <h4>Sucursales Laclicsa</h4>
                    <?php
                    $theBranches = new WP_Query([
                        'post_type' => 'sucursales',
                        'posts_per_page' => 20,
                    ]);
                    ?>
                    <ul>
                        <?php
                        if($theBranches->have_posts()):
                            while($theBranches->have_posts()): $theBranches->the_post();
                                ?>
                                <li><?php the_title(); ?></li>
                            <?php
                            endwhile;
                        endif;
                        ?>
                    </ul>
                </div>

            </div>
        </div>

        <div class="footer-copy">
            <div class="container">
                <p>Copyright © 2017. Laboratorios Laclicsa®. Todos los derechos reservados.</p>
            </div>
        </div>
    </footer>
<?php
wp_footer();
?>
</body></html>
