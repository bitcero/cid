<?php

/**
 * Configurración de scripts y estilos
 */
function laclicsa_scripts() {
    wp_enqueue_style( 'laclicsa-style', get_stylesheet_directory_uri() . '/assets/css/styles.min.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'slider-font', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,900' );
    wp_enqueue_script( 'laclicsa-js', get_stylesheet_directory_uri() . '/assets/js/bootstrap.bundle.min.js');

    wp_dequeue_style('ls-google-fonts');
    wp_deregister_style('ls-google-fonts');
    wp_dequeue_style('simple-job-board-google-fonts');
    wp_deregister_style('simple-job-board-google-fonts');
    wp_dequeue_style('simple-job-board-font-awesome');
    wp_deregister_style('simple-job-board-font-awesome');
    wp_dequeue_style('simple-job-board-jquery-ui');
    wp_deregister_style('simple-job-board-jquery-ui');
    wp_dequeue_style('simple-job-board-frontend');
    wp_deregister_style('simple-job-board-frontend');
    wp_dequeue_style('fbrev_css');
    wp_deregister_style('fbrev_css');

}

add_action( 'wp_enqueue_scripts', 'laclicsa_scripts' );

// Remover estilos y script sinnecesarios
function laclicsa_remove_styles() {
    wp_dequeue_style('simple-job-board-google-fonts-css');
    wp_deregister_style('simple-job-board-google-fonts-css');
    wp_dequeue_style('simple-job-board-font-awesome-css');
}
add_action( 'wp_print_styles', 'laclicsa_remove_styles',9999 );

// Includes
include("lib/theme-setup.php");
include("lib/format-functions.php");
include("lib/theme-options.php");
include("lib/theme-menu.php");
include("lib/custom-posts.php");