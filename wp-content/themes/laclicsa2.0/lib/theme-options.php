<?php
function laclicsa_theme_options() {

    // Verificamos que ACF esté instalado
    if( function_exists('acf_add_options_page') ) {

        acf_add_options_page(array(
            'page_title' 	=> __('Opciones Generales de Laclicsa', 'laclicsa'),
            'menu_title'	=> __('Opciones Laclicsa', 'laclicsa'),
            'menu_slug' 	=> 'laclicsa-general-settings',
            'capability'	=> 'edit_posts',
            'redirect'		=> false,
            'position'      => 58.95,
            'icon_url'      => 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDE4IDE4IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAxOCAxOCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8cGF0aCBmaWxsPSIjOUVBM0E4IiBkPSJNMTcuMywzLjdjMC4xLDAuNSwwLjMsMS43LDAuMywyLjJjMCw1LjMtNC4yLDkuNC05LjMsOS40Yy0zLjYsMC02LjctMi04LjMtNS4xTDAsMTAuMQ0KCQljMS4xLDMuNyw0LjgsNi4zLDguOCw2LjNjNS4yLDAsOS40LTQuMyw5LjItOS41QzE4LDUuOCwxNy44LDUuMiwxNy4zLDMuN0wxNy4zLDMuN3oiLz4NCjwvZz4NCjxwYXRoIGZpbGw9IiM5RUEzQTgiIGQ9Ik00LjgsNy4yQzMuNCw3LjYsMi44LDguMSwyLjksOC41QzMsOS4yLDUuOCw5LjIsOSw4LjVjMy4zLTAuOCw1LjItMS44LDUuMi0yLjVjMC0wLjUtMC44LTAuNS0yLjYtMC41DQoJYzItMC4yLDMuMywwLDMuNSwwLjVjMC4yLDAuOC0yLjEsMS44LTUuNywyLjdjLTMuNSwwLjgtNi43LDAuOC02LjgsMEMyLjQsOC4yLDMuMyw3LjcsNC44LDcuMkw0LjgsNy4yeiIvPg0KPHBhdGggZmlsbD0iIzlFQTNBOCIgZD0iTTUuNiw5LjlDNSwxMS4yLDUuMSwxMS44LDUuMywxMmMwLjUsMC40LDIuNS0xLjYsNC4yLTQuNGMxLjgtMi45LDIuNC00LjksMS45LTUuNGMtMC40LTAuNC0wLjksMC4xLTIuMSwxLjQNCgljMS4yLTEuNywyLjMtMi4zLDIuOC0yYzAuNywwLjQtMC4yLDIuNi0yLjEsNS44Yy0xLjksMy4xLTMuOSw1LjMtNC44LDQuOUM0LjcsMTIuMiw0LjksMTEuMyw1LjYsOS45TDUuNiw5Ljl6Ii8+DQo8cGF0aCBmaWxsPSIjOUVBM0E4IiBkPSJNOS45LDEwLjhjMSwxLDEuNiwxLjEsMS44LDFjMC42LTAuMy0wLjYtMi45LTIuNi01LjVjLTItMi43LTMuNy00LTQuMy0zLjdDNC4zLDIuOCw0LjYsMy40LDUuNCw1LjENCglDNC4zLDMuMyw0LDIuMSw0LjUsMS43YzAuNi0wLjUsMi40LDEuMiw0LjYsNGMyLjMsMi45LDMuNSw1LjUsMi45LDYuMkMxMS42LDEyLjQsMTAuOSwxMS45LDkuOSwxMC44TDkuOSwxMC44eiIvPg0KPGVsbGlwc2UgZmlsbD0iIzlFQTNBOCIgY3g9IjguNyIgY3k9IjcuMyIgcng9IjAuNiIgcnk9IjAuNiIvPg0KPC9zdmc+DQo='
        ));

        /*acf_add_options_sub_page(array(
            'page_title' 	=> __('Portada', 'laclicsa'),
            'menu_title'	=> __('Portada', 'laclicsa'),
            'parent_slug'	=> 'umi-general-settings',
        ));

        acf_add_options_sub_page(array(
            'page_title' 	=> __('Redes Social', 'laclicsa'),
            'menu_title'	=> __('Social', 'laclicsa'),
            'parent_slug'	=> 'umi-general-settings',
        ));*/

    }

}

add_action( 'init', 'laclicsa_theme_options' );