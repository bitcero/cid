<?php

function laclicsa_custom_post_types() {

    /**
     * Custom Post Types para Sucursales
     */
    $labels = array(
        'name'               => __( 'Sucursales', 'laclicsa' ),
        'singular_name'      => __( 'Sucursal', 'laclicsa' ),
        'menu_name'          => __( 'Sucursales', 'laclicsa' ),
        'all_items'          => __( 'Todas las Sucursales', 'laclicsa' ),
        'view_item'          => __( 'Ver Sucursal', 'laclicsa' ),
        'add_new_item'       => __( 'Agregar Sucursal', 'laclicsa' ),
        'add_new'            => __( 'Nueva Sucursal', 'laclicsa' ),
        'edit_item'          => __( 'Editar Sucursal', 'laclicsa' ),
        'update_item'        => __( 'Actualizar Sucursal', 'laclicsa' ),
        'search_items'       => __( 'Buscar Sucursales', 'laclicsa' ),
        'not_found'          => __( 'No se encontró', 'laclicsa' ),
        'not_found_in_trash' => __( 'No se encontró en la Papelera', 'laclicsa' ),
    );

    $args = array(
        'labels'              => $labels,
        'supports'            => array( 'title', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'menu_icon'           => 'dashicons-admin-multisite',
        'rewrite'             => array( 'slug' => 'sucursal' ),
    );

    register_post_type( 'sucursales', $args );

    /**
     * Custom post type para Estudios
     */

    # Categorías personalizadas
    $labels = array(
        'name'              => __( 'Tipos', 'laclicsa' ),
        'singular_name'     => __( 'Tipo', 'textdomain' ),
        'search_items'      => __( 'Buscar Tipos de Estudios', 'textdomain' ),
        'all_items'         => __( 'Todos los Tipos', 'textdomain' ),
        'parent_item'       => __( 'Tipo Superior', 'textdomain' ),
        'parent_item_colon' => __( 'Tipo Superior:', 'textdomain' ),
        'edit_item'         => __( 'Editar Tipo', 'textdomain' ),
        'update_item'       => __( 'Actualizar Tipo', 'textdomain' ),
        'add_new_item'      => __( 'Agregar Tipo', 'textdomain' ),
        'new_item_name'     => __( 'Nuevo Tipo', 'textdomain' ),
        'menu_name'         => __( 'Tipo', 'textdomain' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'tipo' ),
    );

    register_taxonomy( 'type', array( 'study' ), $args );

    $labels = array(
        'name'               => __( 'Estudios', 'laclicsa' ),
        'singular_name'      => __( 'Estudio', 'laclicsa' ),
        'menu_name'          => __( 'Estudios', 'laclicsa' ),
        'all_items'          => __( 'Todos los Estudios', 'laclicsa' ),
        'view_item'          => __( 'Ver Estudio', 'laclicsa' ),
        'add_new_item'       => __( 'Agregar Estudio', 'laclicsa' ),
        'add_new'            => __( 'Nuevo Estudio', 'laclicsa' ),
        'edit_item'          => __( 'Editar Estudio', 'laclicsa' ),
        'update_item'        => __( 'Actualizar Estudio', 'laclicsa' ),
        'search_items'       => __( 'Buscar Estudios', 'laclicsa' ),
        'not_found'          => __( 'No se encontró', 'laclicsa' ),
        'not_found_in_trash' => __( 'No se encontró en la Papelera', 'laclicsa' ),
    );

    $args = array(
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'custom-fields', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'menu_icon'           => 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDIwIDIwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAyMCAyMDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4NCgkuc3Qwe2ZpbGw6IzlFQTNBODt9DQo8L3N0eWxlPg0KPGc+DQoJPGc+DQoJCTxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0xNi4xLDYuOGMtMC43LTEuNC0xLjktMi41LTMuMy0zLjJsMC4zLTAuOEwxMy43LDNMMTQuMiwyTDkuNSwwTDksMS4xbDAuNiwwLjJMNy40LDYuNWwwLjUsMC4yTDcuMiw4LjMNCgkJCWwyLjYsMS4xbDAuNi0xLjZMMTEsOGwwLjYtMS40YzEuMiwwLjcsMiwxLjksMi4yLDMuM2MwLDAsMCwwLDAsMGMwLjctMC4zLDEuNS0wLjMsMi4zLDBjMC40LDAuMSwwLjcsMC40LDAuOSwwLjYNCgkJCUMxNyw5LjIsMTYuNyw3LjksMTYuMSw2Ljh6Ii8+DQoJPC9nPg0KPC9nPg0KPGc+DQoJPGc+DQoJCTxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0xNC45LDE2LjloLTEuNWMwLjYtMC40LDEuMS0wLjgsMS41LTEuM2MwLDAsMCwwLDAsMGMtMC40LDAtMC44LTAuMS0xLjEtMC4yYy0wLjgtMC4zLTEuMy0wLjktMS42LTEuNg0KCQkJYy0xLjIsMS0zLDEuMy00LjUsMC43bC0yLjktMS4ybDAuNS0xLjJsNCwxLjdsMC40LTEuMWwtNi4xLTIuNWwtMC40LDEuMWwxLDAuNGwtMC41LDEuMmwtMS4yLDNsMi41LDFjLTEuNCwwLTIuNSwxLjEtMi41LDIuNVYyMA0KCQkJaDE1di0wLjZDMTcuNSwxOCwxNi4zLDE2LjksMTQuOSwxNi45eiIvPg0KCTwvZz4NCjwvZz4NCjxnPg0KCTxnPg0KCQk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMTYuNiwxMS45Yy0wLjItMC40LTAuNS0wLjgtMS0xYy0wLjQtMC4yLTAuOS0wLjItMS40LDBjLTAuNCwwLjItMC44LDAuNS0xLDFjLTAuNCwwLjksMC4xLDIsMSwyLjQNCgkJCWMwLjksMC40LDItMC4xLDIuNC0xQzE2LjgsMTIuOSwxNi44LDEyLjQsMTYuNiwxMS45eiIvPg0KCTwvZz4NCjwvZz4NCjwvc3ZnPg0K',
        'rewrite'             => array( 'slug' => 'estudios' ),
        'taxonomies'          => array( 'type' ),
    );

    register_post_type( 'study', $args );

    /**
     * Registro de Perfiles
     * Los perfiles agruparan pruebas (estudios) dados de alta previamente
     */
    # Categorías personalizadas
    $labels = array(
        'name'              => __( 'Tipos', 'laclicsa' ),
        'singular_name'     => __( 'Tipo', 'textdomain' ),
        'search_items'      => __( 'Buscar Tipos de Estudios', 'textdomain' ),
        'all_items'         => __( 'Todos los Tipos', 'textdomain' ),
        'parent_item'       => __( 'Tipo Superior', 'textdomain' ),
        'parent_item_colon' => __( 'Tipo Superior:', 'textdomain' ),
        'edit_item'         => __( 'Editar Tipo', 'textdomain' ),
        'update_item'       => __( 'Actualizar Tipo', 'textdomain' ),
        'add_new_item'      => __( 'Agregar Tipo', 'textdomain' ),
        'new_item_name'     => __( 'Nuevo Tipo', 'textdomain' ),
        'menu_name'         => __( 'Tipo', 'textdomain' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'tipos-perfiles' ),
    );

    register_taxonomy( 'profile_type', array( 'profile' ), $args );

    $labels = array(
        'name'               => __( 'Perfiles', 'laclicsa' ),
        'singular_name'      => __( 'Perfil', 'laclicsa' ),
        'menu_name'          => __( 'Perfiles', 'laclicsa' ),
        'all_items'          => __( 'Todos los Perfiles', 'laclicsa' ),
        'view_item'          => __( 'Ver Perfil', 'laclicsa' ),
        'add_new_item'       => __( 'Agregar Perfil', 'laclicsa' ),
        'add_new'            => __( 'Nuevo Perfil', 'laclicsa' ),
        'edit_item'          => __( 'Editar Perfil', 'laclicsa' ),
        'update_item'        => __( 'Actualizar Perfil', 'laclicsa' ),
        'search_items'       => __( 'Buscar Perfiles', 'laclicsa' ),
        'not_found'          => __( 'No se encontró', 'laclicsa' ),
        'not_found_in_trash' => __( 'No se encontró en la Papelera', 'laclicsa' ),
    );

    $args = array(
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'menu_icon'           => 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDIwIDIwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAyMCAyMDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4NCgkuc3Qwe2ZpbGw6IzlFQTNBODt9DQo8L3N0eWxlPg0KPHBhdGggY2xhc3M9InN0MCIgZD0iTTE2LjcsMTMuMmMtMC41LDAtMSwwLjEtMS41LDAuM2wtNC40LTIuOVY4LjJjMS45LTAuNCwzLjMtMiwzLjMtNGMwLTIuMi0xLjgtNC00LTRTNiwyLDYsNC4zDQoJYzAsMS45LDEuMywzLjUsMy4xLDMuOXYyLjRsLTQuNCwyLjljLTAuNC0wLjItMC45LTAuMy0xLjUtMC4zYy0xLjgsMC0zLjMsMS41LTMuMywzLjNjMCwxLjgsMS41LDMuMywzLjMsMy4zczMuMy0xLjUsMy4zLTMuMw0KCWMwLTAuNy0wLjItMS4zLTAuNS0xLjhsMy45LTIuNWMwLDAsMCwwLDAuMSwwczAsMCwwLjEsMGwzLjksMi41Yy0wLjMsMC41LTAuNSwxLjEtMC41LDEuOGMwLDEuOCwxLjUsMy4zLDMuMywzLjMNCgljMS44LDAsMy4zLTEuNSwzLjMtMy4zQzIwLDE0LjcsMTguNSwxMy4yLDE2LjcsMTMuMnoiLz4NCjwvc3ZnPg0K',
        'rewrite'             => array( 'slug' => 'perfiles' )
    );

    // Registering your Custom Post Type
    register_post_type( 'profile', $args );

    /**
     * Promos Empresas
     */
    $labels = array(
        'name'               => __( 'Promociones', 'laclicsa' ),
        'singular_name'      => __( 'Promoción', 'laclicsa' ),
        'menu_name'          => __( 'Promociones', 'laclicsa' ),
        'all_items'          => __( 'Todas las Prmociones', 'laclicsa' ),
        'view_item'          => __( 'Ver Promoción', 'laclicsa' ),
        'add_new_item'       => __( 'Agregar Promoción', 'laclicsa' ),
        'add_new'            => __( 'Nueva Promoción', 'laclicsa' ),
        'edit_item'          => __( 'Editar Promoción', 'laclicsa' ),
        'update_item'        => __( 'Actualizar Promo', 'laclicsa' ),
        'search_items'       => __( 'Buscar Promociones', 'laclicsa' ),
        'not_found'          => __( 'No se encontró', 'laclicsa' ),
        'not_found_in_trash' => __( 'No se encontró en la Papelera', 'laclicsa' ),
    );

    $args = array(
        'labels'              => $labels,
        'supports'            => array( 'title', 'thumbnail', 'custom-fields', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'menu_icon'           => 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj48cGF0aCBmaWxsPSIjODI4NzhDIiBkPSJNMTkuMDM4IDIuMjk0SDQuMjIxYS45Ni45NiAwIDAgMC0uODYxLjUzM0wuMjAzIDkuMTM4YTEuOTMgMS45MyAwIDAgMCAwIDEuNzI0bDMuMTU2IDYuMzEzYS45NjMuOTYzIDAgMCAwIC44NjEuNTMyaDE0LjgxN2EuOTY0Ljk2NCAwIDAgMCAuOTYzLS45NjNWMy4yNTdhLjk2My45NjMgMCAwIDAtLjk2Mi0uOTYzek03Ljk2IDUuNjY2YTEuOTI2IDEuOTI2IDAgMSAxIDAgMy44NTEgMS45MjYgMS45MjYgMCAwIDEgMC0zLjg1MXptNS43OCA5LjYzMmExLjkyNyAxLjkyNyAwIDEgMSAwLTMuODUzIDEuOTI3IDEuOTI3IDAgMCAxIDAgMy44NTN6bS02LjM0NCAwbC0xLjM2Mi0xLjM2MiA4LjI3MS04LjI3IDEuMzYyIDEuMzYyLTguMjcxIDguMjd6Ii8+PC9zdmc+',
        'rewrite'             => array( 'slug' => 'vrim' )
    );

    // Registering your Custom Post Type
    register_post_type( 'promo', $args );

    /**
     * Custom Post Types para Testimoniales
     */
    $labels = array(
        'name'               => __( 'Testimonios', 'laclicsa' ),
        'singular_name'      => __( 'Testimonio', 'laclicsa' ),
        'menu_name'          => __( 'Testimoniales', 'laclicsa' ),
        'all_items'          => __( 'Testimoniales', 'laclicsa' ),
        'view_item'          => __( 'Ver Testimonio', 'laclicsa' ),
        'add_new_item'       => __( 'Agregar Testimonio', 'laclicsa' ),
        'add_new'            => __( 'Nuevo Testimonio', 'laclicsa' ),
        'edit_item'          => __( 'Editar Testiminio', 'laclicsa' ),
        'update_item'        => __( 'Actualizar Testiminio', 'laclicsa' ),
        'search_items'       => __( 'Buscar Testimoniales', 'laclicsa' ),
        'not_found'          => __( 'No se encontró', 'laclicsa' ),
        'not_found_in_trash' => __( 'No se encontró en la Papelera', 'laclicsa' ),
    );

    $args = array(
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'thumbnail' ),
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 30,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type'     => 'post',
        'menu_icon'           => 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCI+PHBhdGggZmlsbD0iIzgyODc4QyIgZD0iTTcuMTM0IDYuODcxYTIuNTU0IDIuNTU0IDAgMSAwLTMuNjEzIDMuNjEgMi41NTQgMi41NTQgMCAwIDAgMy42MTMtMy42MXpNNS40NjUgMTIuMDY1aC0uMjcyQTUuMjA3IDUuMjA3IDAgMCAwIDAgMTcuMjU5di4zNjdjMCAuMjAxLjE2My4zNjQuMzY1LjM2NGg5LjkyN2EuMzY0LjM2NCAwIDAgMCAuMzY1LS4zNjR2LS4zNjdhNS4yMTEgNS4yMTEgMCAwIDAtNS4xOTItNS4xOTR6TTE4LjI2NyAzLjI0OEE2Ljg0NiA2Ljg0NiAwIDAgMCAxNC4yMyAyLjAxYTYuODQ4IDYuODQ4IDAgMCAwLTQuMDM3IDEuMjM4Yy0xLjA3Mi43ODktMS43MzMgMS44ODctMS43MzMgMy4xMS4wMDEuNTUzLjEzNyAxLjA5OC4zOTUgMS41ODcuMjMuNDM2LjUzNC44MjkuODk5IDEuMTYxbC0uOTE2IDEuOTAzYS4zNjIuMzYyIDAgMCAwIC41MTkuNDY0bDIuMTA2LTEuM2MuNC4xNjQuODE1LjI5IDEuMjQuMzc1YTcuNTI1IDcuNTI1IDAgMCAwIDEuNTI2LjE1NCA2Ljg1MiA2Ljg1MiAwIDAgMCA0LjAzNy0xLjIzN0MxOS4zMzkgOC42NzcgMjAgNy41NzkgMjAgNi4zNTZzLS42NjMtMi4zMTktMS43MzMtMy4xMDh6bS0xLjMwMSAyLjUyNGwtLjAwMS4wMDJhLjM2NC4zNjQgMCAwIDEtLjA5LjE3bC0uOTM2IDEuMDcyLjEyNCAxLjQzOWEuMzY0LjM2NCAwIDAgMS0uNTMzLjM1M2wtMS4zMDEtLjU0Ny0xLjMyOS41NjZhLjM2My4zNjMgMCAwIDEtLjUwNC0uMzY3bC4xMjUtMS40MzktLjk0Ni0xLjA5NmEuMzY0LjM2NCAwIDAgMSAuMTkzLS41OTJsMS40MDUtLjMyNi43NDQtMS4yMjdhLjM2NS4zNjUgMCAwIDEgLjYyNiAwbC43NCAxLjIzMSAxLjQwNS4zMjZhLjM2NS4zNjUgMCAwIDEgLjI3OC40MzV6Ii8+PC9zdmc+',
        'rewrite'             => array( 'slug' => 'testimonial' ),
    );

    register_post_type( 'testimonial', $args );


}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'laclicsa_custom_post_types', 0 );

function laclicsa_budgets_rewrite_basic() {

    //add_rewrite_rule('^presupuesto/pagado/?$', 'index.php?page_id=' . get_field('budgets', 'option') . '&s=payed', 'top');
    //add_rewrite_rule('^presupuesto/verificar/?$', 'index.php?page_id=' . get_field('budgets', 'option') . '&s=verify', 'top');
    add_rewrite_rule('^presupuesto/([^/]+)/?$', 'index.php?page_id=' . get_field('budgets', 'option') . '&budget=$matches[1]', 'top');
    add_rewrite_rule('^presupuesto/([^/]+)/(pagado|verificar)/?$', 'index.php?page_id=' . get_field('budgets', 'option') . '&budget=$matches[1]&status=$matches[2]', 'top');
    add_rewrite_rule('^presupuesto/openpay/?$', 'index.php?page_id=' . get_field('budgets', 'option') . '&action=openpay', 'top');
    add_rewrite_rule('^presupuesto/openpay-wh/?$', 'index.php?page_id=' . get_field('budgets', 'option') . '&action=openpay-wh', 'top');

}
add_action('init', 'laclicsa_budgets_rewrite_basic', 10, 0);

function laclicsa_budgets_rewrite_tag() {
    add_rewrite_tag('%budget%', '([^&]+)');
    add_rewrite_tag('%status%', '([^&]+)');
    add_rewrite_tag('%action%', '([^&]+)');
}
add_action('init', 'laclicsa_budgets_rewrite_tag', 10, 0);