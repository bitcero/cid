<?php
function clean_phone_number($number, $addPrefix = true)
{

    $number = preg_replace('/[^0-9,]|,[0-9]*$/', '', $number);

    if ($addPrefix) {
        return "52" . $number;
    }

    return $number;

}

function load_svg($file) {
    if('' == $file){
        return $file;
    }

    if(false == is_file(get_stylesheet_directory() . '/assets/icons/' . $file . '.svg')){
        return $file;
    }

    return file_get_contents( get_stylesheet_directory() . '/assets/icons/' . $file . '.svg');
}