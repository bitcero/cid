<?php

function laclicsa_theme_setup() {

    add_theme_support( 'title-tag' );
    add_theme_support( 'custom-logo' );

    register_nav_menus(
        [
            'primary' => 'Principal',
            'footer' => 'Pie de página',
            'companies' => 'Empresas',
            'contact' => 'Contacto',
            'about' => 'Acerca de Nosotros',
        ]
    );

    add_theme_support(
        'html5',
        array(
            'gallery',
            'caption',
        )
    );
}

add_action( 'after_setup_theme', 'laclicsa_theme_setup' );

function laclicsa_widgets() {
    register_sidebar( array(
        'name'          => 'Facebook en Portada',
        'id'            => 'facebook-widget',
        'description'   => 'Área para presentar los reviews de facebook en la portada',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => 'Widgets Laterales',
        'id'            => 'sidebar-widgets',
        'description'   => 'Sidebar para widgets',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'laclicsa_widgets' );
