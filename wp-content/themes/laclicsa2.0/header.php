<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="l-page-wrap">
    <header id="laclicsa-header">
        <?php require "tpls/top-bar.php"; ?>

        <div class="laclicsa-logo"><?php the_custom_logo(); ?></div>

        <nav class="navbar navbar-expand-lg laclicsa-menu">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#laclicsa-menu" aria-controls="laclicsa-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <?php
            wp_nav_menu( array(
                'theme_location'  => 'primary',
                'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                'container'       => 'div',
                'container_class' => 'navbar-collapse justify-content-end',
                'container_id'    => 'laclicsa-menu',
                'menu_class'      => 'navbar-nav',
                'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                'walker'          => new WP_Bootstrap_Navwalker(),
            ) );
            ?>
        </nav>
    </header>

    <?php if( is_front_page() ): ?>
        <div id="home-slider">
            <?php echo do_shortcode(get_field('home-slider', 'option')); ?>
            <?php echo do_shortcode(get_field('home-slider-mobile', 'option')); ?>
        </div>
    <?php endif; ?>

    <main id="laclicsa-main">
