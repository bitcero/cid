<nav id="l-topbar">

    <?php
    $buttons = get_field('top-buttons', 'option');
    $titleSuc = get_field( 'title-sucursales', 'option' );
    $sucursales = get_field( 'sucursales', 'option' );
    ?>

    <?php if(get_field('sucursales-enabled', 'option') && false == empty($sucursales)): shuffle($sucursales); ?>
        <div class="sucursales">
            <span class="caption">
                <?php if('' == $titleSuc): ?>
                    <?php _e('Sucursales', 'laclicsa'); ?>
                <?php else: ?>
                    <?php echo $titleSuc; ?>
                <?php endif; ?>
            </span>
            <ul>
                <?php foreach($sucursales as $sucursal): ?>
                    <li>
                        <a href="<?php echo get_permalink($sucursal['link']->ID); ?>">
                            <span class="name"><?php echo $sucursal['name']; ?></span>
                            <span class="phone"><?php echo $sucursal['phone']; ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <ul class="buttons d-flex flex-row-reverse">

        <?php if(get_field('cart-enabled', 'option') && WC()->cart->get_cart_contents_count()): ?>
            <li class="top-button cart-button">
                <a href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
                    <span class="caption">Mi Carrito</span>
                    <span class="content">
                        <?php echo WC()->cart->get_cart_total(); ?>
                    </span>
                </a>
            </li>
        <?php endif; ?>

        <?php
        $whatsapp = get_field('whatsapp', 'option');
        if($whatsapp):
            ?>
            <li class="top-button whatsapp-button">
                <a href="https://api.whatsapp.com/send?phone=<?php echo clean_phone_number(get_field("whatsapp-number", 'option')); ?>">
                    <span class="caption"><?php echo get_field('whatsapp-caption', 'option'); ?></span>
                    <span class="content"><?php echo get_field('whatsapp-number', 'option'); ?></span>
                </a>
            </li>
        <?php endif; ?>

        <?php
        $facebook = get_field('facebook', 'option');
        if($facebook):
            ?>
            <li class="top-button facebook-button">
                <a href="<?php echo get_field("facebook-link", 'option'); ?>">
                    <span class="caption"><?php echo get_field('facebook-caption', 'option'); ?></span>
                    <span class="content"><?php _e('Facebook', 'laclicsa'); ?></span>
                </a>
            </li>
        <?php endif; ?>

        <li class="top-button customers-button">
            <a href="https://planlealtad.laclicsa.com.mx">
                <span class="caption">Acceso</span>
                <span class="content"><?php _e('Clientes', 'laclicsa'); ?></span>
            </a>
        </li>

        <li class="top-button companies-button">
            <a href="https://accesoempresas.laclicsa.com.mx">
                <span class="caption">Acceso</span>
                <span class="content"><?php _e('Empresas', 'laclicsa'); ?></span>
            </a>
        </li>

        <?php if(false == empty($buttons)): ?>
            <?php foreach($buttons as $button): ?>
                <li>
                    <a href="<?php echo $button['link']; ?>">
                        <?php if('' != $button['icon']): ?>
                            <span class="icon icon-<?php echo $button['icon']; ?>"></span>
                        <?php endif; ?>
                        <?php echo $button['caption']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>

    </ul>

</nav>