<?php
/**
 * Template Name: Portada Laclicsa
 */

get_header();
?>

    <section id="main-services">
        <div class="container">
            <div class="services-container d-flex justify-content-center align-items-center">

                <?php
                $services = get_field('home-services', 'option');
                foreach($services as $index => $service):
                    ?>
                    <a class="service-item" href="<?php echo $service['page']; ?>">
                        <div class="service-content">
                    <span class="icon-wrapper">
                        <?php echo load_svg($service['icon']); ?>
                    </span>
                            <h3><?php echo $service['name']; ?></h3>
                            <p><?php echo $service['description']; ?></p>
                        </div>
                        <?php
                        if($service['button'] != ''):
                            ?>
                            <span  class="btn btn-service btn-lg">
                        <?php echo $service['button']; ?>
                    </span>
                        <?php endif; ?>
                    </a>

                    <?php
                    if(1 == $index){
                        echo '<div class="clearfix"></div>';
                    }

                    ?>

                <?php
                endforeach;
                ?>

            </div>
        </div>
    </section>

    <?php if(get_field('banner', 'option')): ?>
        <section id="home-quality">
            <div class="container">
                <?php if(get_field('banner-show-logo', 'option')): ?>
                    <img src="<?php echo get_field('banner-logo', 'option'); ?>" alt="<?php bloginfo('name'); ?>">
                <?php endif; ?>

                <div class="isos">
                    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/iso17025.png'; ?>" alt="Laboratorio de prueba tercero autorizado">
                    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/iso15189.png'; ?>" alt="Primer laboratorio en México acreditado en ISO 15189:2012">
                </div>

                <?php if(get_field('banner_text', 'option')): ?><p><?php echo get_field('banner-text', 'option' ); ?></p><?php endif; ?>
            </div>
        </section>
    <?php endif; ?>

    <section id="home-reviews">
        <div class="container">
            <div class="container">
                <h2>¿Que opinan nuestros clientes?</h2>
                <?php dynamic_sidebar('facebook-widget'); ?>
            </div>
        </div>
    </section>

    <section id="home-video" style="background-image: url(<?php echo get_field('video-bg', 'option'); ?>);">
        <div class="video-container">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe src="https://www.youtube.com/embed/4WGcZQT-m_k?rel=0&amp;showinfo=0" frameborder="0"
                        class="embed-responsive-item"
                        allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    </section>

    <section id="home-map">
        <?php echo do_shortcode('[put_wpgm id=1]'); ?>
    </section>

<?php
get_footer();