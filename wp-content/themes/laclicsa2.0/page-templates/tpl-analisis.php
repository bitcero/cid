<?php
get_header();
?>

<main id="lac-container">
    <div class="container">

        <div class="row">
            <div class="col-md-8 col-lg-9 pr-lg-5">
                <span class="search-your-study">Busca tu estudio</span>
                <?php echo do_shortcode('[search_live placeholder="Buscar estudios..."]'); ?>
                <div class="not-find-message">
                    ¿No encuentra el estudio que necesita?
                    <a href="<?php echo get_field('contact-url', 'option' ); ?>">Contáctenos</a>
                    y con gusto podremos ayudarle.
                </div>

                <div class="tests-list">
                    <h2>Estudios buscados recientemente</h2>
                    <?php

                    $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

                    // Tests
                    $categories = get_field( 'studys_cats', 'option' );

                    if( 'control' == $type ){
                        $idsStudies = $categories[0]['control'];
                    } elseif ( 'xrays' == $type ) {
                        $idsStudies = $categories[0]['image'];
                    } else {
                        $idsStudies = $categories[0]['analysis'];
                    }

                    // Profiles
                    $categories = get_field( 'profiles_cats', 'option' );

                    if( 'control' == $type ){
                        $idsProfiles = $categories[0]['control'];
                    } elseif ( 'xrays' == $type ) {
                        $idsProfiles = $categories[0]['image'];
                    } else {
                        $idsProfiles = $categories[0]['analysis'];
                    }

                    $args = [
                        'post_type' => ['profile', 'study'],
                        'post_status' => 'publish',
                        'orderby' => 'reads',
                        'order' => 'ASC',
                        'posts_per_page' => 30,
                        'paged' => $paged,
                        'tax_query' => [
                            'relation' => 'OR',
                            [
                                'taxonomy' => 'type',
                                'field' => 'term_id',
                                'terms' => [$idsStudies]
                            ],
                            [
                                'taxonomy' => 'profile_type',
                                'field' => 'term_id',
                                'terms' => [$idsProfiles]
                            ]
                        ]
                    ];

                    $queryStudies = new WP_Query($args);

                    if($queryStudies->have_posts()):
                        echo "<ul>";
                        while($queryStudies->have_posts()): $queryStudies->the_post();

                            $postType = get_post_type();

                            ?>
                            <li class="<?php echo $postType; ?>">
                                <a href="<?php the_permalink(); ?>">
                                    <span class="icon"></span>
                                    <?php the_title(); ?>
                                </a>
                            </li>
                        <?php
                        endwhile;
                        echo "</ul>";

                    endif;
                    ?>

                </div>

                <div class="lacs-pagination">
                    <?php
                     echo paginate_links( array(
                        'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                        'total'        => $queryStudies->max_num_pages,
                        'current'      => max( 1, get_query_var( 'paged' ) ),
                        'format'       => '?paged=%#%',
                        'show_all'     => false,
                        'type'         => 'list',
                        'end_size'     => 2,
                        'mid_size'     => 1,
                        'prev_next'    => true,
                        'prev_text'    => sprintf( '&laquo; %1$s', __( 'Anterior', 'laclicsa' ) ),
                    ) );
                    ?>
                </div>

            </div>

            <div class="col-md-4 col-lg-3 sidebar-widgets">
                <?php dynamic_sidebar('sidebar-widgets'); ?>
            </div>

        </div>

    </div>
</main>



<?php

get_footer();

