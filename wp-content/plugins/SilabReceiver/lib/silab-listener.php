<?php
/**
 * Silab Receiver
 *
 * Listener for Silab communications
 *
 * @package     SilabReceiver
 * @subpackage  silab
 * @author      Best Code Designs <hello@bestcodedesigns.com>
 * @link        https://www.bestcodedesigns.com
 */

/**
 * Receive and process requests from Silab
 *
 * Requests will be received using the URL
 * https://www.laclicsa.com/?silab=receive
 */
function silab_listener()
{

}

add_action('init', 'silab_listener');