<?php
/**
 * Silab Receiver
 *
 *
 * @package     SilabReceiver
 * @subpackage  silab
 * @author      Best Code Designs <hello@bestcodedesigns.com>
 * @link        https://www.bestcodedesigns.com
 */

class Silab_Admin
{
    function __construct()
    {

        add_action('admin_menu', [$this, 'admin_menu']);
        add_action( 'wp_ajax_process_test_ajax', [$this, 'process_ajax_test'] );
        add_action( 'wp_ajax_process_profile_ajax', [$this, 'process_ajax_profile'] );

    }

    public function process_ajax_test(){

        $this->processTest(isset($_POST['id']) ? $_POST['id'] : 0);

        die();

    }

    public function process_ajax_profile(){

        $this->processProfile(isset($_POST['id']) ? $_POST['id'] : 0);

        die();

    }


    /**
     * Crea el menú para Silab Receiver
     */
    public function admin_menu()
    {
        add_menu_page(
            __('Silab Receiver', 'silab'),
            __('Silab Receiver', 'silab'),
            'manage_options',
            'silab-receiver',
            [$this, 'admin_page'],
            'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiIHdpZHRoPSIyMHB4IiBoZWlnaHQ9IjIwcHgiIHZpZXdCb3g9IjAgMCAyMCAyMCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMjAgMjAiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPjxnPjxnPjxwYXRoIGZpbGw9IiM5RUEzQTgiIGQ9Ik0xNS43MDUsNS44NTNjLTAuNTc5LTIuODg1LTMuMTUtNS4wMTktNi4xMjItNS4wMTljLTMuNDQ3LDAtNi4yNSwyLjgwMy02LjI1LDYuMjVjMCwwLjE0MSwwLjAwNiwwLjI4NSwwLjAxOCwwLjQzOEMxLjQ3MSw3LjcyMSwwLDkuMzE3LDAsMTEuMjVDMCwxMy4zMTcsMS42ODMsMTUsMy43NSwxNWMwLjIzLDAsMC40MTctMC4xODcsMC40MTctMC40MTZjMC0yLjk4NywyLjQzLTUuNDE3LDUuNDE3LTUuNDE3UzE1LDExLjU5NywxNSwxNC41ODRDMTUsMTQuODEzLDE1LjE4NywxNSwxNS40MTYsMTVDMTcuOTQ0LDE1LDIwLDEyLjk0NCwyMCwxMC40MTdDMjAsOC4wMDcsMTguMTMyLDYuMDI3LDE1LjcwNSw1Ljg1M3oiLz48cGF0aCBmaWxsPSIjOUVBM0E4IiBkPSJNOS41ODMsMTBDNy4wNTYsMTAsNSwxMi4wNTYsNSwxNC41ODRjMCwyLjUyNywyLjA1Niw0LjU4Miw0LjU4Myw0LjU4MnM0LjU4My0yLjA1NSw0LjU4My00LjU4MkMxNC4xNjYsMTIuMDU2LDEyLjExMSwxMCw5LjU4MywxMHogTTExLjU0NSwxNS43MTJsLTEuNjY2LDEuNjY1Yy0wLjAzOSwwLjAzOS0wLjA4NSwwLjA3LTAuMTM2LDAuMDkyQzkuNjkyLDE3LjQ4OSw5LjYzOCwxNy41LDkuNTgzLDE3LjVzLTAuMTA4LTAuMDExLTAuMTU5LTAuMDMxYy0wLjA1Mi0wLjAyMS0wLjA5OC0wLjA1My0wLjEzNi0wLjA5MmwtMS42NjYtMS42NjVjLTAuMTYzLTAuMTYzLTAuMTYzLTAuNDI3LDAtMC41ODljMC4xNjMtMC4xNjMsMC40MjctMC4xNjMsMC41ODksMGwwLjk1NSwwLjk1NXYtMy45OTRjMC0wLjIzLDAuMTg3LTAuNDE4LDAuNDE3LTAuNDE4UzEwLDExLjg1NCwxMCwxMi4wODR2My45OTRsMC45NTUtMC45NTVjMC4xNjItMC4xNjMsMC40MjctMC4xNjMsMC41ODksMEMxMS43MDcsMTUuMjg1LDExLjcwNywxNS41NDksMTEuNTQ1LDE1LjcxMnoiLz48L2c+PC9nPjwvZz48L3N2Zz4=)',
            57.33
        );
    }

    /**
     * Muestra la página administrativa
     */
    public function admin_page()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : '';

        if ('silab-receiver' != $page) {
            return;
        }

        $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';

        if ('' == $action) {
            wp_enqueue_style(
                'silab',
                plugins_url() . '/SilabReceiver/assets/css/silab.min.css'
            );

            wp_enqueue_script(
                'silab',
                plugins_url() . '/SilabReceiver/assets/js/parser.min.js'
            );

            require plugin_dir_path(dirname(__FILE__)) . 'admin/admin-page.php';

        } elseif ('process-test' == $action){

            $this->processTest(isset($_GET['id']) ? $_GET['id'] : 0);

        } elseif ('process-tests' == $action) {

            $this->parsePruebas(
                isset($_GET['part']) ? intval($_GET['part']) : 1
            );

        } elseif ('profiles' == $action) {

            return $this->generateProfiles();

        }

    }

    public function processTest( $id ){

        if($id<=0){
            echo "Error: no se proporciono un ID";
            return;
        }

        $category = [
            'ac'  => 17,
            'cs'  => 32,
            'img' => 33
        ];

        $file     = dirname(dirname(__FILE__)) . '/pruebas.json';
        $json     = json_decode(
            file_get_contents($file)
        );

        if(false == isset($json->$id)){
            echo "Error: no existe el ID";
            return;
        }

        $prueba = $json->$id;

        $code          = $id;
        $title         = $prueba->name;
        $method        = $prueba->method;
        $rawConditions = explode(
            "\r\n", $prueba->conditions
        );
        $conditions    = [];
        $days          = $prueba->time;
        $type          = $prueba->type;
        $price         = $prueba->price;

        $postID = wp_insert_post(
            [
                'post_title'  => $title,
                'post_type'   => 'study',
                'post_status' => 'publish',
                'tax_input'   => [
                    'type' => 'cs' == $type ? $category['cs']
                        : ($type == 'im' ? $category['img'] : $category['ac']),
                ],
            ]
        );

        $json->$id->post = $postID;

        update_field('days', $days, $postID);
        update_field('method', $method, $postID);
        update_field('price', $price, $postID);

        foreach ($rawConditions as $condition) {

            if ('' == trim($condition)) {
                continue;
            }

            $conditions[] = [
                'text' => $condition,
            ];
        }

        update_field('instructions', $conditions, $postID);
        update_field('id_laclicsa', $code, $postID);

        file_put_contents($file, json_encode($json));

        wp_send_json($prueba);
        die();

    }

    public function processProfile( $id ){

        if($id<=0){
            echo "Error: no se proporciono un ID";
            return;
        }

        $category = [
            'ac'  => 19,
            'cs'  => 18,
            'img' => 20
        ];

        $file     = dirname(dirname(__FILE__)) . '/perfiles-pruebas.json';
        $json     = json_decode(
            file_get_contents($file)
        );

        if(false == isset($json->$id)){
            echo "Error: no existe el ID";
            return;
        }

        $file     = dirname(dirname(__FILE__)) . '/pruebas.json';
        $pruebas = json_decode(file_get_contents($file), true);

        $perfil = $json->$id;

        $code          = $id;
        $title         = $perfil->name;
        $method        = $perfil->method;
        $type          = $perfil->type;
        $price         = $perfil->price;

        // Get time and conditions
        $conditions = '';
        $priority = 0;
        $posts = [];
        $time = 0;

        foreach($perfil->tests as $i => $void){

            $test = $pruebas[$i];
            $posts[] = $test['post'];

            if($test['priority'] > $priority){
                $conditions = $test['conditions'];
                $priority = $test['priority'];
            }

            $time = $test['time'] > $time ? $test['time'] : $time;

        }

        $rawConditions = explode(
            "\r\n", $conditions
        );
        $conditions    = [];
        $days          = $time;

        $postID = wp_insert_post(
            [
                'post_title'  => $title,
                'post_type'   => 'profile',
                'post_status' => 'publish',
                'tax_input'   => [
                    'profile_type' => 'cs' == $type ? $category['cs']
                        : ($type == 'im' ? $category['img'] : $category['ac']),
                ],
            ]
        );

        //$json->$id->post = $postID;

        update_field('days', $days, $postID);
        update_field('method', $method, $postID);

        foreach ($rawConditions as $condition) {

            if ('' == trim($condition)) {
                continue;
            }

            $conditions[] = [
                'text' => $condition,
            ];
        }

        update_field('instructions', $conditions, $postID);
        update_field('id_laclicsa', $code, $postID);
        update_field('tests', $posts, $postID);

        wp_send_json($perfil);
        die();

    }

    /**
     * Lee los archivos de pruebas y perfiles para generar
     * los datos completos de los perfiles
     */
    public function generateProfiles()
    {

        $path         = dirname(dirname(__FILE__));
        $fileProfiles = $path . '/JSON_PERFILES.json';
        $filePruebas  = $path . '/JSON_PRUEBAS.json';

        $jsonPruebas = json_decode(file_get_contents($filePruebas));
        $tests       = [];

        // Procesamos las pruebas
        foreach ($jsonPruebas as $prueba) {

            if (array_key_exists($prueba->PRUEBA->CVE_PRU, $tests)) {
                continue;
            }

            if($prueba->TIPOPRODUCTO->DESCRIPCION == 'ANALISIS CLINICOS'){
                $type = 'ac';
            } else if($prueba->TIPOPRODUCTO->DESCRIPCION == 'CONTROL SANITARIO'){
                $type = 'cs';
            } else if($prueba->TIPOPRODUCTO->DESCRIPCION == 'IMAGENOLOGIA'){
                $type = 'im';
            }

            $tests[$prueba->PRUEBA->CVE_PRU] = [
                'name'       => $prueba->PRUEBA->DESCRIPCION,
                'method'     => $prueba->PRUEBA->METODO,
                'conditions' => $prueba->lCondiciones[0]->DESCRIPCION,
                'order'      => $prueba->lCondiciones[0]->ORDEN,
                'priority'   => $prueba->lCondiciones[0]->PRIORIDAD,
                'time'       => $prueba->PRUEBA->TIEMPO,
                'price'      => $prueba->PREPUB,
                'cert'       => $prueba->PRUEBA->CERTIFICACION,
                'type'       => $type
            ];

        }

        file_put_contents(
            dirname(dirname(__FILE__)) . '/pruebas.json', json_encode($tests)
        );

        // Procesamos los perfiles
        $jsonPerfiles = json_decode(file_get_contents($fileProfiles));
        $profiles     = [];

        foreach ($jsonPerfiles as $perfil) {

            $id = $perfil->PERFIL->PruebasyPerfiles[0]->CVE_PER;

            if (array_key_exists($id, $profiles)) {
                $profiles[$id]['tests'][$perfil->PERFIL->PruebasyPerfiles[0]->CVE_PRU]
                    = $tests[$perfil->PERFIL->PruebasyPerfiles[0]->CVE_PRU];
            } else {

                if($perfil->TIPOPRODUCTO->DESCRIPCION == 'ANALISIS CLINICOS'){
                    $type = 'ac';
                } else if($perfil->TIPOPRODUCTO->DESCRIPCION == 'CONTROL SANITARIO'){
                    $type = 'cs';
                } else if($perfil->TIPOPRODUCTO->DESCRIPCION == 'IMAGENOLOGIA'){
                    $type = 'im';
                }

                $profiles[$id] = [
                    'name'   => $perfil->PERFIL->DESCRIPCION,
                    'price'  => $perfil->PREPUB,
                    'method' => $perfil->PERFIL->METHOD,
                    'type'   => $type,
                    'tests'  => [
                        $perfil->PERFIL->PruebasyPerfiles[0]->CVE_PRU => $tests[$perfil->PERFIL->PruebasyPerfiles[0]->CVE_PRU],
                    ],
                ];
            }

        }

        file_put_contents(
            dirname(dirname(__FILE__)) . '/perfiles-pruebas.json',
            json_encode($profiles)
        );

    }

    public function parsePerfiles($json = [])
    {

        if (empty($json)) {
            echo "Error";

            return;
        }

        $pruebasPosts = json_decode(
            file_get_contents(dirname(ABSPATH) . '/pruebas.json'), true
        );

        /*if(empty($pruebasPosts)){
            echo "No existe el archivo de pruebas";
            return;
        }*/

        $perfiles = [];

        /**
         * Formato correcto de los perfiles
         */
        foreach ($json as $i => $perfil) {

            $id = $perfil->PERFIL->PruebasyPerfiles[0]->CVE_PER;

            if (array_key_exists($id, $perfiles)) {
                $perfiles[$id]['tests'][]
                    = $perfil->PERFIL->PruebasyPerfiles[0]->CVE_PRU;
            } else {
                $perfiles[$id] = [
                    'name'   => $perfil->PERFIL->DESCRIPCION,
                    'method' => $perfil->PERFIL->METODO,
                    'tests'  => [
                        $perfil->PERFIL->PruebasyPerfiles[0]->CVE_PRU,
                    ],
                ];
            }

        }

        /**
         * Generamos los perfiles en la base de datos
         */
        foreach ($perfiles as $id => $perfil) {

            $postID = wp_insert_post(
                [
                    'post_title'  => ucfirst(strtolower($perfil['name'])),
                    'post_type'   => 'profile',
                    'post_status' => 'publish',
                    'tax_input'   => [
                        'hierarchical_tax' => [3],
                    ],
                ]
            );

            $pruebas = [];
            foreach ($perfil['tests'] as $idTest) {
                $pruebas[] = $pruebasPosts[$idTest];
            }

            update_field('tests', $pruebas, $postID);
            update_field('days', 0, $postID);
            update_field('method', $perfil['method'], $postID);

        }

        print_r($perfiles);

    }

    /**
     * Lee y procesa el array con información sobre las pruebas
     *
     * @param array $json
     */
    public function parsePruebas($part = 1)
    {

        $file     = dirname(dirname(__FILE__)) . '/pruebas.json';
        $jsonOriginal     = json_decode(
            file_get_contents($file)
        );

        $limit = ceil(count((array) $jsonOriginal) / 2);

        $json = $jsonOriginal;

        $category = [
            'ac' => 2,
            'cs' => 28,
        ];

        if (empty($json)) {
            echo "Error";
            die();
        }

        $insertedCodes = [];
        $counter       = 0;

        foreach ($json as $i => $prueba) {

            if($counter >= $limit ){
                break;
            }

            $code          = $i;
            $title         = $prueba->name;
            $method        = $prueba->method;
            $rawConditions = explode(
                "\r\n", $prueba->conditions
            );
            $conditions    = [];
            $days          = $prueba->time;
            $type          = $prueba->type;

            //echo "Clave: $code<br>Nombre: $title<br>Método: $method<br>Condiciones: $condition<br><br>";

            if (array_key_exists($code, $insertedCodes)) {
                continue;
            }

            $postID = wp_insert_post(
                [
                    'post_title'  => ucfirst(strtolower($title)),
                    'post_type'   => 'study',
                    'post_status' => 'publish',
                    'tax_input'   => [
                        'type' => 'cs' == $type ? $category['cs']
                            : ($type == 'im' ? $category['img'] : $category['ac']),
                    ],
                ]
            );

            //$insertedCodes[$code] = $postID;
            $prueba->post = $postID;

            update_field('days', $days, $postID);
            update_field('method', $method, $postID);

            foreach ($rawConditions as $condition) {

                if ('' == trim($condition)) {
                    continue;
                }

                $conditions[] = [
                    'text' => $condition,
                ];
            }

            update_field('instructions', $conditions, $postID);
            update_field('id_laclicsa', $code, $postID);
            $counter++;
            $jsonOriginal->$i = $prueba;

        }

        file_put_contents($file, json_encode($jsonOriginal));

        echo "<strong>$counter</strong> Pruebas Procesadas";

    }
}

new Silab_Admin();