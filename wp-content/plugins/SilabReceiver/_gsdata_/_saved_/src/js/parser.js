(function($) {

    var index = 0;
    var tests;
    var profiles;
    var total;
    var steps;

    $("#parse-tests").click(function(){
alert("Hello");
        $.get('./../wp-content/plugins/SilabReceiver/get-tests.php', {}, function(response){

            tests = response;
            total = tests.length;
            steps = 100 / tests.length;
            $("#silab-log > ul").append('<li>Iniciando...</li>');
            processTest(index);

        }, 'json');

    });

    $("#parse-profiles").click(function(){

        index = 0;

        $.get('./../wp-content/plugins/SilabReceiver/get-profiles.php', {}, function(response){

            profiles = response;
            total = profiles.length;
            steps = 100 / profiles.length;

            $("#silab-progress > .silab-progress-bar")
                .css('width', '0%')
                .html('0%');

            $("#silab-log > ul").append('<li>Iniciando...</li>');
            processProfile(index);

        }, 'json');

    });

    function processTest(index){

        if(index >= tests.length){
            $("#silab-log > ul").append('<li>Finalizado</li>');
            return;
        }

        var params = {
            page: 'silab-receiver',
            action: 'process_test_ajax',
            id: tests[index]
        };

        $.post(ajaxurl, params, function(response){

            $("#silab-log > ul > li").html('Se agregó "' + response.name + '"');
            $("#silab-progress > .silab-progress-bar")
                .css('width', (steps * index) + '%')
                .html((steps * index).toFixed(2) + '%');
            processTest(index + 1);

        }, 'json');

    }

    function processProfile(index){

        if(index >= profiles.length){
            $("#silab-log > ul").append('<li>Finalizado</li>');
            return;
        }

        var params = {
            page: 'silab-receiver',
            action: 'process_profile_ajax',
            id: profiles[index]
        };

        $.post(ajaxurl, params, function(response){

            $("#silab-log > ul > li").html('Se agregó "' + response.name + '"');
            $("#silab-progress > .silab-progress-bar")
                .css('width', (steps * index) + '%')
                .html((steps * index).toFixed(2) + '%');
            processProfile(index + 1);

        }, 'json');

    }

})(jQuery);