<?php
/**
 * Plugin Name: Silab Receiver
 * Plugin URI:  https://www.laclicsa.com
 * Description: Plugin para recibir datos de Silab
 * Author:      Eduardo Cortés
 * Author URI:  https://www.eduardocortes.mx
 * Version:     1.0
 * Text Domain: silab
 * Domain Path: languages
 *
 * Silab Receiver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * Silab Receiver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Silab Receiver. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SilabReceiver
 * @author     Eduardo Cortés
 * @since      1.0.0
 * @license    GPL-2.0+
 * @copyright  Copyright (c) 2017, Eduardo Cortés
 */

// Exit if accessed directly.
if ( ! defined('ABSPATH')) {
    exit;
}

if (false == class_exists('Silab_Receiver')) {

    class Silab_Receiver
    {
        /**
         * Only a single instance of this object
         *
         * @var
         */
        private static $instance;

        /**
         * Plugin version
         *
         * @var string
         */
        public $version = '1.0.0';

        public static function getInstance()
        {
            static $instance;

            if (false == isset(self::$instance)) {
                self::$instance = new Silab_Receiver();
            }

            self::$instance->load_files();

            return self::$instance;
        }

        private function load_files()
        {
            global $wpdb;

            if( is_admin() ) {

//                $wpdb->query("DELETE a,b,c
//    FROM $wpdb->posts a
//    LEFT JOIN $wpdb->term_relationships b
//        ON (a.ID = b.object_id)
//    LEFT JOIN $wpdb->postmeta c
//        ON (a.ID = c.post_id)
//    WHERE a.post_type = 'study';");
//
//                $wpdb->query("DELETE a,b,c
//    FROM $wpdb->posts a
//    LEFT JOIN $wpdb->term_relationships b
//        ON (a.ID = b.object_id)
//    LEFT JOIN $wpdb->postmeta c
//        ON (a.ID = c.post_id)
//    WHERE a.post_type = 'profile';");

                require_once 'lib/admin-setup.php';

            }
        }

    }

    function load_silab() {
        return Silab_Receiver::getInstance();
    }

    load_silab();

}