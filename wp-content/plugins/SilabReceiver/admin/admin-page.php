<header id="silab-header">
    <h1>Silab Receiver</h1>
    <p>Conector y sincronizador para Silab</p>
</header>

<div id="silab-links">
    <a href="<?php echo bloginfo('url'); ?>/wp-admin/admin.php?page=silab-receiver&action=profiles">Generar Datos</a> |
    <a href="#" id="parse-tests">Procesar Pruebas</a> |
    <a href="#" id="parse-profiles">Procesar Perfiles</a>
</div>

<div id="silab-init">
    <label>Configuración Inicial</label>

    <form method="post" enctype="multipart/form-data">
        <label for="json-file">Archivo JSON con datos:</label>
        <input type="file" id="json-file" name="data" class="widefat">
        <input type="hidden" name="action" value="process-json">
        <?php wp_nonce_field(); ?>
        <button type="submit" class="button">Procesar</button>
    </form>
</div>

<div id="silab-progress">
    <div class="silab-progress-bar">
        0%
    </div>
</div>

<div id="silab-log">
    <ul></ul>
</div>