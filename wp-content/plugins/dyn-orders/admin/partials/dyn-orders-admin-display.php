<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Dyn_Orders
 * @subpackage Dyn_Orders/admin/partials
 */
?>

<div class="wrap">
    <h1 class="wp-heading-inline">Órdenes Registradas</h1>

    <table class="wp-list-table widefat fixed striped posts">
        <thead>
        <tr>
            <td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1">Seleccionar todos</label><input
                        id="cb-select-all-1" type="checkbox"></td>
            <th scope="col" id="order_status" class="manage-column column-order_status"><span class="status_head tips">Estado</span></th>
            <th scope="col" id="order_title" class="manage-column column-order_title column-primary sortable desc"><a
                        href="http://bitdesk/laclicsa/wp-admin/edit.php?post_type=shop_order&amp;orderby=ID&amp;order=asc"><span>Pedido</span><span
                            class="sorting-indicator"></span></a></th>
            <th scope="col" id="order_title" class="manage-column column-primary sortable desc"><span>Productos</span></th>
            <th scope="col" id="order_date" class="manage-column column-order_date sortable desc"><a
                        href="http://bitdesk/laclicsa/wp-admin/edit.php?post_type=shop_order&amp;orderby=date&amp;order=asc"><span>Fecha</span><span
                            class="sorting-indicator"></span></a></th>
            <th scope="col" id="order_total" class="manage-column column-order_total sortable desc"><a
                        href="http://bitdesk/laclicsa/wp-admin/edit.php?post_type=shop_order&amp;orderby=order_total&amp;order=asc"><span>Total</span><span
                            class="sorting-indicator"></span></a></th>
            <th scope="col" id="order_actions" class="manage-column column-order_actions">Acciones</th>
        </tr>
        </thead>

        <tbody id="the-list">

        </tbody>

        <tfoot>
        <tr>
            <td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1">Seleccionar todos</label><input
                        id="cb-select-all-1" type="checkbox"></td>
            <th scope="col" id="order_status" class="manage-column column-order_status"><span class="status_head tips">Estado</span></th>
            <th scope="col" id="order_title" class="manage-column column-order_title column-primary sortable desc"><a
                        href="http://bitdesk/laclicsa/wp-admin/edit.php?post_type=shop_order&amp;orderby=ID&amp;order=asc"><span>Pedido</span><span
                            class="sorting-indicator"></span></a></th>
            <th scope="col" id="order_title" class="manage-column column-primary sortable desc"><span>Productos</span></th>
            <th scope="col" id="order_date" class="manage-column column-order_date sortable desc"><a
                        href="http://bitdesk/laclicsa/wp-admin/edit.php?post_type=shop_order&amp;orderby=date&amp;order=asc"><span>Fecha</span><span
                            class="sorting-indicator"></span></a></th>
            <th scope="col" id="order_total" class="manage-column column-order_total sortable desc"><a
                        href="http://bitdesk/laclicsa/wp-admin/edit.php?post_type=shop_order&amp;orderby=order_total&amp;order=asc"><span>Total</span><span
                            class="sorting-indicator"></span></a></th>
            <th scope="col" id="order_actions" class="manage-column column-order_actions">Acciones</th>
        </tr>
        </tfoot>

    </table>
</div>
