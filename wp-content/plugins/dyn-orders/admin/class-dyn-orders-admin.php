<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Dyn_Orders
 * @subpackage Dyn_Orders/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dyn_Orders
 * @subpackage Dyn_Orders/admin
 * @author     Your Name <email@example.com>
 */
class Dyn_Orders_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dyn_orders    The ID of this plugin.
	 */
	private $dyn_orders;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $dyn_orders       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $dyn_orders, $version ) {

		$this->dyn_orders = $dyn_orders;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dyn_Orders_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dyn_Orders_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->dyn_orders, plugin_dir_url( __FILE__ ) . 'css/dyn-orders-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->dyn_orders, plugin_dir_url( __FILE__ ) . 'js/dyn-orders-admin.js', array( 'jquery' ), $this->version, false );


	}

	public function add_custom_posts() {

        /**
         * Custom Post Types para Sucursales
         */
        $labels = array(
            'name'               => __( 'Presupuestos', 'laclicsa' ),
            'singular_name'      => __( 'Presupuesto', 'laclicsa' ),
            'menu_name'          => __( 'Presupuestos', 'laclicsa' ),
            'all_items'          => __( 'Todos los Presupuestos', 'laclicsa' ),
            'view_item'          => __( 'Ver Presupuesto', 'laclicsa' ),
            'add_new_item'       => __( 'Agregar Presupuesto', 'laclicsa' ),
            'add_new'            => __( 'Nuevo Presupuesto', 'laclicsa' ),
            'edit_item'          => __( 'Editar Presupuesto', 'laclicsa' ),
            'update_item'        => __( 'Actualizar Presupuesto', 'laclicsa' ),
            'search_items'       => __( 'Buscar Presupuestos', 'laclicsa' ),
            'not_found'          => __( 'No se encontró', 'laclicsa' ),
            'not_found_in_trash' => __( 'No se encontró en la Papelera', 'laclicsa' ),
        );

        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'custom-fields', ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
            'capabilities'        => [
                'create_posts'  => false
            ],
            'menu_icon'           => 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj48ZyBmaWxsPSIjODI4NzhDIj48cGF0aCBkPSJNOC4zMjggMS43MkgxLjM2M0MuNjAzIDEuNzIgMCAyLjMxIDAgMy4wN3YxLjQ5MmMwIC42My40MDcgMS4xNTguOTYgMS4zMjV2OS4zODljMCAuMDI2LjAxMS4wNTIuMDEzLjA3OS4wNTkgMS4wMTIuOTQ2IDIuOTI1IDMuODY5IDIuOTI1IDIuNzA1IDAgMy43MTItMS44MjkgMy44Ni0yLjc5Ni4wMS0uMDY3LjAyNy0uMTM5LjAyNy0uMjA4VjUuODg4Yy41NTItLjE2Ny45Ni0uNjk1Ljk2LTEuMzI1VjMuMDdhMS4zNDIgMS4zNDIgMCAwIDAtMS4zNjEtMS4zNXptLS4wMDcgMi4zNjRhLjQ3My40NzMgMCAwIDEtLjQ3Ny40NjVoLS4wMjJhLjQ2NC40NjQgMCAwIDAtLjQ1OS40NzR2MTAuMjU0cy0uMjY4IDEuNjI3LTIuNTA2IDEuNjI3Yy0yLjM5NCAwLTIuNTA2LTEuNjI3LTIuNTA2LTEuNjI3VjUuMDIzYzAtLjI2LS4yMi0uNDc0LS40NzktLjQ3NGgtLjAyNGEuNDU2LjQ1NiAwIDAgMS0uNDU3LS40NjV2LS41MjNjMC0uMjU5LjE5OC0uNDc0LjQ1Ny0uNDc0aDUuOTk2YS40OC40OCAwIDAgMSAuNDc3LjQ3NHYuNTIzeiIvPjxwYXRoIGQ9Ik0zLjA5NCAxNC45MTVzLjA3MSAxLjMxNCAxLjc1IDEuMzE0YzEuNTcgMCAxLjc1LTEuMzE0IDEuNzUtMS4zMTR2LTQuMzQ4aC0zLjV2NC4zNDh6TTE4LjY0NSAxLjcyaC02LjkwOWMtLjc1NSAwLTEuMzUzLjU5MS0xLjM1MyAxLjM1MnYxLjQ5MmMwIC42MjkuNDA4IDEuMTU4Ljk2MSAxLjMyNHY5LjM5YzAgLjAyNi4wMDYuMDUyLjAwNy4wNzkuMDU4IDEuMDEzLjkzOCAyLjkyNCAzLjgzNyAyLjkyNCAyLjY4NyAwIDMuNjgyLTEuODI5IDMuODI4LTIuNzk0LjAxMS0uMDY4LjAyMy0uMTQxLjAyMy0uMjA5di05LjM5Yy41NTMtLjE2Ny45NjEtLjY5Ni45NjEtMS4zMjVWMy4wN2MwLS43Ni0uNTk4LTEuMzUtMS4zNTUtMS4zNXptLS4wMTIgMi4zNjRhLjQ2NS40NjUgMCAwIDEtLjQ2Ny40NjVoLS4wMjNhLjQ3NS40NzUgMCAwIDAtLjQ2OS40NzR2MTAuMjU0cy0uMjU5IDEuNjI3LTIuNDgyIDEuNjI3Yy0yLjM3NSAwLTIuNDgyLTEuNjI3LTIuNDgyLTEuNjI3VjUuMDIzYS40NzcuNDc3IDAgMCAwLS40NzItLjQ3NGgtLjAyM2EuNDYyLjQ2MiAwIDAgMS0uNDYyLS40NjV2LS41MjJhLjQ3LjQ3IDAgMCAxIC40NjItLjQ3NWg1Ljk1MmMuMjU3IDAgLjQ2Ny4yMTYuNDY3LjQ3NXYuNTIyaC0uMDAxeiIvPjxwYXRoIGQ9Ik0xMy40NTMgMTQuOTE1cy4wNzMgMS4zMTQgMS43MzYgMS4zMTRjMS41NjEgMCAxLjc0MS0xLjMxNCAxLjc0MS0xLjMxNFY0LjcxNmgtMy40Nzh2MTAuMTk5eiIvPjwvZz48L3N2Zz4=',
            'rewrite'             => array( 'slug' => 'dynorder' ),
        );

        register_post_type( 'dynorders', $args );

        // Custom columns
        add_filter('manage_dynorders_posts_columns', [$this, 'orders_columns']);
        add_action( 'manage_dynorders_posts_custom_column' , [$this, 'orders_column_content'], 10, 2 );
        add_action( 'manage_edit-dynorders_sortable_columns' , [$this, 'orders_column_sortable'], 10, 2 );

        add_submenu_page(
            null,
            'Registros',
            'Registros',
            'manage_options',
            'dynorders-logs',
            array($this, 'renderLogs')
        );

        add_submenu_page(
            'edit.php?post_type=dynorders',
            'Reporte',
            'Reporte',
            'manage_options',
            'dynorders-report',
            array($this, 'renderReports'),
            500
        );

        add_submenu_page(
            'edit.php?post_type=dynorders',
            'Confirmar Pago',
            'Comfirmar',
            'manage_options',
            'dynorders-confirm',
            array($this, 'confirmPayment')
        );

    }

    public function renderReports()
    {

        $view = isset($_GET['employee']) ? $_GET['employee'] : '';

        if('' === $view){
            $this->renderGeneralReports();
            return;
        }

        $this->renderEmployeeReport();

    }

    public function renderEmployeeReport()
    {
        $month = isset($_GET['month']) ? (int) $_GET['month'] : null;
        $year = isset($_GET['year']) ? (int) $_GET['year'] : null;
        $theEmp = isset($_GET['employee']) ? $_GET['employee'] : null;

        if('' === $theEmp){
            header('Location: admin.php?page=dynorders-report');
            exit;
        }

        $args = [
            'post_type' => ['dynorders'],
            'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => -1,
        ];

        if($month && $year){
            $args['monthnum'] = $month;
            $args['year'] = $year;
        }

        $query = new WP_Query($args);
        $report = [];

        if($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $meta = get_post_meta( get_the_ID(), 'body', true );
                $employee = $this->order_data('employee', get_the_ID());
                $payed = $this->order_data('payed', get_the_ID());

                if($employee != $theEmp) continue;
                if(!$payed) continue;

                $report[] = [
                    'internalId' => get_the_ID(),
                    'id' => $meta->id,
                    'folio' => $meta->folio,
                    'customer' => $meta->customer,
                    'products' => $meta->products,
                    'total' => $meta->total
                ];
            }
        }

        require dirname(plugin_dir_path(__FILE__)) . "/templates/tpl-reports-employee.php";
    }

    public function renderGeneralReports()
    {
        $month = isset($_GET['month']) ? (int) $_GET['month'] : null;
        $year = isset($_GET['year']) ? (int) $_GET['year'] : null;

        $args = [
            'post_type' => ['dynorders'],
            'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => -1,
        ];

        if($month && $year){
            $args['monthnum'] = $month;
            $args['year'] = $year;
        }

        $query = new WP_Query($args);
        $report = [];

        if($query->have_posts())
        {
            while( $query->have_posts() ) {
                $query->the_post();
                $meta = get_post_meta( get_the_ID(), 'body', true );
                $employee = $this->order_data('employee', get_the_ID());
                $payed = $this->order_data('payed', get_the_ID());

                if($employee === '') continue;
                if(!$payed) continue;

                if(false === array_key_exists($employee, $report)){
                    $report[$employee] = ['items' => [], 'total' => 0];
                }

                $report[$employee]['items'][] = [
                    'id' => $meta->id,
                    'total' => $meta->total,
                ];

                $report[$employee]['total'] += $meta->total;
            }
        }

        require dirname(plugin_dir_path(__FILE__)) . "/templates/tpl-reports.php";
    }

    public function renderLogs()
    {

        wp_enqueue_style('orders', plugin_dir_url(dirname(__FILE__)) . 'css/styles.css');

        $id = isset($_GET['id']) ? (int) $_GET['id'] : 0;

        if($id <= 0){
            wp_safe_redirect(admin_url('edit.php') . '?post_type=dynorders');
        }

        $post = get_post($id);

        if($post){
            $logs = get_post_meta($id, 'logs', true);
            $folio = get_post_meta($id, 'folio', true);
        }

        include dirname(dirname(__FILE__)) . '/templates/tpl-logs.php';
    }

    public function confirmPayment()
    {
        $id = isset($_GET['id']) ? (int) $_GET['id'] : 0;

        if($id <= 0){
            wp_safe_redirect(admin_url('edit.php') . '?post_type=dynorders');
        }

        $post = get_post($id);

        if(!$post){
            wp_safe_redirect(admin_url('edit.php') . '?post_type=dynorders');
        }

        $pID = $id;
        $data = get_post_meta($id, 'body', true);
        $logs = get_post_meta($id, 'logs', true);

        if($data->payed !== 1){
            wp_safe_redirect(admin_url('edit.php') . '?post_type=dynorders');
        }

        // Send email
        ob_start();
        include dirname(dirname(__FILE__)) . '/templates/tpl-email-payment-confirm.php';
        //echo "Hola";
        $message = ob_get_clean();
        $cabeceras[] = 'MIME-Version: 1.0';
        $cabeceras[] = 'Content-type: text/html; charset=utf8';

        // Cabeceras adicionales
        //$cabeceras[] = 'To: ' . $body->customer->name . ' <'. $body->customer->email . '>';
        $cabeceras[] = 'From: Laboratorios Laclicsa <' . get_field('budget_email', 'option') . '>';

        // Send email to customer
        $mail = wp_mail( get_field('payments-notification-email', 'option'), 'Confirmación de pago de presupuesto ' . $data->folio, $message, $cabeceras );
        $logs->events[] = [
            'event' => 'Confirmación de pago enviada - Admnin',
            'time' => time(),
            'id' => "Presupuesto: #" . $budgetPost->ID,
            'message' => $message,
            'headers' => $cabeceras,
            'result' => $mail
        ];
        update_post_meta($id, 'logs', $logs);

        wp_safe_redirect(admin_url('edit.php') . '?post_type=dynorders');
    }

    public function orders_columns( $columns ) {

	    $columns = [
	        'cb'            => $columns['cb'],
	        'id'            => 'Presupuesto',
	        'folio'            => 'Folio',
	        'status'        => 'Estado',
	        'customer'      => 'Cliente',
	        'products'      => 'Productos',
	        'total'         => 'Total',
	        'actions'       => 'Acciones',
        ];

        return $columns;

    }

    public function orders_column_content( $column, $postID ) {

	    switch($column){

            case 'folio':
                $folio = $this->order_data('folio', $postID );
                echo '<strong>' . $folio . '</strong>';
                break;

            case 'customer':

                $customer = $this->order_data('customer', $postID );

                if($customer === false){
                    echo 'Error in fields';
                }

                echo '<strong>' . $customer->name . ' ' . $customer->lastname . '</strong><br>';
                echo '<a href="mailto:' . $customer->email . '">' . $customer->email . '</a><br>';
                echo 'Teléfono: ' . $customer->phone;
                break;

            case 'products':

                $products = $this->order_data('products', $postID);

                if($products === false){
                    echo 'Error in fields';
                }

                echo '<ul style="padding: 0; margin: 0;">';
                foreach($products as $product){
                    echo '<li><strong>SKU: ' . $product->id . '</strong> &mdash; $' . number_format($product->price, 2) . ' (' . $product->qty . ')</li>';
                }
                echo '</ul>';

                break;

            case 'total':

                $total = $this->order_data('total', $postID);

                if($total === false){
                    echo 'Error in fields';
                }

                echo '<strong>$' . number_format($total, 2) . '</strong>';
                break;

            case 'status':

                $date = $this->order_data('date', $postID );
                $expire = $this->order_data('valid', $postID );

                if($date === false || $expire === false){
                    echo 'Error in fields';
                }

                $date = strtotime($date);
                $expireDate = $date + ($expire * 86400);
                $payed = $this->order_data('payed', $postID);

                if($payed){
                    echo '<span style="color: #006230;"> <span class="dashicons dashicons-yes"></span>Pagado</span>';
                } else if (time() > $expireDate){
                    echo '<span style="color: #DE0D1B;">Caducado</span>';
                } else {
                    echo '<span style="color: #cf9b0a;"><span class="dashicons dashicons-clock"></span> Válido</span>';
                    echo '<br><small>Expiración: ' . date('d \d\e M, Y', $expireDate) . '</small>';
                }

                break;

            case 'id':

                echo '<strong style="font-size: 1.1em;">#' . $postID . '</strong><br>';
                //echo ' por <em>' . get_post_meta( $postID, 'employee', true ) . '</em><br>';
                echo ' por <em>' . $this->order_data('employee', $postID) . '</em><br>';
                echo ' en <small>' . $this->order_data('date', $postID) . '</small>';
                break;

            case 'actions':

                $payed = $this->order_data('payed', $postID);

                if($payed == 0){
                    echo '<a href="' . get_site_url(null, 'presupuesto/' . base64_encode($postID . '-' . $this->order_data('token', $postID )) . '/') . '">View</a>';
                }

                if( $payed === 1 ){
                    echo '<a href="' . get_site_url(null, 'presupuesto/' . base64_encode($postID . '-' . $this->order_data('token', $postID )) . '/') . '">Details</a>';
                    echo ' | <a href="' . admin_url('admin.php') . '?page=dynorders-confirm&id=' . $postID . '">Confirmar</a>';
                }

                echo ' | <a href="' . admin_url('admin.php') . '?page=dynorders-logs&id=' . $postID . '">Logs</a>';



                break;

            default:

                echo $column;
                break;

        }

    }

    private function order_data( $key, $id ) {

	    $meta = get_post_meta( $id, 'body', true );

	    if(false == isset($meta->{$key})){
	        return false;
        }

        $meta = $meta->{$key};
	    return $meta;

    }

    public function orders_column_sortable($columns) {

	    $columns['id'] = 'title';
	    return $columns;

    }

    public function add_settings ()
    {
        register_setting( 'dyn-orders', 'sandbox');
        register_setting( 'dyn-orders', 'from_email');
    }

    public function add_dynorders_menu()
    {
        add_menu_page('Preferencias de Presupuestos', 'Presupuestos', 'manage_options', 'dyn-orders-settings', [$this, 'settings_page'] );
    }

    public function settings_page()
    {
        require_once 'options.php';
    }

    private function readProperty($prop)
    {
        include dirname(dirname(__FILE__)) . '/templates/tpl-logs-property.php';
    }

    private function readArray($prop)
    {
        include dirname(dirname(__FILE__)) . '/templates/tpl-logs-array.php';
    }
}
