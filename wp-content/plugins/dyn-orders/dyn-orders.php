<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Dyn_Orders
 *
 * @wordpress-plugin
 * Plugin Name:       Dynamic Orders for Laclicsa
 * Plugin URI:        http://www.laclicsa.com
 * Description:       Plugin para el manejo de órdenes dinámicas
 * Version:           1.0.0
 * Author:            Eduardo Cortés
 * Author URI:        http://eduardocortes.mx/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       dyn-orders
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'DYN_ORDERS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dyn-orders-activator.php
 */
function activate_dyn_orders() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dyn-orders-activator.php';
	Dyn_Orders_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-dyn-orders-deactivator.php
 */
function deactivate_dyn_orders() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dyn-orders-deactivator.php';
	Dyn_Orders_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_dyn_orders' );
register_deactivation_hook( __FILE__, 'deactivate_dyn_orders' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-dyn-orders.php';



//add_filter( 'template_include', 'templates_dyn_orders' );

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_dyn_orders() {

	$plugin = new Dyn_Orders();
	$plugin->run();

}

// Signature
define('DYN_ORDERS_KEY', '7FfH$,pj>]AFvv0`$FB F^-Zm.!:ql9nm3}FCK$)f=zhOGwv');

run_dyn_orders();
