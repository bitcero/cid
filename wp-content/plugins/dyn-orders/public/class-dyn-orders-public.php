<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Dyn_Orders
 * @subpackage Dyn_Orders/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Dyn_Orders
 * @subpackage Dyn_Orders/public
 * @author     Your Name <email@example.com>
 */
class Dyn_Orders_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $dyn_orders The ID of this plugin.
     */
    private $dyn_orders;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    private $openpay;

    /** @var bool Indicates if the sandbox endpoint is used. */
    private $use_sandbox = false;
    /** @var bool Indicates if the local certificates are used. */
    private $use_local_certs = false;
    /** Production Postback URL */
    const VERIFY_URI = 'https://ipnpb.paypal.com/cgi-bin/webscr';
    /** Sandbox Postback URL */
    const SANDBOX_VERIFY_URI = 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr';
    /** Response from PayPal indicating validation was successful */
    const VALID = 'VERIFIED';
    /** Response from PayPal indicating validation failed */
    const INVALID = 'INVALID';
    /** Webhook URLS */
    const LIVE_WEBHOOK_URL = 'https://cupon.laclicsa.com/silabreceiver/index';
    const SANDBOX_WEBHOOK_URL = 'https://cupon.laclicsa.com/silabreceiver/index';

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string $dyn_orders The name of the plugin.
     * @param      string $version The version of this plugin.
     */
    public function __construct($dyn_orders, $version)
    {

        $this->dyn_orders = $dyn_orders;
        $this->version = $version;

        $this->use_sandbox = get_field('sandbox', 'option');

    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Dyn_Orders_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Dyn_Orders_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->dyn_orders, plugin_dir_url(__FILE__) . 'css/dyn-orders-public.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Dyn_Orders_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Dyn_Orders_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->dyn_orders, plugin_dir_url(__FILE__) . 'js/dyn-orders-public.js', array('jquery'), $this->version, false);

    }

    private function responseWithError($code)
    {

        $errors = [
            'LAC_HEADER' => 'Las cabeceras de la petición no son válidas',
            'LAC_BODY' => 'No es válido el contenido de la petición',
            'LAC_SIGNATURE' => 'Esta petición no puede ser autorizada',
            'LAC_REQUEST' => 'Orden no autorizada',
            'LAC_DATA' => 'Datos incorrectos',
            'LAC_DATA_FOLIO' => 'No se proporcionó el folio de la cotización',
        ];

        $response = [
            'type' => 'error',
            'code' => $code,
            'message' => $errors[$code]
        ];

        echo json_encode($response);
        die();

    }

    public function receive_order()
    {
        $bodyRaw = file_get_contents('php://input');
        $body = json_decode($bodyRaw);
        $headers = getallheaders();

        // Verify the user agent
        if (false == isset($headers['User-Agent']) || substr($headers['User-Agent'], 0, 5) != 'Silab') {
            $this->responseWithError('LAC_HEADER');
        }

        // Verify order ID
        /*if (false == isset($headers['Order-ID']) || $headers['Order-ID'] <= 0) {
            $this->responseWithError('LAC_HEADER');
        }*/

        // Check that Order-Signature header is present
        if (false == isset($headers['Order-Signature'])) {
            $this->responseWithError('LAC_HEADER');
        }

        // Check that Order-Signature is valid
        if (false == isset($headers['Order-Request'])) {
            $this->responseWithError('LAC_HEADER');
        }

        // Check the post body
        if (empty($bodyRaw) || '' == $bodyRaw) {
            $this->responseWithError('LAC_BODY');
        }

        // Verify order signature
        $signature = base64_encode(hash_hmac('sha256', $bodyRaw, DYN_ORDERS_KEY, true));
        if ($signature != $headers['Order-Signature']) {
            $this->responseWithError('LAC_SIGNATURE');
        }

        // Verify order request
        $request = base64_encode(hash_hmac('sha256', $body->date, DYN_ORDERS_KEY, true));
        if ($request != $headers['Order-Request']) {
            $this->responseWithError('LAC_REQUEST');
        }

        // Check body content
        /*if (false == isset($body->id) || $body->id <= 0) {
            $this->responseWithError('LAC_DATA_ID');
        };*/

        if (false == isset($body->date) || $body->date == '') {
            $this->responseWithError('LAC_DATA_DATE');
        };

        $time = strtotime($body->date);

        if (false === $time) {
            $this->responseWithError('LAC_DATA_TIME');
        }

        /*if( $time > time() ){
            $this->responseWithError('LAC_DATA');
        }*/

        if (false == isset($body->total) || $body->total <= 0) {
            $this->responseWithError('LAC_DATA_TOTAL');
        };

        if (false == isset($body->valid) || $body->valid <= 0) {
            $this->responseWithError('LAC_DATA_VALID');
        };

        if (false == isset($body->total) || $body->total <= 0) {
            $this->responseWithError('LAC_DATA_TOTAL');
        };

        if (false == isset($body->customer) || empty($body->customer)) {
            $this->responseWithError('LAC_DATA_CUSTOMER');
        };

        /** Check customer data */
        if (false == isset($body->customer->name) || $body->customer->name == '') {
            $this->responseWithError('LAC_DATA_NAME');
        };

        //if(false == isset($body->customer->lastname) || $body->customer->lastname == ''){
        //    $this->responseWithError('LAC_DATA_LASTNAME');
        //};

        if (false == isset($body->customer->email) || $body->customer->email == '' || false == filter_var($body->customer->email, FILTER_VALIDATE_EMAIL)) {
            $this->responseWithError('LAC_DATA_EMAIL');
        };

        //if(false == isset($body->customer->phone) || $body->customer->phone == ''){
        //    $this->responseWithError('LAC_DATA_PHONE');
        //};

        if (false == isset($body->employee) || $body->employee == '') {
            $this->responseWithError('LAC_DATA_EMPLOYEE');
        };

        if (false == isset($body->products) || empty($body->products)) {
            $this->responseWithError('LAC_DATA_PRODUCTS');
        };

        if(empty($body->folio) || $body->folio === ''){
            $this->responseWithError('LAC_DATA_FOLIO');
        }

        $token = $this->getToken(15);

        $body->token = $token;

        /**
         * Insert new order to database
         */
        $id = $this->insertOrder($headers, $body);

        $logs = get_post_meta($id, 'logs', true);
        $logs->events[] = [
            'event' => 'Orden Generada',
            'id' => $id,
            'body' => $body,
            'time' => time(),
        ];
        update_post_meta($id, 'logs', $logs);
        update_post_meta($id, 'employee', $body->employee);
        update_post_meta($id, 'folio', $body->folio);

        $result = [
            'id' => $id,
            'date' => date('Y-m-d H:i:s', time()),
            'url' => get_site_url(null, 'presupuesto/' . base64_encode($id . '-' . $token) . '/')
        ];

        // Send email
        ob_start();
        include_once $this->templates('tpl-email-link.php' );
        //echo "Hola";
        $message = ob_get_clean();

        $cabeceras[] = 'MIME-Version: 1.0';
        $cabeceras[] = 'Content-type: text/html; charset=utf8';

        // Cabeceras adicionales
        //$cabeceras[] = 'To: ' . $body->customer->name . ' <'. $body->customer->email . '>';
        $cabeceras[] = 'From: Laboratorios Laclicsa <' . get_field('budget_email', 'option') . '>';

        // Send email to customer
        $mail = wp_mail( $body->customer->email, 'Su Presupuesto Laclicsa (#' . $id . ')', $message, $cabeceras );
        $logs->events[] = [
            'event' => 'Email enviado al cliente',
            'id' => "Presuspuesto: #" . $id,
            'message' => $message,
            'headers' => $cabeceras,
            'result' => $mail,
            'time' => time(),
        ];
        file_put_contents( ABSPATH . '/email.log', json_encode($cabeceras) . "\r\nResult: " . $mail);

        update_post_meta($id, 'logs', $logs);
        echo json_encode($result);

        die();
    }

    /**
     * Plugin templates
     */
    private function templates($template_name, $args = [])
    {
        if (is_array($args) && isset($args)) {
            foreach($args as $k => $v){
                $GLOBALS[$k] = $v;
            }
            //extract($args);
        }

        $template_file = dirname(__DIR__) . '/templates/' . $template_name;

        if (!file_exists($template_file)) {
            _doing_it_wrong(__FUNCTION__, sprintf('<code>%s</code> does not exists.', $template_file), '1.0.0');
            return;
        }

        return $template_file;
    }

    public function parse_info()
    {
        global $wp_query;
        $status = $wp_query->query_vars['status'];
        $action = $wp_query->query_vars['budget'];

        if('openpay-wh' == $action){
            $this->openpayWebhook();
            return;
        }

        $budget = explode("-", base64_decode($wp_query->query_vars['budget']));
        $budgetId = (int) $budget[0];

        if($budgetId <= 0){
            $budget = isset($_POST['idp']) ? explode("-", base64_decode($_POST['idp'])) : 0;
            $budgetId = (int) $budget[0];
        }

        $budgetPost = get_post((int)$budgetId);

        if (false == $budgetPost || 'dynorders' != $budgetPost->post_type) {
            $this->no_post_page();
            return;
        }

        $data = get_post_meta($budgetPost->ID, 'body', true);

        if ($data->token != $budget[1]) {
            $this->no_post_page();
            return;
        }

        if('POST' == $_SERVER['REQUEST_METHOD'] && 'openpay' == $action){

            $method = isset($_POST['method']) ? $_POST['method'] : 'cards';

            if('stores' == $method){
                $this->processOpenpayStores($data, $budgetPost);
                return;
            }

            $this->processOpenpay($data, $budgetPost);
            return;

        }

        if ('POST' != $_SERVER['REQUEST_METHOD'] && $status == '') {

            if ($data->payed == 1) {
                $this->no_post_page();
                return;
            }

            include $this->templates('tpl-budget-info.php');
            return;
        }

        if ('pagado' == $status) {
            $this->thankyou_page();
        }

        if ('verificar' == $status) {
            $this->process_payment();
        }
        return;
    }

    /**
     * Receives data from openpay
     */
    private function openpayWebhook()
    {

        $body = json_decode( file_get_contents('php://input') );
        file_put_contents(ABSPATH . '/wp-content/uploads/ipn.json', print_r($body, true));

        if(strtolower($body->transaction->status) == 'completed'){
            $pID = $body->transaction->order_id;

            $budgetPost = get_post((int)$pID);

            if (false === $budgetPost || 'dynorders' != $budgetPost->post_type) {
                die('Not processed');
            }

            $data = get_post_meta($budgetPost->ID, 'body', true);

            // Get post logs
            $logs = get_post_meta($budgetPost->ID, 'logs', true);
            $myPost = [
                'payment_status' => ucfirst($body->transaction->status),
                'mc_gross' => $body->transaction->amount,
                'first_name' => $data->customer->name,
                'last_name' => $data->customer->lastname,
                'payer_email' => $data->customer->email,
                'txn_id' => $body->transaction->id,
            ];

            // Verify amounts match
            if($body->transaction->amount != $data->total){
                $logs->events[] = [
                    'description' => "ERROR: El total registrado no coincide con el total recibido",
                    'source' => 'OpenPay webhook',
                    'total_registered' => $data->total,
                    'total_received' => $body->transaction->amount,
                    'data_received' => $body,
                    'time' => time(),
                ];
                update_post_meta($budgetPost->ID, 'logs', $logs);
                die("Ocurrió un error");
            }

            // Update budget status after verification
            if ($myPost['payment_status'] == 'Completed' && $data->payed !== 1 ) {
                $data->payed = 1;

                // Send email
                ob_start();
                include_once $this->templates('tpl-email-payment-confirm.php' );
                $message = ob_get_clean();

                $cabeceras[] = 'MIME-Version: 1.0';
                $cabeceras[] = 'Content-type: text/html; charset=utf8';

                // Cabeceras adicionales
                //$cabeceras[] = 'To: ' . $body->customer->name . ' <'. $body->customer->email . '>';
                $cabeceras[] = 'From: Laboratorios Laclicsa <' . get_field('budget_email', 'option') . '>';
                // @Todo: remove this cc email
                $cabeceras[] = 'Cc: yo@eduardocortes.mx';

                // Send email to customer
                $mail = wp_mail( get_field('payments-notification-email', 'option'), 'Confirmación de pago de presupuesto ' . $data->folio, $message, $cabeceras );
                $logs->events[] = [
                    'event' => 'Email de confirmación de pago enviado',
                    'time' => time(),
                    'id' => "Presupuesto: #" . $pID,
                    'message' => $message,
                    'headers' => $cabeceras,
                    'result' => $mail
                ];
                update_post_meta($pID, 'logs', $logs);

            } else {
                $data->payed = 0;
            }

            update_post_meta($pID, 'body', $data);
            $this->sendWebhook( 'openpay', $data, $budgetPost, $myPost, $logs);

            die('Procesado');


        }
        die('Ok');

    }

    private function openpayWebhookCreate()
    {
        if(!get_field('sandbox', 'option')){
            Openpay::setProductionMode(true);
        }

        $openpay = Openpay::getInstance(get_field('openpay-id', 'option'), get_field('openpay-private', 'option'));

        if('' == get_field('openpay-webhook', 'option')){

            $webhook = [
                'url' => get_site_url() . '/presupuesto/openpay-wh/',
                'user' => 'laclicsa',
                'password' => 'lacswb',
                'event_types' => [
                    'charge.refunded',
                    'charge.failed',
                    'charge.cancelled',
                    'charge.succeeded',
                    'charge.created',
                    'order.completed',
                    'order.cancelled',
                    'payout.succeeded'
                ]
            ];

            /*$webhook = $openpay->webhooks->get(get_field('openpay-webhook', 'option'));
            $webhook->delete();*/

            try{
                $webhook = $openpay->webhooks->add($webhook);
                update_field('openpay-webhook', $webhook->id, 'option' );
                update_field('openpay-webhook-code', $webhook->verification_code, 'option' );
            } catch (Exception $e){
                echo $e->getMessage();
            }

            return;

        }
    }

    private function processOpenpayStores($data, $budgetPost)
    {
        global $wp_query;

        if(0 == get_field('sandbox', 'option')){
            Openpay::setProductionMode(true);
        }
        $openpay = Openpay::getInstance(get_field('openpay-id', 'option'), get_field('openpay-private', 'option'));
        $pID = $budgetPost->ID;

        //$this->openpayWebhookCreate();

        $customer = array(
            'name' => $data->customer->name,
            'last_name' => $data->customer->lastname,
            'phone_number' => $data->customer->phone,
            'email' => $data->customer->email);

        $chargeData = array(
            'method' => 'store',
            'amount' => $data->total,
            'description' => 'Presupuesto #'  . $pID,
            'customer' => $customer,
            'order_id' => $pID,
            'due_date' => date('Y-m-d\TH:i:s', strtotime('+ 48 hours'))
        );

        $logs = get_post_meta($pID, 'logs', true);

        try {
            $charge = $openpay->charges->create($chargeData);
        } catch (Exception $e) {
            error_log('Error on the script: ' . $e->getMessage(), 0);
            $logs->events[] = [
                'event' => 'Error al crear  cargo OpenPayStores',
                'time' => time(),
                'type' => 'error',
                'error' => $e->getMessage(),
                'charge' => $chargeData,
                'customer' => $customer,
                'result' => 'Error',
                'time' => time(),
            ];
            update_post_meta($pID, 'logs', $logs);

            header('Location: ' . get_site_url(null, 'presupuesto/' . base64_encode($pID . '-' . $data->token) . '/?result=1'));

            exit();
        }

        $logs->events[] = [
            'event' => 'Creando cargo OpenPayStores',
            'charge' => $chargeData,
            'customer' => $customer,
            'result' => 'Correcto',
            'time' => time(),
        ];
        update_post_meta($pID, 'logs', $logs);

        if ( $this->use_sandbox ){
            $url = 'https://sandbox-dashboard.openpay.mx/paynet-pdf/' . get_field('openpay-id', 'option') . '/' . $charge->payment_method->reference;
        } else {
            $url = 'https://dashboard.openpay.mx/paynet-pdf/' . get_field('openpay-id', 'option') . '/' . $charge->payment_method->reference;
        }

        header('Location: ' . $url );
        die();

    }

    private function processOpenpayError($charge)
    {
        if(isset($charge->error_code) && $charge->error_code > 0){
            return true;
        }

        return false;
    }

    private function processOpenpay($data, $budgetPost)
    {

        //echo dirname(plugin_dir_path(__FILE__)) . '/openpay/Openpay.php';
        $pID = $budgetPost->ID;

        if(0 == get_field('sandbox', 'option')){
            Openpay::setProductionMode(true);
        }

        $this->openpayWebhookCreate();

       $openpay = Openpay::getInstance(get_field('openpay-id', 'option'), get_field('openpay-private', 'option'));

        $customer = array(
            'name' => $data->customer->name,
            'last_name' => $data->customer->lastname,
            'phone_number' => $data->customer->phone,
            'email' => $data->customer->email);

        $chargeData = array(
            'method' => 'card',
            'source_id' => $_POST["token_id"],
            'amount' => $data->total,
            'description' => 'Presupuesto #'  . $pID,
            //'use_card_points' => $_POST["use_card_points"], // Opcional, si estamos usando puntos
            'device_session_id' => $_POST["deviceIdHiddenFieldName"],
            'customer' => $customer,
            'order_id' => $pID
        );

        $logs = get_post_meta($budgetPost->ID, 'logs', true);
        //print_R($customer); die();
        
				try{
            $charge = $openpay->charges->create($chargeData);
        } catch (Exception $e) {
            $logs->events[] = [
                'event' => 'Error al crear cargo OpenPay Cards',
                'error' => $e->getMessage(),
                'charge' => $chargeData,
                'customer' => $customer,
                'result' => 'Error',
                'time' => time(),
            ];
            update_post_meta($budgetPost->ID, 'logs', $logs);
            $this->error_transaction();
            return;
        }

        if($this->processOpenpayError($charge)){
            $logs->events[] = [
                'event' => 'Creando cargo OpenPayCards',
                'charge' => $chargeData,
                'result' => 'Error',
                'time' => time(),
            ];
            $this->no_post_page();
            return;
        }

        $logs->events[] = [
            'event' => 'Creando cargo OpenPayCards',
            'charge' => $chargeData,
            'result' => 'Correcto',
            'time' => time(),
        ];

        // Get post logs
        $myPost = [
            'payment_status' => ucfirst($charge->status),
            'mc_gross' => $charge->amount,
            'first_name' => $data->customer->name,
            'last_name' => $data->customer->lastname,
            'payer_email' => $data->customer->email,
            'txn_id' => $charge->id,
        ];

        // Update budget status after verification
        if ($myPost['payment_status'] == 'Completed') {
            $data->payed = 1;
        } else {
            $data->payed = 0;
        }

        update_post_meta($budgetPost->ID, 'logs', $logs);
        update_post_meta($budgetPost->ID, 'body', $data);

        if(0 == get_field('sandbox', 'option')) {
            $this->sendWebhook('openpay', $data, $budgetPost, $myPost, $logs);
        }

        if ($myPost['payment_status'] == 'Completed') {
            $this->thankyou_page();
        } else {
            $this->no_post_page();
        }

        return;

    }


    private function thankyou_page()
    {
        global $wp_query;
        $budget = explode("-", base64_decode($wp_query->query_vars['budget']));
        $budgetId = (int) $budget[0];

        if($budgetId <= 0){
            $budget = isset($_POST['idp']) ? explode("-", base64_decode($_POST['idp'])) : 0;
            $budgetId = (int) $budget[0];
        }

        $budgetPost = get_post((int)$budgetId);

        if (false == $budgetPost || 'dynorders' != $budgetPost->post_type) {
            $this->no_post_page();
            return;
        }

        $data = get_post_meta($budgetPost->ID, 'body', true);

        if ($data->token != $budget[1]) {
            $this->no_post_page();
            return;
        }

        /*if ($data->tkpage == 1) {
            $this->no_post_page();
            return;
        }*/

        if ($data->payed == 1 && $data->tkpage == 1) {
            $this->no_post_page();
            return;
        }

        $data->tkpage = 1;
        update_post_meta($budgetPost->ID, 'body', $data);

        include $this->templates('tpl-thank-you.php');
        die();
    }

    private function no_post_page()
    {
        include $this->templates('tpl-no-post.php');
        return;
    }
		
		private function error_transaction()
    {
        include $this->templates('tpl-no-post.php', ['message' => 'Ocurrió un error al realizar la transacción. Por favor vuelva a intentarlo. Si el problema persiste póngase en contacto con nuestro equipo de atención a clientes.']);
        return;
    }

    public function getPaypalUri()
    {
        if ($this->use_sandbox) {
            return self::SANDBOX_VERIFY_URI;
        } else {
            return self::VERIFY_URI;
        }
    }

    private function process_payment()
    {
        if (!count($_POST)) {
            throw new Exception("Missing POST Data");
        }

        // Check budget information
        global $wp_query;
        $budget = explode("-", base64_decode($wp_query->query_vars['budget']));

        $budgetPost = get_post((int)$budget[0]);

        // If budget does not exists generates an error
        if (false == $budgetPost || 'dynorders' != $budgetPost->post_type) {
            throw new Exception("Budget $budget[0] does not exists!");
        }

        $data = get_post_meta($budgetPost->ID, 'body', true);

        // Verify budget token
        if ($data->token != $budget[1]) {
            throw new Exception("Budget $token $budget[1] does not match!");
        }

        /*
         * Check if this budget has been payed previously.
         * We don´t allow duplicated payments
         */
        if ($data->payed == 1) {
            throw new Exception("Budget does not exists or has been payed previously!");
        }

        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2) {
                // Since we do not want the plus in the datetime string to be encoded to a space, we manually encode it.
                if ($keyval[0] === 'payment_date') {
                    if (substr_count($keyval[1], '+') === 1) {
                        $keyval[1] = str_replace('+', '%2B', $keyval[1]);
                    }
                }
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }
        }

        // Build the body of the verification post request, adding the _notify-validate command.
        $req = 'cmd=_notify-validate';
        $get_magic_quotes_exists = false;
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }

        // Post the data back to PayPal, using curl. Throw exceptions if errors occur.
        $ch = curl_init($this->getPaypalUri());
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        // This is often required if the server is missing a global cert bundle, or is using an outdated one.
        if ($this->use_local_certs) {
            curl_setopt($ch, CURLOPT_CAINFO, __DIR__ . "/cert/cacert.pem");
        }
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'User-Agent: PHP-IPN-Verification-Script',
            'Connection: Close',
        ));
        $res = curl_exec($ch);
        if (!($res)) {
            $errno = curl_errno($ch);
            $errstr = curl_error($ch);
            curl_close($ch);
            throw new Exception("cURL error: [$errno] $errstr");
        }
        $info = curl_getinfo($ch);
        $http_code = $info['http_code'];
        if ($http_code != 200) {
            throw new Exception("PayPal responded with http code $http_code");
        }
        curl_close($ch);

        // Get post logs
        $logs = get_post_meta($budgetPost->ID, 'logs', true);

        // Check if PayPal verifies the IPN data, and if so, return true.
        if ($res == self::INVALID) {
            $logs->errors[] = 'No se pudó verificar la autenticidad de la notificación IPN';
            return;
        }

        // Update budget status after verification
        if ($myPost['payment_status'] == 'Completed' && $data->payed !== 1) {
            $data->payed = 1;

            // Send email
            ob_start();
            include_once $this->templates('tpl-email-payment-confirm.php' );
            //echo "Hola";
            $message = ob_get_clean();

            $cabeceras[] = 'MIME-Version: 1.0';
            $cabeceras[] = 'Content-type: text/html; charset=utf8';

            // Cabeceras adicionales
            //$cabeceras[] = 'To: ' . $body->customer->name . ' <'. $body->customer->email . '>';
            $cabeceras[] = 'From: Laboratorios Laclicsa <' . get_field('budget_email', 'option') . '>';

            // Send email to customer
            $mail = wp_mail( get_field('payments-notification-email', 'option'), 'Confirmación de pago de presupuesto ' . $data->folio, $message, $cabeceras );
            $logs->events[] = [
                'event' => 'Email de confirmación de pago enviado',
                'time' => time(),
                'id' => "Presupuesto: #" . $budgetPost->ID,
                'message' => $message,
                'headers' => $cabeceras,
                'result' => $mail,
            ];
            update_post_meta($budgetPost->ID, 'logs', $logs);

        } else {
            $data->payed = 0;
        }

        update_post_meta($budgetPost->ID, 'body', $data);

        $this->sendWebhook( 'paypal', $data, $budgetPost, $myPost, $logs);

        return;
    }

    /**
     * Envía la información al servidor de cupones para procesar el pago
     * @param string $method
     * @param $data
     * @param $budgetPost
     * @param $myPost
     * @param $logs
     */
    private function sendWebhook($method = 'PayPal', $data, $budgetPost, $myPost, $logs)
    {
        // Add items to order
        $products = [];
        $total = 0;
        foreach ($data->products as $product) {
            $products[] = [
                'id' => $product->id,
                'product_id' => $product->id,
                'sku' => $product->id,
                'name' => $product->name,
                'quantity' => $product->qty,
                'price' => $product->price,
                'subtotal' => $product->price * $product->qty,
                'subtotal_tax' => '0.00',
                'total' => $product->price * $product->qty,
                'total_tax' => '0.00',
                "taxes" => [],
                "meta_data" => [],
                "tax_class" => "",
            ];
            $total += ($product->price * $product->qty);
        }


        // Configure webhook call
        $whData = [
            'id' => $budgetPost->ID,
            "number" => $budgetPost->ID,
            "status" => $myPost['payment_status'],
            "currency" => 'MXN',
            "prices_include_tax" => false,
            'date_created' => date('c', time()),
            'date_completed' => $myPost['payment_status'] == 'Completed' ? date('c', time()) : '',
            'total' => $total,
            'folio' => $data->folio,
            'billing' => [
                'first_name' => $myPost['first_name'],
                'last_name' => $myPost['last_name'],
                'email' => $myPost['payer_email'],
                "company" => "",
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "postcode" => "",
                "country" => "",
                "phone" => "",
            ],
            'payment_method' => $method,
            'payment_method_title' => $method,
            'transaction_id' => $myPost['txn_id'],
            'line_items' => $products
        ];

        $start_time = microtime(true);

        // Setup request args.
        $http_args = array(
            'method' => 'POST',
            'timeout' => MINUTE_IN_SECONDS,
            'redirection' => 0,
            'httpversion' => '1.0',
            'blocking' => true,
            'user-agent' => sprintf('DynOrders/%s Hookshot (WordPress/%s)', WC_VERSION, $GLOBALS['wp_version']),
            'body' => trim(wp_json_encode($whData)),
            'headers' => array(
                'Content-Type' => 'application/json',
            ),
            'cookies' => array(),
        );

        $http_args = apply_filters('dynorders_webhook_http_args', $http_args);

        // Add custom headers.
        $delivery_id = wp_hash($whData['id'] . strtotime('now'));
        $http_args['headers']['X-WC-Webhook-Source'] = home_url('/'); // Since 2.6.0.
        $http_args['headers']['X-WC-Webhook-Topic'] = 'order.updated';
        $http_args['headers']['X-WC-Webhook-Resource'] = 'order';
        $http_args['headers']['X-WC-Webhook-Event'] = 'updated';
        $http_args['headers']['X-WC-Webhook-Signature'] = base64_encode(hash_hmac('sha256', $http_args['body'], DYN_ORDERS_KEY, true)); //$this->generate_signature( $http_args['body'] );
        $http_args['headers']['X-WC-Webhook-Delivery-ID'] = $delivery_id;

        // Webhook away!
        $response = wp_safe_remote_request($this->get_delivery_url(), $http_args);

        $duration = round(microtime(true) - $start_time, 5);

        if (is_wp_error($response)) {
            $response_code = $response->get_error_code();
            $response_message = $response->get_error_message();
            $response_headers = array();
            $response_body = '';
        } else {
            $response_code = wp_remote_retrieve_response_code($response);
            $response_message = wp_remote_retrieve_response_message($response);
            $response_headers = wp_remote_retrieve_headers($response);
            $response_body = wp_remote_retrieve_body($response);
        }

        $responseData = array(
            'Code' => $response_code,
            'Message' => $response_message,
            'Headers' => $response_headers,
            'Body' => $response_body,
        );

        $logs->events[] = [
            'event' => 'Enviando orden para generar Cupón',
            'Delivery ID' => $delivery_id,
            'time' => time(),
            'URL' => $this->get_delivery_url(),
            'Duration' => $duration,
            'Request' => array(
                'Method' => $http_args['method'],
                'Headers' => array_merge(
                    array(
                        'User-Agent' => $http_args['user-agent'],
                    ),
                    $http_args['headers']
                ),
            ),
            'Response' => $responseData,
            'Body' => json_decode($http_args['body']),
        ];

        update_post_meta($budgetPost->ID, 'logs', $logs);

        file_put_contents(ABSPATH . '/wp-content/uploads/ipn.json', json_encode($logs->events));

        ob_start();
        include_once $this->templates('tpl-email.php' );
        $message = ob_get_clean();

        $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
        $cabeceras .= 'Content-type: text/html; charset=utf8' . "\r\n";

        // Cabeceras adicionales
        $cabeceras .= 'To: ' . $myPost['first_name'] . ' <'. $myPost['payer_email'] . '>' . "\r\n";
        $cabeceras .= 'From: Laboratorios Laclicsa <' . get_field('budgets_email', 'option') . '>' . "\r\n";

        // Send email to customer
        $mail = wp_mail( $myPost['payer_email'], 'Pago de Estudios en Laclicsa (#' . $budgetPost->ID . ')', $message, $cabeceras );
    }

    private function get_delivery_url()
    {
        if ($this->use_sandbox) {
            return $this::SANDBOX_WEBHOOK_URL;
        }

        return $this::LIVE_WEBHOOK_URL;
    }

    public function webhook_listener()
    {

        if (isset($_GET['todo']) || $_GET['todo'] == 'webhook-order') {
            $this->receive_order();
        }

        if (isset($_GET['status']) && $_GET['status'] == 'payed' && isset($_GET['budget'])) {
            $this->thankyou_page();
        }

    }

    private function insertOrder($headers, $body)
    {

        $id = wp_insert_post([
            'post_status' => 'publish',
            'meta_input' => ['headers' => $headers, 'body' => $body],
            'post_type' => 'dynorders'
        ], true);

        if ($id <= 0) {
            return false;
        }

        return $id;

    }

    private function randSecure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int)($log / 8) + 1; // length in bytes
        $bits = (int)$log + 1; // length in bits
        $filter = (int)(1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    public function getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->randSecure(0, $max - 1)];
        }

        return $token;
    }

}
