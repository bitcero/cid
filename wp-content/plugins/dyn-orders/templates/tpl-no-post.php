<?php
add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');
add_action('genesis_after_header', 'laclicsa_page_header');

// Custom body classes
function dyn_orders_body_classes($classes)
{
    $classes[] = 'tpl-budget-error';
    return $classes;
}

add_filter('body_class', 'dyn_orders_body_classes');

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'laclicsa_presupuesto_loop');

function laclicsa_presupuesto_loop()
{
    global $message;
    ?>

    <div class="er-container">
        <h1>¡Ocurrió un Error!</h1>
        <?php if(isset($message)): ?>
            <p><?php echo $message; ?></p>
        <?php else: ?>
            <p>No existe el presupuesto especificado o ya ha sido completado. ¿Necesita ayuda?
            <strong><a href="#" onclick="parent.LC_API.open_chat_window({source:'minimized'});">Estamos para atenderle</a></strong>.</p>
        <?php endif; ?>
    </div>
    <hr>
    <div class="laclicsa-promos woocommerce">

        <h2>Promociones</h2>
        <ul class="products columns-3">
            <?php

            $bQuery = new WP_Query([
                'post_type' => 'product',
                'posts_per_page' => 3,
                'orderby' => 'rand'
            ]);

            while ($bQuery->have_posts()) : $bQuery->the_post();
                ?>

                <?php wc_get_template_part( 'content', 'product' ); ?>

            <?php
            endwhile;
            ?>
        </ul>
    </div>
    <?php
}

genesis();