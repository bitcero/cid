<?php
/* Template Name: Presupuestos */

add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');
add_action('genesis_after_header', 'laclicsa_page_header');

// Custom body classes
function dyn_orders_body_classes($classes)
{
    $classes[] = 'tpl-budgets';
    return $classes;
}

add_filter('body_class', 'dyn_orders_body_classes');

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'laclicsa_presupuesto_loop');

// Add openpay scripts
add_action('wp_enqueue_scripts', 'lacs_openpay_scripts');

function lacs_openpay_scripts() {
    wp_enqueue_script('openpay', 'https://openpay.s3.amazonaws.com/openpay.v1.min.js', [], null, true);
    wp_enqueue_script('openpay-data', 'https://openpay.s3.amazonaws.com/openpay-data.v1.min.js', ['openpay'], null, true);
}

function laclicsa_presupuesto_loop()
{
    global $wp_query;
    $budget = explode("-", base64_decode($wp_query->query_vars['budget']));

    $budgetPost = get_post((int)$budget[0]);

    // Add fallback


    // Budget data
    $data = get_post_meta($budgetPost->ID, 'body', true);

    //$s = isset($_GET)

    ?>

    <div id="budget-data">

        <?php
        $error = isset($_GET['result']) ? intval($_GET['result']) : false;

        if($error){
            ?>
            <div class="alert alert-error">
                No se ha podido procesar su solicitud. Por favor póngase en contacto con nuestro equipo de atención a clientes. Este error puede deberse a
                que la orden que intenta pagar ya ha sido procesada previamente, a que ha vencido o a algún problema en el procesamiento de su pago.
            </div>
            <?php
        }

        ?>

        <div class="details">

            <h1>Presupuesto #<?php echo $budgetPost->ID; ?></h1>

            <div class="customer-data">
                <div class="data-item">
                    <label>Nombre:</label>
                    <div class="data-value">
                        <?php echo $data->customer->name . ' ' . $data->customer->lastname; ?>
                    </div>
                </div>
                <div class="data-item">
                    <label>Email:</label>
                    <div class="data-value">
                        <?php echo $data->customer->email; ?>
                    </div>
                </div>
                <div class="data-item">
                    <label>Teléfono:</label>
                    <div class="data-value">
                        <?php echo $data->customer->phone != '' ? $data->customer->phone : 'Not provided'; ?>
                    </div>
                </div>
                <div class="data-item">
                    <label>Fecha:</label>
                    <div class="data-value">
                        <?php echo $data->date; ?>
                    </div>
                </div>
            </div>

            <div class="budget-details">
                <div class="table-responsive">
                    <table>
                        <thead>
                        <tr>
                            <th>Cant.</th>
                            <th>Estudio</th>
                            <th style="text-align: right;">
                                <span class="large">Precio Unitario</span>
                                <span class="small">P.U.</span>
                            </th>
                            <th style="text-align: right;">
                                <span class="large">Precio Total</span>
                                <span class="small">Subtotal</span>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($data->products as $product): ?>
                            <tr>
                                <td><?php echo $product->qty; ?></td>
                                <td><?php echo $product->name; ?> (<?php echo $product->id; ?>)</td>
                                <td>$ <?php echo number_format($product->price, 2); ?></td>
                                <td>$ <?php echo number_format($product->price * $product->qty, 2); ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr class="total">
                            <td colspan="2">&nbsp;</td>
                            <td>Total:</td>
                            <td><strong>$ <?php echo number_format($data->total, 2); ?></strong></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <div class="payment">

            <div class="price">
                <small>TOTAL</small>
                <span class="total">$<?php echo number_format($data->total, 2); ?></span>
                <span class="choose">Elija su método de pago</span>
            </div>

            <?php if(get_field('openpay-cards', 'option')): ?>
            <div class="paynow openpay-cards">
                <button type="button" id="openpay-cards">
                    Tarjeta de Débito o Crédito
                    <span>
                        <img src="<?php echo bloginfo('url'); ?>/wp-content/plugins/openpay-cards/assets/images/openpay.png" alt="Pagar con OpenPay">
                        <img src="<?php echo bloginfo('url'); ?>/wp-content/plugins/openpay-cards//assets/images/credit_cards.png" alt="Pagar con Tarjeta de Crédito">
                        <img src="<?php echo bloginfo('url'); ?>/wp-content/plugins/openpay-cards//assets/images/debit_cards.png" alt="Pagar con Tarjeta de Débito">
                    </span>
                </button>
            </div>
            <?php endif; ?>

            <?php if(get_field('openpay-stores', 'option')): ?>
            <div class="paynow openpay-stores">
                <button type="button" id="openpay-stores">
                    Tiendas de Conveniencia
                    <span>
                        <img src="<?php echo bloginfo('url'); ?>/wp-content/plugins/openpay-cards/assets/images/openpay.png" alt="Pagar con OpenPay">
                        <img src="<?php echo bloginfo('url'); ?>/wp-content/plugins/openpay-stores//assets/images/stores.png" alt="Pagar en tiendas de conveniencia">
                    </span>
                </button>
            </div>
            <?php endif; ?>

            <?php if(get_field('paypal', 'option')): ?>
            <div class="paynow paypal">

                <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                    <button type="submit"><img src="https://www.paypalobjects.com/webstatic/mktg/logo-center/logotipo_paypal_pagos.png" alt="Pague con PayPal"></button>
                    <input type="hidden" name="budget" value="<?php echo $budgetPost->ID; ?>">
                    <!--?php wp_nonce_field( 'pay-budget-'. $budgetPost->ID); ?-->
                    <INPUT TYPE="hidden" name="cmd" value="_xclick">
                    <INPUT TYPE="hidden" name="currency_code" value="MXN">
                    <INPUT TYPE="hidden" name="business" value="<?php echo get_field('budget_paypal', 'option'); ?>">
                    <INPUT TYPE="hidden" name="email" value="<?php echo $data->customer->email; ?>">
                    <input type="hidden" name="item_name" value="Presupuesto #<?php echo $budgetPost->ID; ?>">
                    <input type="hidden" name="item_number" value="<?php echo $budgetPost->ID; ?>">
                    <input type="hidden" name="amount" value="<?php echo $data->total; ?>">
                    <INPUT TYPE="hidden" name="return" value="<?php echo bloginfo('url'); ?>/presupuesto/<?php echo $wp_query->query_vars['budget']; ?>/pagado/">
                    <INPUT TYPE="hidden" name="cancel_return" value="<?php echo bloginfo('url'); ?>/presupuesto/<?php echo $wp_query->query_vars['budget']; ?>/">
                    <INPUT TYPE="hidden" name="notify_url" value="<?php echo bloginfo('url'); ?>/presupuesto/<?php echo $wp_query->query_vars['budget']; ?>/verificar/">
                </form>
            </div>
            <?php endif; ?>

        </div>

    </div>

    <div id="payment-overlay"></div>

    <?php if(get_field('openpay-cards', 'option')): ?>
    <div id="openpay-cards-modal" class="openpay-modal">
        <form action="<?php echo bloginfo('url'); ?>/presupuesto/openpay/" method="POST" id="payment-form">
            <input type="hidden" name="token_id" id="token_id">
            <div class="pymnt-itm card active">
                <h2>Tarjeta de crédito o débito</h2>
                <div class="pymnt-cntnt">
                    <div class="card-expl">
                        <div class="credit">
                            <h4>Tarjetas de crédito</h4>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/cards1.png" alt="Tarjetas de crédito">
                        </div>
                        <div class="debit">
                            <h4>Tarjetas de débito</h4>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/cards2.png" alt="Tarjetas de débito">
                        </div>
                    </div>
                    <div class="sctn-row">
                        <div class="one-half first">
                            <label>Nombre del titular</label>
                            <input
                                    type="text"
                                    placeholder="Como aparece en la tarjeta"
                                    autocomplete="off"
                                    data-openpay-card="holder_name"
                                    <?php if(get_field('sandbox', 'option')): ?>
                                    value="Sandbox Name"
                                    <?php endif; ?>
                                    required
                            >
                        </div>
                        <div class="one-half">
                            <label>Número de tarjeta</label>
                            <input
                                    type="text"
                                    autocomplete="off"
                                    data-openpay-card="card_number"
                                    placeholder="•••• •••• •••• ••••"
                                    inputmask=""
                                    maxlength="16"
                                    required
                                    <?php if(get_field('sandbox', 'option')): ?>
                                    value="4242424242424242"
                                    <?php endif; ?>
                            >
                        </div>
                    </div>
                    <div class="sctn-row">
                        <div class="one-half first">
                            <label>Fecha de expiración</label>
                            <div class="one-third first">
                                <input
                                        type="text"
                                        placeholder="Mes"
                                        data-openpay-card="expiration_month"
                                        maxlength="2"
                                        <?php if(get_field('sandbox', 'option')): ?>
                                        value="03"
                                        <?php endif; ?>
                                        required
                                >
                            </div>
                            <div class="one-sixth">&nbsp;</div>
                            <div class="one-third">
                                <input
                                        type="text"
                                        placeholder="Año"
                                        data-openpay-card="expiration_year"
                                        maxlength="2"
                                        <?php if(get_field('sandbox', 'option')): ?>
                                        value="22"
                                        <?php endif; ?>
                                        required
                                >
                            </div>
                        </div>
                        <div class="one-half cvv"><label>Código de seguridad</label>
                            <div class="one-third first">
                                <input
                                        type="text"
                                        placeholder="3 dígitos"
                                        autocomplete="off"
                                        data-openpay-card="cvv2"
                                        <?php if(get_field('sandbox', 'option')): ?>
                                        value="123"
                                        <?php endif; ?>
                                        required
                                >
                            </div>
                            <div class="two-thirds" style="padding: 10px;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/cvv.png" style="max-width: 100%;"></div>
                        </div>
                    </div>

                    <div class="sctn-row" style="margin-top: 20px;">
                        <div class="logo">
                            <strong>Transacciones realizadas vía:</strong>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/openpay.png">
                        </div>
                        <div class="shield">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/security.png">
                            Tus pagos se realizan de forma segura con encriptación de 256 bits
                        </div>
                    </div>

                    <div class="sctn-row" style="text-align: right">
                        <button type="button" id="cancel-openpay">Cancelar</button>
                        <a class="button rght" id="pay-button">Pagar</a>
                    </div>
                </div>
            </div>
            <input type="hidden" name="idp" value="<?php echo $wp_query->query_vars['budget']; ?>">
            <input type="hidden" name="method" value="cards">
        </form>
    </div>
    <?php endif; ?>

    <?php if(get_field('openpay-stores', 'option')): ?>
    <div id="openpay-stores-modal" class="openpay-modal">
        <h2>Pago en efectivo en tiendas de conveniencia</h2>
        <div class="info-container">
            <div class="scnt-row">
                <div class="one-third first">
                    <img src="<?php echo bloginfo('url'); ?>/wp-content/plugins/openpay-stores//assets/images/step1.png">
                    Haz clic en el botón <strong>"Generar ficha de pago"</strong>; tu compra quedará en espera de que realices el pago.
                </div>
                <div class="one-third">
                    <img src="<?php echo bloginfo('url'); ?>/wp-content/plugins/openpay-stores//assets/images/step2.png">
                    Imprime tu recibo, llévalo a tu tienda de conveniencia más cercana y realiza el pago.
                </div>
                <div class="one-third">
                    <img src="<?php echo bloginfo('url'); ?>/wp-content/plugins/openpay-stores//assets/images/step3.png">
                    Inmediatamente después de recibir tu pago te enviaremos un correo electrónico con la confirmación de pago.
                </div>
            </div>

            <div class="sctn-row" style="text-align: right">
                <form action="<?php echo bloginfo('url'); ?>/presupuesto/openpay/" method="post">
                    <button type="button" id="cancel-openpay2">Cancelar</button>
                    <button type="submit" class="button rght" id="pay-button2">Generar Ficha de Pago</button>
                    <input type="hidden" name="idp" value="<?php echo $wp_query->query_vars['budget']; ?>">
                    <input type="hidden" name="method" value="stores">
                </form>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <?php

}

genesis();