<div class="wrap">
    <h1 class="wp-heading-inline">Reporte de Presupuestos</h1>
    <hr class="wp-header-end">

    <div class="notice notice-info is-dismissible" data-dismissible="ao-img-opt-plug-123">
        <p>
            Este es el reporte de ventas por ejecutivo.
        </p>
        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Descartar este aviso.</span></button>
    </div>

    <div class="tablenav top">

        <form id="month-selector" method="get">
            <div class="alignleft actions">
                <select name="month" id="bulk-action-selector-top">
                    <option value=""<?php echo $month === null ? 'selected' : ''?>>Seleccionar Mes</option>
                    <option value="1"<?php echo $month === 1 ? 'selected' : ''?>>Enero</option>
                    <option value="2"<?php echo $month === 2 ? 'selected' : ''?>>Febrero</option>
                    <option value="3"<?php echo $month === 3 ? 'selected' : ''?>>Marzo</option>
                    <option value="4"<?php echo $month === 4 ? 'selected' : ''?>>Abril</option>
                    <option value="5"<?php echo $month === 5 ? 'selected' : ''?>>Mayo</option>
                    <option value="6"<?php echo $month === 6 ? 'selected' : ''?>>Junio</option>
                    <option value="7"<?php echo $month === 7 ? 'selected' : ''?>>Julio</option>
                    <option value="8"<?php echo $month === 8 ? 'selected' : ''?>>Agosto</option>
                    <option value="9"<?php echo $month === 9 ? 'selected' : ''?>>Septiembre</option>
                    <option value="10"<?php echo $month === 10 ? 'selected' : ''?>>Octubre</option>
                    <option value="11"<?php echo $month === 11 ? 'selected' : ''?>>Noviembre</option>
                    <option value="12"<?php echo $month === 12 ? 'selected' : ''?>>Diciembre</option>
                </select>

                <select name="year" id="bulk-action-selector-top">
                    <option value=""<?php echo $year === null ? 'selected' : ''?>>Seleccionar Año</option>
                    <?php $currentYear = date('Y'); for($i = $currentYear - 5; $i <= $currentYear; $i++): ?>
                        <option value="<?php echo $i; ?>"<?php echo $year === $i ? 'selected' : ''?>><?php echo $i; ?></option>
                    <?php endfor; ?>
                </select>
                <button type="submit" id="doaction" class="button action">Aplicar</button>
            </div>
            <input type="hidden" name="page" class="post_type_page" value="dynorders-report">
        </form>

        <br class="clear">

    </div>

    <table class="wp-list-table widefat fixed striped reports" style="width: auto;">
        <thead>
        <tr>
            <td id="employee"class=" manage-column">
                Ejecutivo
            </td>
            <td id="sales" class="manage-column">
                No. Ventas
            </td>
            <td id="total" class="manage-column">
                <strong>Total</strong>
            </td>
            <td>&nbsp;</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach($report as $name => $item): ?>
            <tr>
                <td>
                    <?php echo $name; ?>
                </td>
                <td>
                    <?php echo count($item['items']); ?>
                </td>
                <td>
                    $ <?php echo number_format($item['total'], 2); ?>
                </td>
                <td>
                    <a href="admin.php?page=dynorders-report&month=<?php echo $month; ?>&year=<?php echo $year; ?>&employee=<?php echo urlencode($name); ?>">Presupuestos</a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>