<table>
    <?php foreach($prop as $id => $value): ?>
        <?php if(is_object($value)): ?>
            <tr>
                <td colspan="2" class="inner-content">
                    <?php $this->readProperty($value); ?>
                </td>
            </tr>
        <?php elseif(is_array($value)): ?>
            <tr><td class="title" colspan="2"><?php echo $id; ?></td></tr>
            <?php $this->readArray($value); ?>
        <?php else: ?>
            <tr>
                <td class="data-name"><?php echo $id; ?></td>
                <td><?php echo $value; ?></td>
            </tr>
        <?php endif; ?>
    <?php endforeach; ?>
</table>