<?php
add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');
add_action('genesis_after_header', 'laclicsa_page_header');

// Custom body classes
function dyn_orders_body_classes($classes)
{
    $classes[] = 'tpl-thank-you';
    return $classes;
}

add_filter('body_class', 'dyn_orders_body_classes');

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'laclicsa_presupuesto_loop');

function laclicsa_presupuesto_loop()
{
    ?>
    <div class="ty-container">
        <h1>¡Gracias por su preferencia!</h1>

        <p>Su pago está siendo procesado. Si todo es correcto, en breve recibirá un email informándole sobre el cargo a
            su tarjeta o cuenta <strong>PayPal&reg;</strong>.</p>
        <p>¿Deseas más información?<br><strong><a href="#" onclick="parent.LC_API.open_chat_window({source:'minimized'});">Estamos para atenderle</a></strong>
        </p>
    </div>
<hr>
    <div class="laclicsa-promos woocommerce">

        <h2>Promociones</h2>
        <ul class="products columns-3">
            <?php

            $bQuery = new WP_Query([
                'post_type' => 'product',
                'posts_per_page' => 3,
                'orderby' => 'rand'
            ]);

            while ($bQuery->have_posts()) : $bQuery->the_post();
                ?>

                <?php wc_get_template_part( 'content', 'product' ); ?>

            <?php
            endwhile;
            ?>
        </ul>
    </div>

    <?php
}

genesis();