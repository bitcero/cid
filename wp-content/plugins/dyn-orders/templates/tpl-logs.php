<?php if(!$post): ?>
    <?php $error = new WP_Error('budget-not-found', 'No existe el presupuesto especificado'); ?>
    <div style="clear: both; margin-bottom: 15px;"></div>
    <div class="error notice" style="margin-left: 0; padding: 10px;"><p><?php $error->get_error_message(); ?></p></div>';
<?php endif; ?>

<?php if($logs): ?>

    <div id="logs-page">
        <div style="clear: both; margin-bottom: 30px;"></div>
        <h1 class="wp-heading-inline section-title">
            Eventos para el presupuesto #<?php echo $post->post_name; ?>
            <?php if($folio): ?>(Folio: <?php echo $folio; ?>)<?php endif; ?>
        </h1>

        <div class="logs-list">
            <ul>
                <?php foreach($logs->events as $i => $log): ?>
                    <li>
                        <span class="commands">
                            <button class="btn btn-view" type="button" onclick="jQuery('.the-log:visible').slideUp(); jQuery('#log-<?php echo $i+1; ?>').slideDown();">Mostrar</button>
                        </span>
                        <span class="id"><?php echo $i+1; ?></span>
                        <span class="date"><?php echo date_i18n('M j, Y @ G:i', $log['time'], true); ?></span>
                        <span class="name"><strong><?php echo $log['event']; ?></strong></span>
                        <div class="the-log" id="log-<?php echo $i+1; ?>">
                            <table>
                                <tr>
                                    <th>Dato</th>
                                    <th>Valor</th>
                                </tr>
                                <?php foreach($log as $id => $value): ?>
                                    <?php if(is_object($value) || is_array($value)): ?>
                                        <tr><td class="title" colspan="2"><?php echo $id; ?></td></tr>
                                        <tr>
                                            <td colspan="2" class="inner-content">
                                                <?php $this->readProperty($value); ?>
                                            </td>
                                        </tr>
                                    <?php else: ?>
                                    <tr>
                                        <td class="data-name"><?php echo $id; ?></td>
                                        <td>
                                            <?php if('message' === $id): ?>
                                                <?php
                                                    $message = $value;
                                                    $message = str_replace(['<body','/body'],['<bodyhtml','/bodyhtml'], $message);
                                                    echo $message;
                                                ?>
                                            <?php else: ?>
                                                <?php echo $value; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

<?php endif; ?>
