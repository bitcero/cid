<h3>Confirmación de pago completado</h3>

<ul style="list-style: none">
    <li>Folio: <strong><?php echo $data->folio; ?></strong></li>
    <li>ID: <strong><?php echo $pID ? $pID : $budgetPost->ID; ?></strong></li>
    <li>Total: <strong>$<?php echo number_format($data->total, 2); ?></strong></li>
    <li><hr></li>
    <li>Cliente: <strong><?php echo $data->customer->name; ?></strong> &lt<?php echo $data->customer->email; ?>&gt</li>
    <li>Estudios:
        <ul>
        <?php foreach($data->products as $product): ?>
        <li><?php echo $product->qty . ' - ' . $product->name . '(' . $product->id . ') - $' . number_format($product->price, 2); ?>
        <?php endforeach; ?>
        </li>
    </li>
</ul>
